# LaLo transport planner
LaLo transport planner is a time-saving tool to automatically place cargo in a cargo space using an algorithm. The cargo in the cargo space are visualized as floor plans so the loader can use those floor plans while loading the vehicles.

LaLo is a generic solution and can therefore be used by various companies.

LaLo transport planner consists of an Angular front-end and an Express backend. Two algorithms are implemented: a Backtracking and a Greedy algorithm. Those algorithms take into account the weights and dimensions of the cargo and the cargo space.

## Installation
This repository is a mono repo. Both the front and backend are in this repository.

1. Navigate via the terminal to the folder: `backend`
2. Run `npm install`
3. Navigate via the terminal to the folder: `frontend`
4. Run `npm install`

## Usage
1. Start the front end
   i. Run `ng serve` or `npm run ng serve` from the terminal in the folder: `frontend`
2. Start the back end
   i. Run `npm run start` from the terminal in the folder: `backend`
3. Browse to: http://localhost:4200

## Configuration
A configuration can be done for the algorithm. This configuration is located in `backend/src/config/xx.yml`. Currently there is a configuration for testing and development/production environment.
The configuration can be modified in the yml file, but it can also be modified by adding environment variables.

Environment variables:
* `VOXELSIZE` => Number of centimeters that one virtual voxel should represent
* `UNITBOUNDARY` => Amount of units when the algorithm should take into account the possibility of an emergency stop
* `WEIGHTBOUNDARY` => Amount of kilograms when the algorithm should take into account the possibility of an emergency stop

## Database
LaLo is using MongoDB to save the cargo plans. Turn on your mongo server to save and retrieve the plans.

## Floor plans
At the moment the cargo in the cargo space is visualized as floor plans.
Those floor plans were created with the JavaScript library P5.js. https://p5js.org/

## Features
Planner can:
- [x] Select cargo space
- [x] Select way of loading (through the sides or tailgate)
- [x] Select orders and rearrange them
- [x] Fill in a title for the cargo plan
- [x] Choose for a fast (Greedy) or good (Backtracking) algorithm
- [x] Fill in the amount of time he gives the algorithm to finish
- [x] Watch the cargo plan
- [x] Add a note for the loaders to the plan

Loader can:
- [x] Select a saved cargo plan
- [x] Watch the cargo plan
- [x] Mark the cargo plan as `completed` when he is done

System:
- [x] Plans the cargo in the cargo space and take into account the weight and dimension requirements
- [x] Saves the cargo plan to a MongoDB
- [ ] Gets the products, orders and cargo spaces from an external API or database
- [ ] Gets the kind of algorithms and load directions from a database


## Project status
LaLo transport planner was made as a graduation assignment.
It is an MVP and is not solving the whole problem.