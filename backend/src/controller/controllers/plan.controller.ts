import {Request, Response} from 'express';
import {PlanService} from '../../business/services/plan/plan.service';
import {Order} from '../../domain/models/order.model';
import {Cargospace} from '../../domain/models/cargospace.model';
import {Product} from '../../domain/models/product.model';
import {BaseError} from '../../domain/models/baseError.model';
import {CargoPlanningService} from '../../business/services/plan/cargo-planning.service';

/*
 * Class that calls the business logic
 */
class PlanController {
    async planCargoLayout (
        req: Request,
        res: Response
    ) {
        // Convert cargo space object to Cargospace class
        const cargoSpace = new Cargospace (
                                req.body.cargospace.id,
                                req.body.cargospace.name,
                                req.body.cargospace.length,
                                req.body.cargospace.width,
                                req.body.cargospace.height,
                                req.body.cargospace.maxWeight)

        // Convert req.body.orders to Order array
        const orders = req.body.orders as Order[]

        // Convert product-elements in Order object to Product class
        orders.forEach(order => {
            order.products.forEach((
                product:        Product,
                index:          number,
                product_array:  Product[]
            ) => {
                product_array[index] = new Product (
                                            product.id,
                                            product.name,
                                            product.length,
                                            product.width,
                                            product.height,
                                            product.weight,
                                            product.maxLoadWeight,
                                            product.units);
            })

            // Sort the product array based on height (start with the highest value)
            order.products.sort(function(firstProduct, secondProduct) {
               return secondProduct.height - firstProduct.height
            });
        })

        // Call planService to make a planning with the selected cargospace, orders and algorithm (response is a promise)
        try
        {
            const settings  = req.body.settings;
            const title     = req.body.title;
            const loadDir   = req.body.loadDirection;

            const response = await PlanService.plan (title, cargoSpace, orders, settings, loadDir)

            // Made a planning successfully
            res.status(200).send(response)
        }
        catch(e) {
            // Failed making a planning
            res.status(400).send(new BaseError ('Planning cargo layout failed', 'Plan cargo layout'))
        }
    }

    async getCargoPlannings(
        req: Request,
        res: Response
    ) {
        try {
            const response = await new CargoPlanningService().getAllUnusedCargoPlannings();
            res.status(200).send(response)
        }
        catch (e) {
            res.status(400).send();
        }
    }

    async markCargoPlanningAsCompleted(
        req: Request,
        res: Response
    ) {
        try {
            const response = await new CargoPlanningService().markCargoPlanningAsCompleted(req.body);
            res.status(200).send(response)
        }
        catch (e) {
            res.status(400).send();
        }
    }

    async addNoteToCargoPlanning(
        req: Request,
        res: Response
    ) {
        try {
            const response = await new CargoPlanningService().addNoteToCargoPlanning(req.params.id, req.body);
            res.status(200).send(response)
        }
        catch (e) {
            res.status(400).send();
        }
    }
}

export default new PlanController()
