const router = require('express').Router()
import PlanController from '../controllers/plan.controller'

// Plan routes
router.post('/', PlanController.planCargoLayout)
router.get('/cargoplannings', PlanController.getCargoPlannings)
router.put('/cargoplanning/completed', PlanController.markCargoPlanningAsCompleted)
router.put('/cargoplanning/:id', PlanController.addNoteToCargoPlanning)

export default router
