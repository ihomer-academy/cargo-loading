import {CargoPlanning} from '../../domain/models/cargoPlanning.model';

/*
 * Abstract class that contains the functions that each DAO should implement and make their own implementation for
 */
export abstract class PlanDao {
    abstract saveNewCargoPlanning(cargoPlanning: CargoPlanning): Promise<string>;

    abstract getAllUnusedCargoPlannings(): Promise<CargoPlanning[]>;

    abstract markCargoPlanningAsCompleted(body: object): Promise<CargoPlanning>;

    abstract addNoteToCargoPlanning(id: string, body: object): Promise<CargoPlanning>;
}
