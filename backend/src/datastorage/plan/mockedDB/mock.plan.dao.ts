import {PlanDao} from '../plan.dao';
import {CargoPlanning} from '../../../domain/models/cargoPlanning.model';
import {Cargospace} from '../../../domain/models/cargospace.model';

export class MockPlanDao implements PlanDao {
    cargoPlanning1: CargoPlanning = {
        id: '1',
        cargoLayout: [],
        loadList: [],
        loadDistribution: {leftKg: 0, rightKg: 0},
        cargoLayoutSteps: [],
        cargoSpace: new Cargospace(1, '', 0, 0, 0, 0),
        completed: false,
        createdOn: null,
        note: '',
        title: '',
        loadDirection: '',
        amountOfOriginalProducts: 1
    }

    cargoPlanning2: CargoPlanning = {
        id: '2',
        cargoLayout: [],
        loadList: [],
        loadDistribution: {leftKg: 0, rightKg: 0},
        cargoLayoutSteps: [],
        cargoSpace: new Cargospace(1, '', 0, 0, 0, 0),
        completed: false,
        createdOn: null,
        note: '',
        title: '',
        loadDirection: '',
        amountOfOriginalProducts: 1
    }

    async getAllUnusedCargoPlannings(): Promise<CargoPlanning[]> {
        const cargoPlannings = [
            this.cargoPlanning1,
            this.cargoPlanning2
        ]
        return Promise.resolve(cargoPlannings);
    }

    saveNewCargoPlanning(cargoPlanning:CargoPlanning): Promise<string> {
        return Promise.resolve('');
    }

    markCargoPlanningAsCompleted(
        body: object
    ): Promise<CargoPlanning> {
        return Promise.resolve(this.cargoPlanning2);
    }

    addNoteToCargoPlanning(
        id: string,
        body: object
    ): Promise<CargoPlanning> {
        return Promise.resolve(this.cargoPlanning2);
    }
}
