import {PlanDao} from '../plan.dao';
import mongoose from 'mongoose';
import {CargoPlanning} from '../../../domain/models/cargoPlanning.model';
import MongoCargoPlanning from '../mongoDB/models/cargoPlanning.model';

export class MongoPlanDao implements PlanDao {
    constructor() {
        // Connect to mongo database
        mongoose.connect('mongodb://localhost/cargo_loading', {useNewUrlParser: true, useUnifiedTopology: true});

        const database = mongoose.connection;
        database.on('error', console.error.bind(console, 'connection error:'));
        database.once('open', function() {
            console.log('MongoDB connection established')
        });
    }

    async getAllUnusedCargoPlannings(): Promise<CargoPlanning[]> {
        return MongoCargoPlanning.find({completed: false})
            .then(result => {
                return result;
            })
            .catch(error => {
                return error;
            })
    }

    saveNewCargoPlanning(cargoPlanning:CargoPlanning): Promise<string> {
        const cargoplanning = new MongoCargoPlanning({
                                title:                      cargoPlanning.title,
                                cargoLayout:                cargoPlanning.cargoLayout,
                                cargoSpace:                 cargoPlanning.cargoSpace,
                                loadList:                   cargoPlanning.loadList,
                                loadDistribution:           cargoPlanning.loadDistribution,
                                createdOn:                  cargoPlanning.createdOn,
                                amountOfOriginalProducts:   cargoPlanning.amountOfOriginalProducts,
                                loadDirection:              cargoPlanning.loadDirection,
                                cargoLayoutSteps:           cargoPlanning.cargoLayoutSteps});

        return cargoplanning.save()
            .then((result) => {
                return result._id;
            })
            .catch(() => {
                return '-1';
            })
    }

    markCargoPlanningAsCompleted(
        body: object
    ): Promise<CargoPlanning> {
        return MongoCargoPlanning.findOneAndUpdate({ _id: body['_id'] }, { completed: true })
            .then((result) => {
                return result;
            })
            .catch((error) => {
                return error;
            })
    }

    addNoteToCargoPlanning(
        id: string,
        body: object
    ): Promise<CargoPlanning> {
        return MongoCargoPlanning.findOneAndUpdate({ _id: id }, { note: body['note'] })
            .then((result) => {
                return result;
            })
            .catch((error) => {
                return error;
            })
    }
}
