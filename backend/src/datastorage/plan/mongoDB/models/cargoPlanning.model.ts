import mongoose, { Schema } from 'mongoose';

const CargoPlanningSchema: Schema = new Schema({
    createdOn: {
        type : Date,
        default: Date.now
    },
    title: String,
    note: {
        type: String,
        default: ''
    },
    amountOfOriginalProducts: Number,
    loadDirection: String,
    cargoLayout: {
        type: [],
        default: []
    },
    cargoSpace: {},
    loadList: {
        type: [],
        default: []
    },
    loadDistribution: {
        leftKg: {
            type: Number,
            default: 0
        },
        rightKg: {
            type: Number,
            default: 0
        },
    },
    cargoLayoutSteps: {
        type: [],
        default: []
    },
    completed: {
        type: Boolean,
        default: false
    }
});

export default mongoose.model('CargoPlanning', CargoPlanningSchema);
