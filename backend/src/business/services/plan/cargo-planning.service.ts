import {Inject} from 'typescript-ioc';
import {PlanDao} from '../../../datastorage/plan/plan.dao';
import {CargoPlanning} from '../../../domain/models/cargoPlanning.model';

/**
 * Service that calls function of the selected database implementation
 */
export class CargoPlanningService {
    @Inject private planDAO: PlanDao; // Dependency injection

    getAllUnusedCargoPlannings(): Promise<CargoPlanning[]> {
        return this.planDAO.getAllUnusedCargoPlannings();
    }

    markCargoPlanningAsCompleted(body: object): Promise<CargoPlanning> {
        return this.planDAO.markCargoPlanningAsCompleted(body);
    }

    addNoteToCargoPlanning(id: string, body: object): Promise<CargoPlanning> {
        return this.planDAO.addNoteToCargoPlanning(id, body);
    }
}
