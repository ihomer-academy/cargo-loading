import {Cargospace} from '../../../domain/models/cargospace.model';
import Debug from "debug";
import {CargoLayout} from '../../../domain/types/types';

/**
 * Class that contains functions related to the cargo layout
 */
export class CargolayoutService {
    // Function to initialize an empty cargo layout based on the selected cargospace
    static initializeEmptyCargoLayout(cargospace: Cargospace): CargoLayout
    {
        const cargolayout: CargoLayout = [] // Empty cargolayout (without size)

        for (let iy = 0; iy < cargospace.gridHeight; iy++)
        {
            cargolayout[iy] = new Array(cargospace.gridWidth).fill('00') // Fill gives the array elements a value

            for (let iz = 0; iz < cargospace.gridLength; iz++) {
                cargolayout[iy][iz] = new Array(cargospace.gridWidth).fill('00') // Fill gives the array elements a value
            }
        }

        return cargolayout; // Return 3d array that represents the cargo layout
    }

    /**
     * Function to visualize the cargo layout
     */
    static visualizeCargoLayout(
        cargospace:     Cargospace,
        cargoLayout:   CargoLayout
    ): void
    {
        const log = Debug('cargoServer');

        for (let ih = 0; ih < cargospace.gridHeight; ih++)
        {
            log("---- Laag  " + ih + " ----")

            for (let il = 0; il < cargospace.gridLength; il++) {
                log(cargoLayout[ih][il])
            }
        }
    }
}
