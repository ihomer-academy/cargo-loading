import {AlgorithmFactory} from './algorithms/factories/algorithm.factory';
import {Cargospace} from '../../../domain/models/cargospace.model';
import {Order} from '../../../domain/models/order.model';
import {CargoSettings} from '../../../domain/types/types';
import {CargolayoutService} from './cargolayout.service';
import Debug from 'debug';

/**
 * Class between the controller and the implementation
 */
export class PlanService {
    // Plan function
    static async plan(
        title: string,
        cargoSpace: Cargospace,
        orders: Order[],
        settings: CargoSettings,
        loadDirection: string
    ): Promise<any>
    {
        const log = Debug('cargoServer');

        // Factory to get the right algorithm implementation
        const algorithm = AlgorithmFactory.createAlgorithm({
            cargoSpace,
            orders,
            algorithm: settings.algorithm,
            emergencyStop: settings.emergencyStop,
            timer: settings.timer,
            title,
            direction: loadDirection
        })

        // Make cargo layout
        const plannedCargo = algorithm.planCargo();

        if (plannedCargo)
        {
            const response = algorithm.getResponseValues();

            log("Successfully made a cargo plan!")

            CargolayoutService.visualizeCargoLayout (response.cargoSpace, response.cargoLayout); // Function to visualize the multidimensional array

            try {
                const id = await algorithm.saveCargoplanning();
                return Promise.resolve({
                    id:                     id,
                    cargoLayout:            response.cargoLayout,
                    loadList:               response.loadList,
                    loadDistribution:       {leftKg: response.loadDistribution.leftKg, rightKg: response.loadDistribution.rightKg},
                    cargoLayoutSteps:       response.cargolayoutSteps,
                    amountOfOriginalProducts: response.amountOfOriginalProducts
                })
            }
            catch(e) {
                return Promise.reject()
            }
        }
        else {
            // Product(s) couldn't be placed in the cargospace
            log("Making a cargo plan failed")

            return Promise.reject('failed')
        }
    }
}
