import {Backtracking} from '../types/backtracking';
import {AlgorithmConfig} from '../../../../../domain/models/algorithm/algorithmConfig.model';
import {Greedy} from '../types/greedy';
import {Algorithm} from '../algorithm';

/**
 * Factory to select the corresponding algorithm class
 */
export class AlgorithmFactory {
    static createAlgorithm (
        algorithmConfig: AlgorithmConfig
    ): Algorithm {
        switch (algorithmConfig.algorithm)
        {
            case 'backtracking': {
                return new Backtracking(algorithmConfig);
            }
            case 'greedy': {
                return new Greedy(algorithmConfig);
            }
            default: {
                return new Backtracking(algorithmConfig);
            }
        }
    }
}
