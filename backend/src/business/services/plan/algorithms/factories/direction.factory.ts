import {ZXYDirection} from '../load_directions/z-x-y-direction';
import {Direction} from '../load_directions/direction';
import {XYZDirection} from '../load_directions/x-y-z-direction';

/**
 * Factory to select the corresponding load direction class
 */
export class DirectionFactory {
    static chooseDirection (
        direction: string
    ): Direction {
        switch (direction)
        {
            case 'z-x-y': {
                return new ZXYDirection();
            }
            case 'x-y-z': {
                return new XYZDirection();
            }
            default: {
                return new ZXYDirection();
            }
        }
    }
}
