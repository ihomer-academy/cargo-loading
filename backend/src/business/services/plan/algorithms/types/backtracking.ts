import {Algorithm} from '../algorithm';
import {ProductOrientation} from '../../../../../domain/models/enum/product.orientation.enum';
import {Product} from '../../../../../domain/models/product.model';
import Debug from "debug";
import {Coordinates} from '../../../../../domain/models/coordinates.model';
import {CanPlaceAndWasPlaced} from '../../../../../domain/types/types';

export class Backtracking extends Algorithm {
    log = Debug('cargoServer');

    private toRight:    boolean      = false;   // Place product on right side
    private products:   Product[]    = [];

    /**
     * Backtracking implementation to find a location for the products
     */
    planCargo(): boolean
    {
        // if the startTime + time that the planner gave for the plan has not expired
        if(this.returnTime > new Date()) {
            // Return true if all orders are already processed (so orderList is empty)
            if (this.orderList.length === 0) {
                this.log("All orders are already processed");
                return true;
            }

            // Return false if the cargospace is full and not all orders are processed
            if (this.checkIfCargoSpaceIsFull()) {
                this.log("Cargo space is full");
                return false;
            }

            this.order      =   this.orderList[0];      // Get first order of the orderlist (because of the loading order)
            this.products   =   this.order.products;    // Set products of the first order

            // If the left side is heavier than the right side, than we place the next product on the right side
            this.toRight    =   this.leftPerctWeight > this.rightPerctWeight;

            return this.loopVoxelsAndCheckIfProductCanBePlaced(this.toRight);
        } else {
            // Return false if the time has expired
            return false;
        }
    }

    /*
     * Loop all products of the current order and check if one of them can fit in the cargospace with the
     *   specified start coordinates and direction
     */
    loopProductAndCheckIfItCanFitOnStartPosition (
        coordinates: Coordinates
    ): boolean
    {
        // Return false if voxel is already filled
        if (this.voxelIsAlreadyFilled(coordinates)) {
            return false;
        }

        this.log(`New voxel coordinates: y=${coordinates.y} x=${coordinates.x} z=${coordinates.z}`)

        let alreadyTriedProducts = [];

        // Loop all products of the current order
        for (const product of this.products)
        {
            // Check if product is a square or not because changing the orientation of a square is unnecessary
            if (product.gridWidth === product.gridLength) {
                this.log(`Product to try: p.id=${product.id}, index=${alreadyTriedProducts.indexOf(product)}`)

                // Check of current product can fit and check if the same product isn't also tried before
                const [result, productsArray] = this.tryProduct(product, alreadyTriedProducts, coordinates);
                alreadyTriedProducts = productsArray;

                if (result) {
                    return true; // Return true if a product can be placed (and is placed)
                }
            } else {
                // Loop all kind of orientations and check if the product can fit with the changed orientation
                for (let o = 0; o < Object.keys(ProductOrientation).length / 2; o++) {
                    product.changeOrientation(o) // Change orientation of product to the current looped orientation

                    this.log(`Product to try: p.id=${product.id}, index=${alreadyTriedProducts.indexOf(product)}`)

                    // Check of current product can fit and check if the same product isn't also tried before
                    const [result, productsArray] = this.tryProduct(product, alreadyTriedProducts, coordinates);
                    alreadyTriedProducts = productsArray;

                    if (result) {
                        return true;
                    }
                }
            }
        }
        return false; // Return false if none of the products of the current order fits in the cargo layout
    }

    /*
     * Check if product meets requirements. If so, place product and call recursive planCargo function
     */
    private checkIfProductCanBePlacedOnCurrentCoordinates (
        coordinates:    Coordinates,
        productToPlace: Product
    ): CanPlaceAndWasPlaced
    {
        const currentProductCounter = this.loadList.length + 1;

        // If toRight, then check right requirements, otherwise check left requirements
        if (this.toRight)
        {
            // First check if placing the product to the right fits. If not, then try it to the left
            const [meetsRightRequirements, rightCoordinatesWeight] = this.productMeetsRightRequirements(coordinates, productToPlace);
            if (meetsRightRequirements)
            {
                // If product meets right measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, rightCoordinatesWeight)

                // Contains recursive call
                return this.returnPlanCargoResponse();
            }

            // Try left
            const [meetsLeftRequirements, leftCoordinatesWeight] = this.productMeetsLeftRequirements (coordinates, productToPlace);
            if (meetsLeftRequirements)
            {
                // If product meets left measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, leftCoordinatesWeight)

                // Recursive call
                return this.returnPlanCargoResponse();
            }
        }
        else
        {
            // First check if placing the product to the left fits. If not, then try it to the right
            const [meetsLeftRequirements, leftCoordinatesWeight] = this.productMeetsLeftRequirements (coordinates, productToPlace);
            if (meetsLeftRequirements)
            {
                // If product meets left measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, leftCoordinatesWeight)

                // Recursive call
                return this.returnPlanCargoResponse();
            }

            // Try right
            const [meetsRightRequirements, rightCoordinatesWeight] = this.productMeetsRightRequirements(coordinates, productToPlace);
            if (meetsRightRequirements)
            {
                // If product meets right measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, rightCoordinatesWeight)

                // Contains recursive call
                return this.returnPlanCargoResponse();
            }
        }

        // Return response: false and wasPlaced: false if product could not be placed
        return [
            false,
            false
        ];
    }

    /*
     * Check if product can be placed on current coordinates
     * If product was placed (wasPlaced) but response (because another product couldn't fit,
     *   then remove the placed product and try another location for it
     */
    private canPlaceProductWithBacktracking(
        coordinates: Coordinates,
        productToPlace: Product
    ): boolean
    {
        // Product counter is used in the loadlist to see which number belongs to which product
        const currentProductCounter = this.loadList.length + 1;

        const [response, wasPlaced] = this.checkIfProductCanBePlacedOnCurrentCoordinates (coordinates, productToPlace);

        if (response) {
            return true; // Return true if product can be placed
        }
        else if (wasPlaced)
        {
            /**
             * Not all products can be placed, so we need to backtrack
             * Backtracking results in undoing the last correctly done step
             * Undoing in this case results in removing the last placed product
             */
            this.removeProduct (currentProductCounter)
            this.toRight = this.leftPerctWeight > this.rightPerctWeight;
        }

        return false;
    }

    private returnPlanCargoResponse(): CanPlaceAndWasPlaced {
        const response = this.planCargo();

        // If next product can be placed, return true, true
        if (response) {
            return [
                true,
                true
            ];
        } else {
            // If next product cannot be placed, return false, true
            return [
                false,
                true
            ];
        }
    }

    private tryProduct(
        product:                Product,
        alreadyTriedProducts:   any[],
        coordinates:            Coordinates
    ): [
        boolean,
        any[]
    ] {
        /*
         * If the order contains two identical products, and the first one did not fit,
         *  then we don't need to check if the second (identical) product can fit in the cargo layout
         *  because if the first one didn't fit, then the second one will also not fit
         */
        if (alreadyTriedProducts.indexOf(this.getProductObj(product)) === -1) {
            alreadyTriedProducts.push(this.getProductObj(product)); // Add product to the alreadyTriedProducts array

            // Check if product can be placed
            if (this.canPlaceProductWithBacktracking(coordinates, product)) {
                return [true, alreadyTriedProducts]; // Return true if product can be placed
            }
        }
        return [false, alreadyTriedProducts]; // Return false if product cannot be placed (then we try the next one)
    }
}
