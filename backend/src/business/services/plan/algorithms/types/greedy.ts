import {Algorithm} from '../algorithm';
import {Product} from '../../../../../domain/models/product.model';
import Debug from 'debug';
import {Coordinates} from '../../../../../domain/models/coordinates.model';
import {ProductOrientation} from '../../../../../domain/models/enum/product.orientation.enum';

export class Greedy extends Algorithm {
    log = Debug('cargoServer');

    private toRight:    boolean      = false;   // Place product on right side
    private products:   Product[]    = [];

    private makeCargoplan: boolean = true;     // Boolean that decides if it should keep going with making a plan or not

    /**
     * Greedy implementation to find a location for the products
     */
    planCargo(): boolean
    {
        // Loop till makeCargoplan is false
        while(this.makeCargoplan) {
            // if the startTime + time that the planner gave for the plan has not expired, run plan function
            if (this.returnTime > new Date()) {
                // Stop making plan if all orders are already processed (so orderList is empty)
                if (this.orderList.length === 0) {
                    this.makeCargoplan = false;
                    break;
                }

                // Stop making plan if the cargospace is full and not all orders are processed
                if(this.checkIfCargoSpaceIsFull()) {
                    this.makeCargoplan = false;
                    break;
                }

                this.order      =   this.orderList[0];      // Get first order of the orderlist (because of the loading order)
                this.products   =   this.order.products;

                // If the left side is heavier than the right side, than we place the next product on the right side
                this.toRight    =   this.leftPerctWeight > this.rightPerctWeight;

                this.callLoopVoxelsAndUpdateMakeCargoplan();
            } else {
                // Stop making cargoplan if time is up
                this.makeCargoplan = false;
                break;
            }
        }
        return true;
    }

    private callLoopVoxelsAndUpdateMakeCargoplan() {
        if(this.loopVoxelsAndCheckIfProductCanBePlaced(this.toRight)) {
            return true;                    // Return true if product can be placed
        } else {
            this.makeCargoplan = false;     // Set makeCargoplan false when while loop needs to stop
            return false;                   // Return false if not products can be placed anymore
        }
    }

    /*
    * Loop all products of the current order and check if one of them can fit in the cargospace with the
    *   specified start coordinates and direction
    */
    loopProductAndCheckIfItCanFitOnStartPosition (
        coordinates: Coordinates
    ): boolean
    {
        // Return false if voxel is already filled
        if (this.voxelIsAlreadyFilled(coordinates)) {
            return false;
        }

        this.log(`New voxel coordinates: y=${coordinates.y} x=${coordinates.x} z=${coordinates.z}`)

        let alreadyTriedProducts = [];

        let bestProduct = this.products[0];         // Set default best product
        let differenceBetweenLeftAndRight = 101;    // Set default difference (%) between left and right side of the cargospace

        // Loop all products of the current order
        for (const product of this.products)
        {
            // Check if product is a square or not because changing the orientation of a square is unnecessary
            if (product.gridWidth === product.gridLength)
            {
                // Compare the results of placing the current product with the current best product
                const [dif, bp] = this.compareWithBestValues(coordinates, product, differenceBetweenLeftAndRight, bestProduct);
                differenceBetweenLeftAndRight = dif;
                bestProduct = bp;
            } else
            {
                // Loop all kind of orientations and check if the product can fit with the changed orientation
                for (let o = 0; o < Object.keys(ProductOrientation).length / 2; o++) {
                    product.changeOrientation(o) // Change orientation of product to the current looped orientation

                    // Compare the results of placing the current product with the current best product
                    const [dif, bp] = this.compareWithBestValues(coordinates, product, differenceBetweenLeftAndRight, bestProduct);
                    differenceBetweenLeftAndRight = dif;
                    bestProduct = bp;
                }
            }
        }

        this.log(`Product to try: p.id=${bestProduct.id}, index=${alreadyTriedProducts.indexOf(bestProduct)}`)

        // Try to fit (and place) the best product
        const [result] = this.tryProduct(bestProduct, alreadyTriedProducts, coordinates);
        return result;
    }

    /**
     * Method that compares the current best product with the current product
     * With "best" we mean the product that results in the least cargo distribution difference
     */
    private compareWithBestValues(
        coordinates:    Coordinates,
        product:        Product,
        curDif:         number,
        curBestProduct: Product
    ): [
        number,
        Product
    ] {
        let meetsRequirements;
        let weightCoordinates;

        if (this.toRight) {
            [meetsRequirements, weightCoordinates] = this.productMeetsRightRequirements(coordinates, product);
        } else {
            [meetsRequirements, weightCoordinates] = this.productMeetsLeftRequirements(coordinates, product);
        }

        // Check of product meets all requirements
        if(meetsRequirements) {
            const leftPerct     = weightCoordinates.leftWeight  / (weightCoordinates.leftWeight + weightCoordinates.rightWeight) * 100;
            const rightPerct    = weightCoordinates.rightWeight / (weightCoordinates.leftWeight + weightCoordinates.rightWeight) * 100;

            // Calculate new difference
            const tempDif = Math.abs(leftPerct - rightPerct);

            // If new difference is less than the current best difference, then return the new diff and product
            if(tempDif < curDif) {
                return [tempDif, product]
            }
        }

        // If current product does not meet all requirements, then return the best difference and product (so the best keeps the same)
        return [curDif, curBestProduct];
    }

    private tryProduct(
        product:                Product,
        alreadyTriedProducts:   any[],
        coordinates:            Coordinates
    ): [
        boolean,
        any[]
    ] {
        /*
         * If the order contains two identical products, and the first one did not fit,
         *  then we don't need to check if the second (identical) product can fit in the cargo layout
         *  because if the first one didn't fit, then the second one will also not fit
         */
        if (alreadyTriedProducts.indexOf(this.getProductObj(product)) === -1) {
            alreadyTriedProducts.push(this.getProductObj(product)); // Add product to the alreadyTriedProducts array

            // Check if product can be placed
            if (this.canPlaceProduct(coordinates, product, this.toRight))
            {
                // If all products are placed, stop make cargo plan
                if(this.products.length === 0) {
                    this.makeCargoplan = false;
                }

                return [true, alreadyTriedProducts];
            }
        }
        return [false, alreadyTriedProducts];
    }
}
