import {CargoLayout, YXAndZDimensionArrays, YAndZDimensionArrays} from '../../../../../domain/types/types';
import {Cargospace} from '../../../../../domain/models/cargospace.model';
import {Product} from '../../../../../domain/models/product.model';

/**
 * Abstract class that represents a direction
 */
export abstract class Direction {
    abstract cartesianVoxels (yCoordinates: number[], xCoordinates: number[], zCoordinates: number[]): any[]
    abstract getXYZOfCartesianLoop (coordinatesArray: number[]): [number, number, number]

    getAllCargospaceYAndXAndZVoxelsToLoop(
        cargoSpace: Cargospace
    ): YXAndZDimensionArrays {
        /**
         * Spread operator is used to prevent for loop and make it easier to read
         * This: [...Array(cargoSpace.gridHeight).keys()],
         * Is same as:
         * for(let y = 0; y < cargoSpace.gridHeight; y++) { this.yVoxels.push(y) }
        */
        return [
            [...Array(cargoSpace.gridHeight).keys()],
            [...Array(cargoSpace.gridWidth).keys()],
            [...Array(cargoSpace.gridLength).keys()]
        ]
    }

    protected getCargospaceYAndZVoxelsToLoop(
        cargoSpace:     Cargospace,
        highestYValue:  number
    ): YAndZDimensionArrays {
        // Loop till cargoSpace.gridHeight and keep the values <= this.highestYValue + 2
        const yCoordinates = [...Array(cargoSpace.gridHeight).keys()].filter(nr => nr <= highestYValue + 2)

        // Loop till cargoSpace.gridLength
        const zCoordinates = [...Array(cargoSpace.gridLength).keys()]

        return [yCoordinates, zCoordinates]
    }

    protected getRightCargospaceXVoxelsToLoop(
        cargoSpace:     Cargospace,
        rightBeginX:    number
    ): number[] {
        // Loop 0 till this.cargoSpace.gridWidth and only return the values >= rightBeginX
        return [...Array(cargoSpace.gridWidth).keys()].filter(nr => nr >= rightBeginX)
    }

    protected getLeftCargospaceXVoxelsToLoop(
        rightBeginX: number
    ): number[] {
        // Loop 0 till rightBeginX and then return the reversed array (for example: [ 1, 0 ] instead of [0, 1])
        return [...Array(rightBeginX).keys()].reverse();
    }

    /**
     * Cartesian product is a way to prevent nested for loops and create one (flat) array with all possible combinations
     */
    protected cartesianProduct(
        arr: YXAndZDimensionArrays
    ): any[]
    {
        return arr.reduce (function(a,b){
            return a.map (function(x){
                return b.map (function(y){
                    return x.concat ([y]);
                })
            }).reduce (function(c,d){ return c.concat (d) },[])
        }, [[]])
    }

    protected getYAndZVoxelsToLoop(
        y:              number,
        z:              number,
        productToPlace: Product,
    ): YAndZDimensionArrays
    {
        // Get Y and Z voxels where the product will come
        return [
            [...Array(y + productToPlace.gridHeight).keys()].filter(nr => nr >= y),
            [...Array(z + productToPlace.gridLength).keys()].filter(nr => nr >= z)
        ]
    }

    protected getRightXVoxelsToLoop = (
        x:              number,
        productToPlace: Product
    ): number[] =>
    {
        // Loop till x + gridWidth. Startvalue is x
        return [...Array(x + productToPlace.gridWidth).keys()].filter(nr => nr >= x);
    };

    protected getLeftXVoxelsToLoop = (
        x:              number,
        productToPlace: Product
    ): number[] =>
    {
        const xCoordinates = [];

        // Not done with spread operator, because this is more difficult when having x-- instead op x++
        for (let xx = x; xx >= x - (productToPlace.gridWidth - 1); xx--) {
            xCoordinates.push(xx);
        }

        return xCoordinates;
    };

    /**
     * Some directions can have extended requirements (for example when loading a truck with tailgate)
     * Those requirements can be added when overriding this method
     * Default reponse is true (when there are no extended requirements)
     */
    protected meetsDirectionRequirements(
        cargoLayout:    CargoLayout,
        cargoSpace:     Cargospace,
        z:              number,
        x:              number,
        y:              number,
        zCoordinates:   []
    ): boolean
    {
        return true;
    }
}
