import {Direction} from './direction';
import {Cargospace} from '../../../../../domain/models/cargospace.model';
import {CargoLayout} from '../../../../../domain/types/types';

/**
 * Class that represents a cargospace with a tailgate (so loading can only via the back of the cargospace)
 * First load the whole back, then stack the back (if possible), and then move to the front
*/
export class XYZDirection extends Direction {
    cartesianVoxels(
        yCoordinates: number[],
        xCoordinates: number[],
        zCoordinates: number[]
    ): any[] {
        // Call cartesianProduct with the right coordinates sequence
        return this.cartesianProduct ([zCoordinates, yCoordinates, xCoordinates]);
    }

    /**
     * Get the x, y and z value of the array (the sequence 0, 1 or 2 is very important here)
     */
    getXYZOfCartesianLoop(
        coordinatesArray: number[]
    ): [
        number,
        number,
        number
    ] {
        const x = coordinatesArray[2];
        const y = coordinatesArray[1];
        const z = coordinatesArray[0];

        return [x, y, z];
    }

    /**
     * Optional direction requirements
     * A truck with tailgate can only be loaded via the back of it
     * So we need to confirm that the voxels to fill are accessible via the back
     */
    protected meetsDirectionRequirements(
        cargoLayout:    CargoLayout,
        cargoSpace:     Cargospace,
        z:              number,
        x:              number,
        y:              number,
        zCoordinates:   []
    ): boolean
    {
        const maxZ = zCoordinates[zCoordinates.length - 1]; // Max value to loop in the length

        const zArray = [...Array(cargoSpace.gridLength).keys()].filter(zNr => zNr > maxZ);
        const yArray = [...Array(cargoSpace.gridHeight).keys()];

        const voxelsToLoop = this.cartesianVoxels(yArray, [x], zArray); // Flat array with all combinations

        // Loop flat array with all combinations
        for (const coordinatesArray of voxelsToLoop) {
            const [xx, yy, zz] = this.getXYZOfCartesianLoop(coordinatesArray);

            // Return false if there is a product on the voxel to fill (!== '00')
            if(cargoLayout[yy][zz][xx] !== '00') {
                return false;
            }
        }
        return true; // Return true if all voxels are accessible
    }
}
