import {Direction} from './direction';

/**
 * Class that represents a cargospace that can be loaded via the sides (for example an open truck)
 * First load the middle, then move from the inside to the outside (width), and then stack
 */
export class ZXYDirection extends Direction {
    cartesianVoxels(
        yCoordinates: number[],
        xCoordinates: number[],
        zCoordinates: number[]
    ): any[] {
        // Call cartesianProduct with the right coordinates sequence
        return this.cartesianProduct ([yCoordinates, xCoordinates, zCoordinates]);
    }

    /**
     * Get the x, y and z value of the array (the sequence - 0, 1 or 2 - is very important here)
     */
    getXYZOfCartesianLoop(
        coordinatesArray: number[]
    ): [
        number,
        number,
        number
    ] {
        const x = coordinatesArray[1];
        const y = coordinatesArray[0];
        const z = coordinatesArray[2];

        return [x, y, z];
    }
}
