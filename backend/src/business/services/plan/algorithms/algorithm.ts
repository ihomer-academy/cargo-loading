import {Order} from '../../../../domain/models/order.model';
import {Cargospace} from '../../../../domain/models/cargospace.model';
import {Product} from '../../../../domain/models/product.model';
import {LoadItem} from '../../../../domain/models/loaditem.model';
import {AlgorithmConfig} from '../../../../domain/models/algorithm/algorithmConfig.model';
import {CargolayoutService} from '../cargolayout.service';
import Debug from "debug";
import {Coordinates} from '../../../../domain/models/coordinates.model';
import {CoordinatesWeight} from '../../../../domain/models/coordinatesWeight.model';
import {Inject, InjectValue} from 'typescript-ioc';
import {PlanDao} from '../../../../datastorage/plan/plan.dao';
import {
    CanPlaceAndCoordinatesWeight,
    CanPlaceAndLeftAndRightWeightInKg, CargoLayout,
    LeftAndRightWeightInKg
} from '../../../../domain/types/types';
import {DirectionFactory} from './factories/direction.factory';
import {Config} from '../../../../domain/models/config.model';
import {CargoPlanning} from '../../../../domain/models/cargoPlanning.model';

/**
 * Abstract class that represents an algorithm.
 * This class also contains functions that will be used for all algorithms (such as checking if there is place and placing the products)
 */
export abstract class Algorithm {
    @Inject private planDAO: PlanDao;                   // Database implementation
    @InjectValue("Configuration") config!: Config // Dependency injection

    log = Debug('cargoServer');

    // Reponse info
    protected readonly cargoSpace:      Cargospace;
    protected readonly timer:           number;             // amount of time the planner filled in
    protected loadList:                 LoadItem[] = []     // loadlist with the sequence of the products
    protected readonly orderList:       Order[] = []
    protected readonly cargolayout:     CargoLayout = []    // 3d array that represents the cargo layout
    protected readonly cargolayoutSteps:CargoLayout[] = []; // steps to create final layout (first, there is 1 product, then 2, ...)

    // Weight
    protected leftWeightTotal:          number = 0;         // (in kilograms)
    protected rightWeightTotal:         number = 0;         // (in kilograms)
    protected leftPerctWeight:          number = 0;         // (in percents)
    protected rightPerctWeight:         number = 0;         // (in percents)

    // Supportive variables
    protected readonly rightBeginX:     number = 0;         // x value where the right part of the cargospace begins
    protected highestYValue:            number = 0;         // highest y value that is loaded (if first p is 2 voxels high, value will be 1)
    private amountOfFilledVoxels:       number = 0;         // to determine of the cargospace is fully loaded
    private readonly title:             string = '';
    protected readonly direction;                           // load direction
    protected order:                    Order;              // current order
    protected readonly returnTime:      Date;               // maximum date/time when there should be returned a response
    protected readonly emergencyStop:   boolean;            // take into account emergency stop
    private amountOfOriginalProducts:   number = 0;
    private readonly loadDirection:     string;

    constructor(
        algorithmConfig: AlgorithmConfig
    ) {
        // Get configuration options
        this.cargoSpace     = algorithmConfig.cargoSpace;
        this.orderList      = algorithmConfig.orders;
        this.title          = algorithmConfig.title;
        this.timer          = algorithmConfig.timer;
        this.emergencyStop  = algorithmConfig.emergencyStop;

        this.loadDirection  = algorithmConfig.direction;
        algorithmConfig.orders.forEach(o => this.amountOfOriginalProducts += o.products.length);

        this.direction      = DirectionFactory.chooseDirection(algorithmConfig.direction);

        // Create multidimensional array with the right amount of voxels
        this.cargolayout    = CargolayoutService.initializeEmptyCargoLayout (this.cargoSpace)

        this.rightBeginX    = Math.ceil (this.cargoSpace.gridWidth / 2); // This is the x value where the right side of the cargospace starts

        this.returnTime     = new Date();
        this.returnTime.setSeconds( this.returnTime.getSeconds() + this.timer );
    }

    // All kind of algorithms need to implement this function and make their own implementation
    abstract planCargo (): boolean
    abstract loopProductAndCheckIfItCanFitOnStartPosition (coordinates: Coordinates): boolean

    /*
     * Check if the cargospace boundaries are not exceeded when placing the product on the specified location
     */
    private productFitsCargospaceBoundaries(coordinates: Coordinates): boolean
    {
        return  (coordinates.y < this.cargoSpace.gridHeight && coordinates.y >= 0) &&
                (coordinates.x < this.cargoSpace.gridWidth  && coordinates.x >= 0) &&
                (coordinates.z < this.cargoSpace.gridLength && coordinates.z >= 0);
    }

    protected loopVoxelsAndCheckIfProductCanBePlaced(toRight: boolean): boolean {
        const [yCoordinates, zCoordinates] = this.direction.getCargospaceYAndZVoxelsToLoop(this.cargoSpace, this.highestYValue);

        let x_coordinates;

        if(toRight) {
            x_coordinates = this.direction.getRightCargospaceXVoxelsToLoop(this.cargoSpace, this.rightBeginX);
        } else {
            x_coordinates = this.direction.getLeftCargospaceXVoxelsToLoop(this.rightBeginX);
        }

        const coordinatesOfAllVoxelsToLoop = this.direction.cartesianVoxels (yCoordinates, x_coordinates, zCoordinates);

        // Loop all voxels in the cargospace
        for (const coordinatesArray of coordinatesOfAllVoxelsToLoop) {
            const [x, y, z] = this.direction.getXYZOfCartesianLoop(coordinatesArray);

            const coordinates = new Coordinates (y, x, z)

            // Return true if the current voxel is not floating in the air (when voxel below is '00')
            //  And when a product can be placed on this start coordinate
            if (this.yIsNotFloating (coordinates) &&
                this.loopProductAndCheckIfItCanFitOnStartPosition (coordinates)) {
                return true;
            }
        }
        return false;
    }

    // Check if the current product fits in the cargospace with as start voxel(y, x and z) and moving to the right
    protected productMeetsRightRequirements(
        coordinates: Coordinates,
        productToPlace: Product
    ): CanPlaceAndCoordinatesWeight
    {
        const xVoxelsToLoop = this.direction.getRightXVoxelsToLoop (coordinates.x, productToPlace);

        return this.productMeetsRequirements (
            coordinates,
            productToPlace,
            xVoxelsToLoop
        )
    }

    // Check if the current product fits in the cargospace with as start voxel(y, x and z) and moving to the left
    protected productMeetsLeftRequirements(
        coordinates: Coordinates,
        productToPlace: Product
    ): CanPlaceAndCoordinatesWeight
    {
        const xVoxelsToLoop = this.direction.getLeftXVoxelsToLoop(coordinates.x, productToPlace);

        return this.productMeetsRequirements(
            coordinates,
            productToPlace,
            xVoxelsToLoop
        )
    }


    /*
     * General functions
    */

    protected productMeetsRequirements(
        coordinates: Coordinates,
        productToPlace: Product,
        xCoordinates: number[]
    ): CanPlaceAndCoordinatesWeight
    {
        // Get array of y and z coordinates that will be filled when placing the productToPlace
        const [yCoordinates, zCoordinates] = this.direction.getYAndZVoxelsToLoop (
            coordinates.y,
            coordinates.z,
            productToPlace
        );

        // Get all possible combinations of the coordinates
        const coordinatesOfAllVoxelsToLoop = this.direction.cartesianVoxels(yCoordinates, xCoordinates, zCoordinates);

        // If not all voxels are empty, the product cannot be placed here
        if(!this.allVoxelsAreEmpty(coordinatesOfAllVoxelsToLoop)) {
            return [false, {voxelCoordinatesArray: [], leftWeight: 0, rightWeight: 0}];
        }

        let tempTotalLeft   = this.leftWeightTotal;     // Bind current left weight total value
        let tempTotalRight  = this.rightWeightTotal;    // Bind current right weight total value

        /*
         * Array to save the coordinates. If the product can be placed on the current start location, then we use this value
         *  when placing the product so we don't need to do this for loop twice
         */
        const voxelCoordinatesArray: Coordinates[] = [];

        /*
            Loop cartesian product coordinates
         */
        for (const coordinatesArray of coordinatesOfAllVoxelsToLoop) {
            const [x, y, z] = this.direction.getXYZOfCartesianLoop(coordinatesArray);
            const newCoordinates = new Coordinates(y, x, z);

            voxelCoordinatesArray.push(newCoordinates) // Add current coordinates to the voxel array

            // Return false if placing the product doesn't meet the optional requirements (for example with tailgate)
            if(!this.direction.meetsDirectionRequirements(
                this.cargolayout,
                this.cargoSpace,
                z,
                x,
                y,
                zCoordinates
            )) {
                return [false, {voxelCoordinatesArray: [], leftWeight: 0, rightWeight: 0}];
            }

            if(y === yCoordinates[0]) {
                // Check if placing (a part of) the product in this voxel meets the weight and measurement requirements
                const [meetsRequirements, [tempLeftWeight, tempRightWeight]] = this.meetsAllRequirements (
                    coordinates.y,
                    newCoordinates,
                    productToPlace,
                    tempTotalLeft,
                    tempTotalRight
                );

                // Return false if filling the voxel does not meet requirements
                if(!meetsRequirements) {
                    return [false, {voxelCoordinatesArray: [], leftWeight: 0, rightWeight: 0}];
                }

                // Update temporary weights
                tempTotalLeft = tempLeftWeight;
                tempTotalRight = tempRightWeight;
            }
        }

        // Return false when placing the product doesn't meet the emergency stop requirement
        if(!this.canHandleEmergencyStop(
            coordinates,
            xCoordinates,
            yCoordinates,
            productToPlace
        )) {
            return [false, {voxelCoordinatesArray: [], leftWeight: 0, rightWeight: 0}];
        }

        return this.returnCoordinatesWeight(tempTotalLeft, tempTotalRight, voxelCoordinatesArray);
    }

    /*
     * Place product on specified location (this function is executed when the algorithm knows that placing the product is possible)
     */
    protected placeProduct(
        productCounter:     number,
        productToPlace:     Product,
        order:              Order,
        coordinatesWeight:  CoordinatesWeight
    ): void
    {
        // Update cargolayout array
        this.loopCoordinatesAndPlaceProductInCargolayout (productCounter, coordinatesWeight);

        // Calculate new distribution of the weight in percents
        this.updateWeights();

        this.log(`After placing: p.id = ${productToPlace.id} with counter: ${productCounter}`)
        this.log(`left=${this.leftPerctWeight}, right=${this.rightPerctWeight} %`)
        this.log(`left=${this.leftWeightTotal}, right=${this.rightWeightTotal}`)

        // Show cargolayout visualisation after placing a product
        CargolayoutService.visualizeCargoLayout(this.cargoSpace, this.cargolayout)

        // Add the current cargolayout as a step to the cargolayoutSteps variable
        // JSON.parse/JSON.stringify is used because otherwise it will update a previous step when updating the cargolayout
        this.cargolayoutSteps.push(JSON.parse(JSON.stringify(this.cargolayout)));

        let orderInfo = JSON.parse(JSON.stringify(order));
        orderInfo.products = [];
        this.loadList.push(new LoadItem(productCounter, orderInfo, productToPlace)) // Add item to load list

        /**
         * Remove product of productsList (because it is already placed)
         * If product of the current order is 0 after removing the current product, then remove the whole order (shift),
         *  because then we can go to the next order
         */
        const alreadyLoopedProducts: Product[] = [];

        // Filter current product out of products array
        this.orderList[0].products = this.orderList[0].products.filter (loopedProduct => {
            if (alreadyLoopedProducts.indexOf(loopedProduct) > -1) {
                return true;
            }

            alreadyLoopedProducts.push (loopedProduct)

            return loopedProduct !== productToPlace;
        })

        // Remove whole order if no other products left
        if (this.orderList[0].products.length === 0) {
            this.orderList.shift();
        }
    }

    /**
     * Remove an already placed product of the cargolayout by loadlist number
     */
    protected removeProduct(
        productCounter: number
    ): void
    {
        this.log("Before removing product with nr: " + productCounter)

        const loadItem = this.getLoadItemById (productCounter) // Get loaditem based on nr

        if (loadItem) {
            this.log(`Remove p.id = ${loadItem.product.id} of cargolayout`)

            this.removeProductOfCargolayout (productCounter, loadItem.product);
            this.addPlacedOrderBackToOrderlist (loadItem);

            // Remove product from loadlist (because it isn't placed anymore)
            this.loadList = this.loadList.filter (li => li.id !== productCounter);

            // JSON.parse / JSON.stringify used to remove the reference to the original object
            // Notice that the step to place this product will not be removed of the steps array
            // We add another step where the product is removed, so we can replay the steps later
            this.cargolayoutSteps.push(JSON.parse(JSON.stringify(this.cargolayout)));
        }
    }

    /**
     * Check if the cargo space is fully full (chance is very low that this is true)
     */
    protected checkIfCargoSpaceIsFull(): boolean {
        const amountOfTotalVoxels = this.cargoSpace.gridHeight * this.cargoSpace.gridWidth * this.cargoSpace.gridLength;
        return amountOfTotalVoxels === this.amountOfFilledVoxels;
    }

    /**
     * Get loaditem by id
     */
    private getLoadItemById(
        id: number
    ): LoadItem
    {
        return this.loadList.find (loadItem => loadItem.id === id)
    }

    /**
     * Check on which side of the cargospace the voxel needs to be filled
     * Add voxel box weight to the corresponding side
     */
    private addWeightPerVoxelToSide(
        xx:             number,
        weightPerVoxel: number,
        leftTotal:      number,
        rightTotal:     number
    ): LeftAndRightWeightInKg
    {
        /*
         * If the x axis of the current voxel is less than the start x value of the right side AND the width is odd, then add the weight of the voxel to the left side
         * Or if the x axis of the current voxel is less than the start x value of the right side minus one, then add the weight of the voxel to the left side
        */
        if (this.cargoSpace.gridWidth % 2 === 0 && xx < this.rightBeginX ||
            xx < this.rightBeginX - 1) {
            leftTotal += weightPerVoxel;
        }

        // If the x axis of the current voxel is equal or more than the start x value of the right side, then add the weight of the voxel to the right side
        if (xx >= this.rightBeginX) {
            rightTotal += weightPerVoxel;
        }

        // Return the new side weight values as an object
        return [leftTotal, rightTotal]
    }

    /*
     * Function that checks of placing the product will result in an evenly distributed cargo and will not break other products
     * Requirement 1. Placing the product on the specified location should result in an evenly distributed cargo
     * Requirement 2. Placing a product on top of another is only possible if the product(s) below can manage the weight of the product to place
    */
    private productMeetsWeightRequirements(
        coordinates:    Coordinates,
        productToPlace: Product,
        start_y:        number,
        tempTotalLeft:  number,
        tempTotalRight: number
    ): CanPlaceAndLeftAndRightWeightInKg
    {
        // Calculate weight per voxel
        const weightPerVoxel = productToPlace.weight / (productToPlace.gridWidth * productToPlace.gridLength);

        const yFloor = start_y === 0; // True if voxel is located on the first (0th) layer, false if y > 0

        // Get weight of left and right side when placing the product
        const [tempLeftWeight, tempRightWeight] = this.addWeightPerVoxelToSide(
            coordinates.x,
            weightPerVoxel,
            tempTotalLeft,
            tempTotalRight
        );

        if (!yFloor)
        {
            /**
             * Check if the product to place is not heavier than the product one layer below
             *  because a heavier product cannot be placed on top of a product that weighs less
             */
            if (!this.checkIfProductBelowCanHandleWeightOfProductToPlace (coordinates, productToPlace)) {
                return [false, [0, 0]]
            }

            /*
              * If the product to place is placed on top of other products, we need to check if all products
              *  below can manage the sum of the weight on top of it
            */
            if (!this.checkIfProductsBelowCanHandleWeightOnTopOfIt(coordinates, productToPlace)) {
                return [false, [0, 0]]
            }
        }

        return [
            true,
            [
                tempLeftWeight,
                tempRightWeight
            ]
        ]
    }

    /*
     * Place product in the cargo layout when it meets all requirements
     */
    private loopCoordinatesAndPlaceProductInCargolayout(
        productCounter:     number,
        coordinatesWeight:  CoordinatesWeight
    ): void
    {
        // Loop saved coordinates
        coordinatesWeight.voxelCoordinatesArray.forEach ((coordinates) => {
            // Place an unique number (unique for this layout) in the cargospace 'voxel' to recognize which product is planned on what location
            this.cargolayout[coordinates.y][coordinates.z][coordinates.x] = productCounter.toString().padStart(2, "0");

            this.updateHighestYValue (coordinates.y); // Update highest y value

            this.amountOfFilledVoxels++;  // Add one to the amountOfFilledVoxels
        })

        // Add voxel weight to the right side
        this.leftWeightTotal    = coordinatesWeight.leftWeight;
        this.rightWeightTotal   = coordinatesWeight.rightWeight;
    }

    /*
     * Remove an already placed product of the cargolayout
     */
    private removeProductOfCargolayout(
        productCounter: number,
        productToPlace: Product
    ): void
    {
        // Calculate weight of the product per voxel
        const weightPerVoxel = productToPlace.weight / (productToPlace.gridWidth * productToPlace.gridHeight * productToPlace.gridLength);

        const [yCoordinates, xCoordinates, zCoordinates] = this.direction.getAllCargospaceYAndXAndZVoxelsToLoop(this.cargoSpace);
        const coordinatesOfAllVoxelsToLoop = this.direction.cartesianVoxels(yCoordinates, xCoordinates, zCoordinates);

        // Loop all voxels
        for (const coordinatesArray of coordinatesOfAllVoxelsToLoop)
        {
            const [x, y, z] = this.direction.getXYZOfCartesianLoop(coordinatesArray);

            const productCounterValue = productCounter.toString().padStart(2, "0");

            // If loadlist number is equal to the voxel value, change voxel value back to '00'
            if (this.cargolayout[y][z][x] === productCounterValue)
            {
                this.cargolayout[y][z][x] = '00';

                // Remove weight per box of the left weight total
                if (this.cargoSpace.gridWidth % 2 === 0 && x < this.rightBeginX ||
                    x < this.rightBeginX - 1
                ) {
                    this.leftWeightTotal -= weightPerVoxel;
                }

                // Remove weight per box of the right weight total
                if (x >= this.rightBeginX) {
                    this.rightWeightTotal -= weightPerVoxel;
                }

                // Remove one of the amount of filled voxels
                this.amountOfFilledVoxels--;
            }
        }

        this.updateWeights();
    }

    /*
     * Add placed product (coming from an order) back to the orderlist so it can be placed again on another location
     */
    private addPlacedOrderBackToOrderlist(
        loadItem: LoadItem
    ): void
    {
        // Check if there is still an order in the orderlist with the same order id
        const loadItemIsInList = this.orderList.find (o => o.id === loadItem.order.id)

        if (loadItemIsInList) {
            // If the order is available in the list, add product in front (unshift) of order.products array
            this.orderList[0].products.unshift(loadItem.product);
        }
        else
        {
            // If the order is not existing anymore (because the whole order was already processed), "recreate" the order and add the product
            const order = loadItem.order;

            order.products.push(loadItem.product);
            this.orderList.unshift(order)
        }
    }

    /*
     * Check if the product below the voxel to fill can handle the weight of the product to place
     * With 'handle' we mean that the weight on top is less than the product's weight
     */
    private checkIfProductBelowCanHandleWeightOfProductToPlace(
        coordinates:    Coordinates,
        productToPlace: Product
    ): boolean
    {
        // Get corresponding loaditem below current one (yy-1)
        const loadItem = this.getLoadItemById (+this.cargolayout[coordinates.y - 1][coordinates.z][coordinates.x])
        const loadItemWeightPerVoxel = loadItem.product.maxLoadWeight / (loadItem.product.gridLength * loadItem.product.gridWidth);
        const productWeightPerVoxel = productToPlace.weight / (productToPlace.gridLength * productToPlace.gridWidth);

        // Return false if product on top weighs more
        return loadItem && loadItemWeightPerVoxel >= productWeightPerVoxel || !loadItem;
    }

    /*
     * Check if product can handle the weight of the products on top of it (including the product to place)
     */
    private checkIfProductsBelowCanHandleWeightOnTopOfIt(
        coordinates:    Coordinates,
        productToPlace: Product
    ): boolean
    {
        for (let layerLoop = 0; layerLoop < coordinates.y; layerLoop++)
        {
            const loopedProductCounters = [];

            const productCounter = +this.cargolayout[layerLoop][coordinates.z][coordinates.x];
            loopedProductCounters.push(productCounter)

            // Get weight of product on bottom layer
            const loadItemBottom = this.getLoadItemById(productCounter);
            const weightOfBottomPerVoxel = loadItemBottom?.product.maxLoadWeight / (loadItemBottom?.product.gridWidth * loadItemBottom?.product.gridLength);

            let weightOnTop = productToPlace.weight / (productToPlace.gridWidth * productToPlace.gridLength);

            for (let layerTopLoop = layerLoop + 1; layerTopLoop < coordinates.y; layerTopLoop++)
            {
                const productCounter2 = +this.cargolayout[layerTopLoop][coordinates.z][coordinates.x];
                if(loopedProductCounters.indexOf(productCounter2) === -1){
                    loopedProductCounters.push(productCounter2);

                    const loadItemTop = this.getLoadItemById(productCounter2);
                    const weightTopVoxel = loadItemTop.product.weight / (loadItemTop.product.gridWidth * loadItemTop.product.gridLength);
                    weightOnTop += weightTopVoxel;
                }
            }

            if (weightOnTop > weightOfBottomPerVoxel) {
                return false;
            }
        }
        return true; // Return true if all weight requirements are passed
    }

    /*
     * Update the highestYValue variable if the current y value is bigger than the current highestYValue
     */
    private updateHighestYValue(
        currentY: number
    ): void
    {
        if (currentY > this.highestYValue) {
            this.highestYValue = currentY;
        }
    }

    /*
     * Check if filling the voxel meets the measurement requirements
     * Return true if voxel is empty and not floating
     */
    private checkIfFillingVoxelMeetsMeasurementRequirements(
        startY:        number,
        coordinates:    Coordinates
    ): boolean
    {
        /*
         * Check if voxel is not empty, because if the voxel is already filled we cannot place another product on that location
         */
        if (this.cargolayout[coordinates.y][coordinates.z][coordinates.x] !== '00')
        {
            this.log(`Voxel with coordinates: (y=${coordinates.y}, z=${coordinates.z}, x=${coordinates.x}) is already filled with another product`);
            return false;
        }

        /*
          * Validate if product is not floating in the air
          * With 'floating' we mean that the product is not placed on the floor (so yy > 0) and that the voxel with coordinates(yy - 1, zz, xx) is not empty ('00')
          * We only need to check this for the first layer of the product to place (that's why yy === y is added)
          *  because if we don't do this and the product's height is more than 1 voxel, then the condition will be true while checking the second layer
          * We use y - 1 to look at the voxel below the one where we would like to place the product
        */
        return  coordinates.y <= 0 ||
                coordinates.y !== startY ||
                this.cargolayout[coordinates.y - 1][coordinates.z][coordinates.x] !== '00';
    }

    /*
     * Return true if y is 0 OR y is bigger than 0 and voxel below current one is not empty
     */
    protected yIsNotFloating(
        coordinates: Coordinates
    ): boolean
    {
        return  coordinates.y === 0 ||
                coordinates.y > 0 && this.cargolayout[coordinates.y - 1][coordinates.z][coordinates.x] !== '00';
    }

    /*
     * Check if the cargo is evenly distributed when placing all voxels
     */
    private cargoIsStillEvenlyDistributed(
        tempTotalLeft:  number,
        tempTotalRight: number
    ): boolean
    {
        // Calculate the new weight percents
        const totalTempWeight   = tempTotalLeft     +   tempTotalRight;
        const leftPercentage    = tempTotalLeft     /   totalTempWeight * 100;
        const rightPercentage   = tempTotalRight    /   totalTempWeight * 100;

        this.log(`tmpleft=${tempTotalLeft}, tmpright=${tempTotalRight}, leftperct=${leftPercentage}, rightperct=${rightPercentage}`)
        this.log(`rightWeightTotal=${this.rightWeightTotal}, leftWeightTotal=${this.leftWeightTotal}`)

        /*
         * If the difference between one side relative to the other side is more than 20%, then return false
         *   because the cargo is not evenly distributed anymore
         */
        return  this.rightWeightTotal === 0 ||
                this.leftWeightTotal === 0 ||
                leftPercentage >= rightPercentage && leftPercentage - rightPercentage <= 20 ||
                rightPercentage >= leftPercentage && rightPercentage - leftPercentage <= 20
    }

    /*
     * Check if placing (a part of) the product in the specified voxel meets the weight and measurement requirements
     */
    private meetsAllRequirements(
        start_y:        number,
        coordinates:    Coordinates,
        productToPlace: Product,
        tempTotalLeft:  number,
        tempTotalRight: number
    ): CanPlaceAndLeftAndRightWeightInKg
    {
        // Check measurement requirements
        if (!this.checkIfFillingVoxelMeetsMeasurementRequirements (start_y, coordinates)) {
            return [false, [0, 0]]
        }

        // Check weight requirements
        const [fits, [tempLeftWeight, tempRightWeight]] = this.productMeetsWeightRequirements (
            coordinates,
            productToPlace,
            start_y,
            tempTotalLeft,
            tempTotalRight)

        // Return false if the product to place does not meet the weight requirements, If false is returned, we do nothing with the temporary weights
        if(!fits) {
            return [false, [0, 0]]
        }

        return [
            true,
            [
                tempLeftWeight,
                tempRightWeight
            ]
        ]
    }

    private returnCoordinatesWeight (
        tempTotalLeft:          number,
        tempTotalRight:         number,
        voxelCoordinatesArray:  Coordinates[]
    ): CanPlaceAndCoordinatesWeight
    {
        // Check if the loading is evenly distributed on the last product
        if(this.order.products.length === 1) {
            return [
                this.cargoIsStillEvenlyDistributed (tempTotalLeft, tempTotalRight),
                {
                    voxelCoordinatesArray:  voxelCoordinatesArray,
                    leftWeight:             tempTotalLeft,
                    rightWeight:            tempTotalRight
                }
            ];
        }

        return [
            true,
            {
                voxelCoordinatesArray:  voxelCoordinatesArray,
                leftWeight:             tempTotalLeft,
                rightWeight:            tempTotalRight
            }
        ];

    }

    protected voxelIsAlreadyFilled(
        coordinates: Coordinates
    ): boolean
    {
        return this.cargolayout[coordinates.y][coordinates.z][coordinates.x] !== '00';
    }

    protected getProductObj(product: Product): {} {
        return {
            width:          product.gridWidth,
            height:         product.gridHeight,
            length:         product.gridLength,
            weight:         product.weight,
            maxLoadWeight:  product.maxLoadWeight,
            units:          product.units
        };
    }

    public async saveCargoplanning(): Promise<string> {
        const cargoPlanning: CargoPlanning = {
            id:                         '',
            cargoLayout:                this.cargolayout,
            loadList:                   this.loadList,
            loadDistribution:           {leftKg: this.leftWeightTotal, rightKg: this.rightWeightTotal},
            cargoLayoutSteps:           this.cargolayoutSteps,
            cargoSpace:                 this.cargoSpace,
            completed:                  null,
            createdOn:                  new Date(),
            note:                       '',
            title:                      this.title,
            loadDirection:              this.loadDirection,
            amountOfOriginalProducts:   this.amountOfOriginalProducts
        };

        // Call saveNewCargoPlanning of the dependency injected database implementation
        return this.planDAO.saveNewCargoPlanning(cargoPlanning)
    }

    public getResponseValues(): {
        loadDistribution:           {leftKg: number, rightKg: number};
        cargolayoutSteps:           CargoLayout[];
        loadList:                   LoadItem[];
        cargoLayout:                CargoLayout;
        cargoSpace:                 Cargospace;
        amountOfOriginalProducts:   number;
    } {
        return {
            loadDistribution:           {leftKg: this.leftWeightTotal, rightKg: this.rightWeightTotal},
            cargolayoutSteps:           this.cargolayoutSteps,
            loadList:                   this.loadList,
            cargoLayout:                this.cargolayout,
            cargoSpace:                 this.cargoSpace,
            amountOfOriginalProducts:   this.amountOfOriginalProducts
        }
    }

    private allVoxelsAreEmpty(
        coordinatesArray: []
    ): boolean
    {
        for (const coordinateArray of coordinatesArray)
        {
            const [x, y, z] = this.direction.getXYZOfCartesianLoop(coordinateArray);
            const coordinate = new Coordinates(y, x, z);

            if (!this.productFitsCargospaceBoundaries(coordinate) || this.voxelIsAlreadyFilled(coordinate)) {
                return false;
            }
        }
        return true;
    }

    /*
     * Check if product can be placed on current coordinates
     * If product can be placed (wasPlaced) but reponse is false, then remove the placed product and try another location
     */
    protected canPlaceProduct(
        coordinates:    Coordinates,
        productToPlace: Product,
        toRight:        boolean
    ): boolean
    {
        const currentProductCounter = this.loadList.length + 1;

        // If direction is right, then check right requirements, otherwise check left requirements
        if (toRight)
        {
            // First check if placing the product to the right fits. If not, then try it to the left
            const [meetsRightRequirements, rightCoordinatesWeight] = this.productMeetsRightRequirements(coordinates, productToPlace);
            if (meetsRightRequirements)
            {
                // If product meets right measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, rightCoordinatesWeight)

                return true;
            }

            const [meetsLeftRequirements, leftCoordinatesWeight] = this.productMeetsLeftRequirements (coordinates, productToPlace);
            if (meetsLeftRequirements)
            {
                // If product meets left measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, leftCoordinatesWeight)

                return true;
            }
        }
        else
        {
            // First check if placing the product to the left fits. If not, then try it to the right
            const [meetsLeftRequirements, leftCoordinatesWeight] = this.productMeetsLeftRequirements (coordinates, productToPlace);
            if (meetsLeftRequirements)
            {
                // If product meets left measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, leftCoordinatesWeight)

                return true;
            }

            const [meetsRightRequirements, rightCoordinatesWeight] = this.productMeetsRightRequirements(coordinates, productToPlace);
            if (meetsRightRequirements)
            {
                // If product meets right measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct (currentProductCounter, productToPlace, this.order, rightCoordinatesWeight)

                return true;
            }
        }

        return false;
    }

    private canHandleEmergencyStop(
        startCoordinates:   Coordinates,
        xCoordinates:       number[],
        yCoordinates:       number[],
        productToPlace:     Product
    ): boolean
    {
        const start_z = startCoordinates.z;

        if(start_z === 0 || productToPlace.units < this.config.unitBoundary || !this.emergencyStop || productToPlace.weight < this.config.weightBoundary) {
            return true;
        }

        // Startcoordinates z - 1 (item in front)
        const zVoxelInFront = start_z - 1;

        // Get voxels to loop
        const voxelsToLoop = this.direction.cartesianVoxels(yCoordinates, xCoordinates, [zVoxelInFront]);

        for (const voxel of voxelsToLoop) {
            const [x, y, z] = this.direction.getXYZOfCartesianLoop(voxel);

            const loadItemInFront = this.getLoadItemById (+this.cargolayout[y][z][x])

            if(loadItemInFront) {
                const heightOfLoadItemInFront = loadItemInFront.product.gridHeight;
                if (productToPlace.gridHeight >= (heightOfLoadItemInFront * 3)) {
                    return false;
                }
            } else {
                return false
            }
        }

        return true;
    }

    private updateWeights() {
        const totalLoadedWeight = this.leftWeightTotal + this.rightWeightTotal;
        if (this.leftWeightTotal > 0) {
            this.leftPerctWeight    =   this.leftWeightTotal    /   totalLoadedWeight * 100;
        } else {
            this.leftPerctWeight = 0;
        }

        if (this.rightWeightTotal > 0) {
            this.rightPerctWeight   =   this.rightWeightTotal   /   totalLoadedWeight * 100;
        } else {
            this.rightPerctWeight = 0;
        }
    }
}
