import {CoordinatesWeight} from '../models/coordinatesWeight.model';

/*
 * TS class where common types are located
 */
export type CanPlaceAndCoordinatesWeight = [boolean, CoordinatesWeight]
export type LeftAndRightWeightInKg = [number, number];
export type YXAndZDimensionArrays = [number[], number[], number[]];
export type YAndZDimensionArrays = [number[], number[]];
export type CanPlaceAndLeftAndRightWeightInKg = [boolean, LeftAndRightWeightInKg];
export type CanPlaceAndWasPlaced = [boolean, boolean]
export type CargoLayout = string[][][];
export type CargoSettings = {algorithm: string, timer: number, emergencyStop: boolean}
