import {Product} from './product.model';
import {Order} from './order.model';

/**
 * Model that represents a loaded item (this will be used for the loadlist)
 */
export class LoadItem {
    constructor(
        public readonly id:         number,
        public readonly order:      Order,  // Order were the current product is part of
        public readonly product:    Product // Product that will be placed
    ) {}
}
