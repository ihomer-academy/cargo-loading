import {Coordinates} from './coordinates.model';

/**
 * Interface that contains an array with coordinates and the total weight of the left and right side
 */
export interface CoordinatesWeight {
    voxelCoordinatesArray:  Coordinates[];  // coordinates that will be filled when placing the current product
    leftWeight:             number;         // left weight distribution
    rightWeight:            number;         // right weight distribution
}
