/**
 * Model that represents the coordinates of a voxel
 */
export class Coordinates {
    constructor(
        public readonly y:  number, // height coordinates
        public readonly x:  number, // width coordinates
        public readonly z:  number  // length coordinates
    ) {}
}
