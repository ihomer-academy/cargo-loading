/**
 * Interface that will be used for the configuration with dependency injection
 */
export interface Config {
    voxelSize:      number; // Amount of cm that will represent one voxel
    unitBoundary:   number; // Value (together with weightboundary) that decides when a product is a risky product when having an emergencystop
    weightBoundary: number; // Value (together with unitboundary) that decides when a product is a risky product when having an emergencystop
}
