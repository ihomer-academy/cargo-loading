import {Product} from './product.model';

/**
 * Model that represents an order (an order can contain multiple products)
 */
export class Order {
    constructor(
        public readonly id:         number,
        public products:            Product[]
    ) {}
}
