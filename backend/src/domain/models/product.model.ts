import {ProductOrientation} from './enum/product.orientation.enum';
import {InjectValue} from 'typescript-ioc';
import {Config} from './config.model';

/**
 * Model that represents a product
 */
export class Product {
    @InjectValue("Configuration") config!: Config // Dependency injection

    gridLength:     number; // Amount of virtual voxels
    gridWidth:      number; // Amount of virtual voxels
    gridHeight:     number; // Amount of virtual voxels

    constructor(
        public readonly id:             number,
        public readonly name:           string,
        public readonly length:         number,         // (in cm)
        public readonly width:          number,         // (in cm)
        public readonly height:         number,         // (in cm)
        public readonly weight:         number,         // (in kg)
        public readonly maxLoadWeight?: number,         // Weight the product can maximum handle on top of itself (in kg)
        public readonly units:          number  =   1   // Amount of units (for example: a pallet with 800 bricks, is 800 units)
    ) {
        // Convert the real dimensions to virtual dimensions (= amount of voxels)
        this.gridLength     = this.calculateAmountOfVoxels(length)
        this.gridWidth      = this.calculateAmountOfVoxels(width)
        this.gridHeight     = this.calculateAmountOfVoxels(height)

        // If there is no maxLoadWeight passed, then the maxloadweight will be the same as their own weight
        if(maxLoadWeight) {
            this.maxLoadWeight = maxLoadWeight;
        } else {
            this.maxLoadWeight = weight;
        }
    }

    /**
     * Function to converts the real dimensions to virtual dimensions
     */
    private calculateAmountOfVoxels(value: number): number {
        return Math.ceil(value / this.config.voxelSize);
    }

    /**
     * Function to change the orientation of a product
     */
    changeOrientation(o: number) {
        switch (o)
        {
            case ProductOrientation.LONGEST_SIDE_IS_WIDTH:
                if(this.length > this.width) {
                    // If length is bigger than width, than make gridWidth equal to the length
                    this.gridLength = this.calculateAmountOfVoxels(this.width)
                    this.gridWidth  = this.calculateAmountOfVoxels(this.length)
                } else {
                    // Make gridWidth equal to the width
                    this.gridLength = this.calculateAmountOfVoxels(this.length)
                    this.gridWidth  = this.calculateAmountOfVoxels(this.width)
                }
                break;
            case ProductOrientation.LONGEST_SIDE_IS_LENGTH:
                if(this.length > this.width) {
                    // If length is bigger than width, than make gridWidth equal to the width
                    this.gridLength = this.calculateAmountOfVoxels(this.length)
                    this.gridWidth  = this.calculateAmountOfVoxels(this.width)
                } else {
                    // Make gridWidth equal to the length
                    this.gridLength = this.calculateAmountOfVoxels(this.width)
                    this.gridWidth  = this.calculateAmountOfVoxels(this.length)
                }
                break;
            default:
                break;
        }
    }
}
