/**
 * Error model that will be used when there need to be an error returned with additional information
 */
export class BaseError {
    constructor(
        public readonly message:            string,
        public readonly originalAction?:    string,
        public readonly body?:              object
    ) {}
}
