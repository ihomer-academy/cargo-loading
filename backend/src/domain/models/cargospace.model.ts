import {InjectValue} from 'typescript-ioc';
import {Config} from './config.model';

/**
 * Model that represents a cargospace (= the space to be loaded)
 */
export class Cargospace {
    @InjectValue("Configuration") config!: Config // Dependency injection for the configuration

    readonly gridLength:    number;         // Amount of virtual voxels
    readonly gridWidth:     number;         // Amount of virtual voxels
    readonly gridHeight:    number;         // Amount of virtual voxels

    constructor(
        public readonly id:         number,
        public readonly name:       string,
        public readonly length:     number, // length in cm of the cargospace
        public readonly width:      number, // width in cm of the cargospace
        public readonly height:     number, // height in cm of the cargospace
        public readonly maxWeight:  number  // maximum weight that the cargospace can handle (in kg)
    ) {
        /**
         * The real length, width and height of the cargospace will be converted to a virtual version.
         * Math.floor is used to round down the virtual value
         * For example: length = 625 cm, voxel size = 50 (cm), then the virtual length will be 12 (625 / 50).
         * We don't want more voxels in the cargospace than there in reality is, because the product won't fit then
         */
        const VOXELSIZE =   this.config.voxelSize;
        this.gridLength =   Cargospace.calculateAmountOfVoxels(length, VOXELSIZE)
        this.gridWidth  =   Cargospace.calculateAmountOfVoxels(width, VOXELSIZE)
        this.gridHeight =   Cargospace.calculateAmountOfVoxels(height, VOXELSIZE)
    }

    /**
     * Function to calculate the amount of voxels based on the real measurements
     */
    private static calculateAmountOfVoxels(
        value: number,
        voxelSize: number
    ) {
        return Math.floor(value / voxelSize) // .floor rounds the value down to a whole number (2.5 will be 2)
    }
}
