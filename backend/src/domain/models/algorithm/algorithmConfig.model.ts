import {Order} from '../order.model';
import {Cargospace} from '../cargospace.model';

/**
 * Model that contains the configurations for the plan
 */
export interface AlgorithmConfig {
    algorithm:      string;
    timer:          number;     // amount of seconds that the user gave the system to make a plan
    emergencyStop:  boolean;    // take into account the height/amount of units when making the plan
    orders:         Order[];
    cargoSpace:     Cargospace;
    title:          string;     // title that the planner gave to the plan
    direction:      string;     // load direction, for example via tailgate or via sides
}
