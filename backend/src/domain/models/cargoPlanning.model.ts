import {LoadItem} from './loaditem.model';
import {CargoLayout} from '../types/types';
import {Cargospace} from './cargospace.model';

/**
 * Model that will be used as response for the created cargo plan
 */
export interface CargoPlanning {
    id: string,
    cargoLayout:             CargoLayout,                           // final layout
    loadList:                LoadItem[],                            // list with the sequence of products to load
    loadDistribution:        { leftKg: number; rightKg: number },   // cargo distribution in the cargospace
    cargoLayoutSteps:        CargoLayout[],                         // steps to create final layout (first, there is 1 product, second, 2..)
    cargoSpace:              Cargospace,
    completed:               boolean,                               // is true when loader used the layout while loading
    createdOn:               Date,
    note:                    string,                                // note that the planner filled in for the loader
    title:                   string,                                // title that the planner filled in to recognize this plan
    loadDirection:           string,                                // load direction, for example via tailgate or via sides
    amountOfOriginalProducts:number                                 // amount of products that should be placed
}