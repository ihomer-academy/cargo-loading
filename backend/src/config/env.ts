export default {
    VOXELSIZE: process.env.VOXELSIZE || 50,
    UNITBOUNDARY: process.env.UNITBOUNDARY || 20,
    WEIGHTBOUNDARY: process.env.WEIGHTBOUNDARY || 500
}