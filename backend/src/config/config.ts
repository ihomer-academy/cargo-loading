import yaml from 'js-yaml';
import fs from 'fs';
import {Config} from '../domain/models/config.model';
import path from 'path';
import env from './env';

/*
 * TS class that chooses the right configuration file and returns it for DI
 */
export default function(
    test = false
) {
    let constants;

    if(test) {
        // Set test constants
        constants = yaml.load(fs.readFileSync(path.resolve('./src/config/test-constants.yml'), 'utf8'));
    } else {
        // Get constants template
        constants = yaml.load(fs.readFileSync(path.resolve('./src/config/constants.yml'), 'utf8'));

        // Get environment variables
        const voxelSize = env.VOXELSIZE;
        const unitBoundary = env.UNITBOUNDARY;
        const weightBoundary = env.WEIGHTBOUNDARY;

        // Set constant values
        constants.voxelSize = voxelSize;
        constants.unitBoundary = unitBoundary;
        constants.weightBoundary = weightBoundary;
    }

    return constants as Config;
}
