import {PlanDao} from './datastorage/plan/plan.dao';

const express = require('express')
import { Request, Response } from 'express'
const bodyParser = require('body-parser')
const morgan = require('morgan')
import PlanRoutes from './controller/routes/plan.routes'
const env_config = require('./config/environment-config.json')
const cors = require('cors');
const path = require('path');
import { Container } from "typescript-ioc";
import Config from './config/config';
import {MongoPlanDao} from './datastorage/plan/mongoDB/mongo.plan.dao';

let app = express()
app.disable("x-powered-by");

app.use(cors({
    origin: 'http://localhost:4200'
}));

app.use(express.static(path.join(__dirname, '../')));

app.use(morgan('dev'))
app.use(bodyParser.json())

// Dependency injection
Container.bindName('Configuration').to(Config()) // Bind Config object (based on yml file) to name 'Configuration'
Container.bind(PlanDao).to(MongoPlanDao);

// Routes
app.use('/api/plan', PlanRoutes)

// Accept requests of the client
app.use(function(req: Request, res: Response, next: any) {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
    next();
});

app.listen(env_config.port, () => console.log(`Cargo load backend listening on port ${env_config.port}!`))

// module.exports = app
export default app
