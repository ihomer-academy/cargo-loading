import chai = require('chai');
const expect = chai.expect;
import app from '../../src/app';
import init = require('../init')
import chaiHttp = require('chai-http')
import {Cargospace} from '../../src/domain/models/cargospace.model';
import {Product} from '../../src/domain/models/product.model';
import {Order} from '../../src/domain/models/order.model';
chai.use(chaiHttp);

init

const planEndpoint = '/api/plan' // Arrange

describe('Plan controller', () => {

    describe('Backtracking', () => {
        describe('ZXY', () => {
            it('D-TC01: should return the expected information when having two orders that can be placed easily (without BT)', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":100,"width":50,"height":50,"weight":400},
                                    {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":800}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"backtracking",
                            timer:600
                        },
                        "loadDirection": 'z-x-y'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '03', '03', '02' ],
                                [ '00', '04', '04' ],
                                [ '00', '04', '04' ]
                            ]
                        ]
                        const loadList = [
                            {
                                "id": 1,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":1,"name":"Product 1","length":100,"maxLoadWeight":800,"width":100,"height":50,"weight":800, gridWidth: 2, gridHeight: 1, gridLength: 2, "units": 1}
                            },
                            {
                                "id": 2,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":2,"name":"Product 2","length":150,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 1, gridHeight: 1, gridLength: 3, "units": 1}
                            },
                            {
                                "id": 3,
                                order:{"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":3,"name":"Product 3","length":100,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 2, gridHeight: 1, gridLength: 1, "units": 1}
                            },
                            {
                                "id": 4,
                                order:{"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":4,"name":"Product 1","length":100,"maxLoadWeight":800,"width":100,"height":50,"weight":800, gridWidth: 2, gridHeight: 1, gridLength: 2, "units": 1}
                            }
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(res.body.loadDistribution).to.eql({leftKg: 600, rightKg: 800})
                        expect(res.body.loadList).to.eql(loadList)

                        done();
                    });
            })

            it('D-TC01: should return the expected information when having two orders that can be placed completely with BT', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":150,"width":50,"height":50,"weight":300},
                                    {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":400}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"backtracking",
                            timer:600
                        },
                        "loadDirection": 'z-x-y'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '03', '03', '02' ],
                                [ '03', '03', '00' ],
                                [ '04', '04', '04' ]
                            ],
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(Math.round(res.body.loadDistribution.leftKg)).to.equal(700)
                        expect(Math.round(res.body.loadDistribution.rightKg)).to.equal(500)
                        expect(res.body.cargoLayoutSteps.length).to.above(4)
                        expect(res.body.amountOfOriginalProducts).to.be.equal(res.body.loadList.length)

                        done();
                    });
            })

            describe('TC26: EmergencyStop', () => {
                it('TC26.1: should take into account the amount of units and weight when checking the emergencyStop option', (done) => {
                    // arrange
                    const testCargospace = new Cargospace(1, 'Cargospace 1', 250, 150, 150, 40000);
                    const p1 = new Product(1, 'p1', 100, 100, 50, 400, 400);
                    const p2 = new Product(2, 'p2', 100, 50, 50, 200, 200);
                    const p3 = new Product(3, 'p3', 150, 100, 150, 600, 600, 20);

                    const orders = [
                        new Order(1, [p1, p2, p3]),
                    ];

                    // Act
                    chai.request(app)
                        .post(planEndpoint)
                        .send({
                            "orders": orders,
                            "cargospace": testCargospace,
                            "settings":{
                                algorithm:"backtracking",
                                timer:600,
                                emergencyStop: true,
                            },
                            "loadDirection": 'z-x-y'
                        })
                        .end((err, res) => {
                            // Assert
                            expect(res.status).to.equal(200);
                            expect(res.body).to.haveOwnProperty('cargoLayout')
                            expect(res.body).to.haveOwnProperty('loadList')
                            expect(res.body).to.haveOwnProperty('loadDistribution')

                            const expectedCargolayout = [
                                [
                                    ["01", "01", "02"],
                                    ["01", "01", "02"],
                                    ["01", "01", "00"],
                                    ["00", "03", "03"],
                                    ["00", "03", "03"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ]
                            ];
                            expect(res.body.cargoLayout).to.eql(expectedCargolayout);

                            done()
                        })
                })
            })
        })

        describe('XYZ', () => {
            it('D-TC01: should return the expected information when having two orders that can be placed easily (without BT)', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":100,"width":50,"height":50,"weight":400},
                                    {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":800}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"backtracking",
                            timer:600
                        },
                        "loadDirection": 'x-y-z'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '03', '03', '02' ],
                                [ '00', '04', '04' ],
                                [ '00', '04', '04' ]
                            ]
                        ]
                        const loadList = [
                            {
                                "id": 1,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":1,"name":"Product 1","length":100,"maxLoadWeight":800,"width":100,"height":50,"weight":800, gridWidth: 2, gridHeight: 1, gridLength: 2, "units": 1}
                            },
                            {
                                "id": 2,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":2,"name":"Product 2","length":150,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 1, gridHeight: 1, gridLength: 3, "units": 1}
                            },
                            {
                                "id": 3,
                                order:{"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":3,"name":"Product 3","length":100,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 2, gridHeight: 1, gridLength: 1, "units": 1}
                            },
                            {
                                "id": 4,
                                order:{"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":4,"name":"Product 1","length":100,"maxLoadWeight":800,"width":100,"height":50,"weight":800, gridWidth: 2, gridHeight: 1, gridLength: 2, "units": 1}
                            }
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(res.body.loadDistribution).to.eql({leftKg: 600, rightKg: 800})
                        expect(res.body.loadList).to.eql(loadList)

                        done();
                    });
            })

            it('D-TC01: should return the expected information when having two orders that can be placed completely with BT', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":150,"width":50,"height":50,"weight":300},
                                    {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":400}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"backtracking",
                            timer:600
                        },
                        "loadDirection": 'x-y-z'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '03', '03', '02' ],
                                [ '03', '03', '00' ],
                                [ '04', '04', '04' ]
                            ],
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(Math.round(res.body.loadDistribution.leftKg)).to.equal(700)
                        expect(Math.round(res.body.loadDistribution.rightKg)).to.equal(500)
                        expect(res.body.cargoLayoutSteps.length).to.above(4)
                        expect(res.body.amountOfOriginalProducts).to.be.equal(res.body.loadList.length)

                        done();
                    });
            })

            describe('TC26: EmergencyStop', () => {
                it('TC26.1: should take into account the amount of units and weight when checking the emergencyStop option', (done) => {
                    // Arrange
                    const testCargospace = new Cargospace(1, 'Cargospace 1', 250, 150, 150, 40000);
                    const p1 = new Product(1, 'p1', 100, 100, 50, 400, 400);
                    const p2 = new Product(2, 'p2', 100, 50, 50, 200, 200);
                    const p3 = new Product(3, 'p3', 150, 100, 150, 600, 600, 20);

                    const orders = [
                        new Order(1, [p1, p2, p3]),
                    ];

                    // Act
                    chai.request(app)
                        .post(planEndpoint)
                        .send({
                            "orders": orders,
                            "cargospace": testCargospace,
                            "settings":{
                                algorithm:"backtracking",
                                timer:600,
                                emergencyStop: true,
                            },
                            "loadDirection": 'x-y-z'
                        })
                        .end((err, res) => {
                            // Assert
                            expect(res.status).to.equal(200);
                            expect(res.body).to.haveOwnProperty('cargoLayout')
                            expect(res.body).to.haveOwnProperty('loadList')
                            expect(res.body).to.haveOwnProperty('loadDistribution')

                            const expectedCargolayout = [
                                [
                                    ["01", "01", "02"],
                                    ["01", "01", "02"],
                                    ["01", "01", "00"],
                                    ["00", "03", "03"],
                                    ["00", "03", "03"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ]
                            ];
                            expect(res.body.cargoLayout).to.eql(expectedCargolayout);

                            done()
                        })
                })
            })
        })

        describe('Compare load directions', () => {
            it('D-TC12: should show the difference between ZXY and XYZ load direction', (done) => {
                // Arrange
                const orders = [
                    {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                            {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":600},
                            {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":600},
                            {"id":3,"name":"Product 3","length":150,"width":50,"height":50,"weight":600},
                            {"id":4,"name":"Product 4","length":100,"width":50,"height":50,"weight":200}],
                    }
                ];
                const cargospace = {
                    "id":1,"name":"Test laadruimte","length":350,"width":250,"height":50,"maxWeight":10000
                };

                let zxy_output,
                    xyz_output;

                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders": orders,
                        "cargospace": cargospace,
                        "settings":{
                            algorithm:"backtracking",
                            timer:600
                        },
                        "loadDirection": 'z-x-y'
                    })
                    .end((err, res) => {
                        zxy_output = res.body;

                        // Act
                        chai.request(app)
                            .post(planEndpoint)
                            .send({
                                "orders": orders,
                                "cargospace": cargospace,
                                "settings":{
                                    algorithm:"backtracking",
                                    timer:600
                                },
                                "loadDirection": 'x-y-z'
                            })
                            .end((err, res) => {
                                // Assert
                                xyz_output = res.body;

                                const expectedZxyCargoLayout = [
                                    [
                                        ['00', '01', '01', '02', '00'],
                                        ['00', '01', '01', '02', '00'],
                                        ['03', '03', '03', '02', '00'],
                                        ['00', '00', '00', '04', '04'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                    ]
                                ];

                                const expectedXyzCargoLayout = [
                                    [
                                        ['03', '01', '01', '02', '04'],
                                        ['03', '01', '01', '02', '04'],
                                        ['03', '00', '00', '02', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                    ]
                                ];

                                expect(zxy_output.cargoLayout).to.eql(expectedZxyCargoLayout);
                                expect(xyz_output.cargoLayout).to.eql(expectedXyzCargoLayout);

                                done();
                            });
                    });
            })
        });
    })

    describe('Greedy', () => {
        describe('ZXY', () => {
            it('D-TC01: should return the expected information when having two orders that can be placed easily', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":100,"width":50,"height":50,"weight":400},
                                    {"id":4,"name":"Product 4","length":100,"width":100,"height":50,"weight":800}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"greedy",
                            timer:600
                        },
                        "loadDirection": 'z-x-y'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '00', '03', '02' ],
                                [ '00', '03', '00' ],
                                [ '00', '00', '00' ]
                            ]
                        ]
                        const loadList = [
                            {
                                "id": 1,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":1,"name":"Product 1","length":100,"maxLoadWeight":800,"width":100,"height":50,"weight":800, gridWidth: 2, gridHeight: 1, gridLength: 2, "units": 1}
                            },
                            {
                                "id": 2,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":2,"name":"Product 2","length":150,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 1, gridHeight: 1, gridLength: 3, "units": 1}
                            },
                            {
                                "id": 3,
                                order:{"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":3,"name":"Product 3","length":100,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 1, gridHeight: 1, gridLength: 2, "units": 1}
                            }
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(res.body.loadDistribution).to.eql({leftKg: 400, rightKg: 400})
                        expect(res.body.loadList).to.eql(loadList)

                        done();
                    });
            })

            it('D-TC01: should return the expected information when having two orders that can not be placed completely with greedy', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":150,"width":50,"height":50,"weight":300},
                                    {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":400}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"greedy",
                            timer:600
                        },
                        "loadDirection": 'x-y-z'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '00', '03', '02' ],
                                [ '00', '03', '00' ],
                                [ '00', '03', '00' ]
                            ],
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(Math.round(res.body.loadDistribution.leftKg)).to.equal(400)
                        expect(Math.round(res.body.loadDistribution.rightKg)).to.equal(400)
                        expect(res.body.cargoLayoutSteps.length).to.equal(3)
                        expect(res.body.amountOfOriginalProducts).to.be.not.equal(res.body.loadList.length)

                        done();
                    });
            })

            describe('TC26: EmergencyStop', () => {
                it('TC26.1: should take into account the amount of units and weight when checking the emergencyStop option', (done) => {
                    // Arrange
                    const testCargospace = new Cargospace(1, 'Cargospace 1', 250, 150, 150, 40000);
                    const p1 = new Product(1, 'p1', 100, 100, 50, 400, 400);
                    const p2 = new Product(2, 'p2', 100, 50, 50, 200, 200);
                    const p3 = new Product(3, 'p3', 150, 100, 150, 600, 600, 20);

                    const orders = [
                        new Order(1,  [p1, p2, p3]),
                    ];

                    // Act
                    chai.request(app)
                        .post(planEndpoint)
                        .send({
                            "orders": orders,
                            "cargospace": testCargospace,
                            "settings":{
                                algorithm:"greedy",
                                timer:600,
                                emergencyStop: true,
                            },
                            "loadDirection": 'z-x-y'
                        })
                        .end((err, res) => {
                            // Assert
                            expect(res.status).to.equal(200);
                            expect(res.body).to.haveOwnProperty('cargoLayout')
                            expect(res.body).to.haveOwnProperty('loadList')
                            expect(res.body).to.haveOwnProperty('loadDistribution')

                            const expectedCargolayout = [
                                [
                                    ["01", "01", "02"],
                                    ["01", "01", "02"],
                                    ["01", "01", "00"],
                                    ["00", "03", "03"],
                                    ["00", "03", "03"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ]
                            ];
                            expect(res.body.cargoLayout).to.eql(expectedCargolayout);

                            done()
                        })
                })
            })
        })

        describe('XYZ', () => {
            it('D-TC01: should return the expected information when not all orders can be placed', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":100,"width":50,"height":50,"weight":400},
                                    {"id":4,"name":"Product 4","length":100,"width":100,"height":50,"weight":800}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"greedy",
                            timer:600
                        },
                        "loadDirection": 'x-y-z'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '00', '03', '02' ],
                                [ '00', '03', '00' ],
                                [ '00', '00', '00' ]
                            ]
                        ]
                        const loadList = [
                            {
                                "id": 1,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":1,"name":"Product 1","length":100,"maxLoadWeight":800,"width":100,"height":50,"weight":800, gridWidth: 2, gridHeight: 1, gridLength: 2, "units": 1}
                            },
                            {
                                "id": 2,
                                order:{"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":2,"name":"Product 2","length":150,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 1, gridHeight: 1, gridLength: 3, "units": 1}
                            },
                            {
                                "id": 3,
                                order:{"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[]},
                                product: {"__config": {"voxelSize": 50,"unitBoundary":20,"weightBoundary":500},"id":3,"name":"Product 3","length":100,"maxLoadWeight":400,"width":50,"height":50,"weight":400, gridWidth: 1, gridHeight: 1, gridLength: 2, "units": 1}
                            }
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(res.body.loadDistribution).to.eql({leftKg: 400, rightKg: 400})
                        expect(res.body.loadList).to.eql(loadList)

                        done();
                    });
            })

            it('D-TC01: should return the expected information when having two orders that can not be placed completely with greedy', (done) => {
                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders":[
                            {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                                    {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
                                    {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
                            },
                            {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
                                    {"id":3,"name":"Product 3","length":150,"width":50,"height":50,"weight":300},
                                    {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":400}]
                            }
                        ],
                        "cargospace":{
                            "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
                        },
                        "settings":{
                            algorithm:"greedy",
                            timer:600
                        },
                        "loadDirection": 'x-y-z'
                    })
                    .end((err, res) => {
                        // Assert
                        expect(res.status).to.equal(200);
                        expect(res.body).to.haveOwnProperty('cargoLayout')
                        expect(res.body).to.haveOwnProperty('loadList')
                        expect(res.body).to.haveOwnProperty('loadDistribution')

                        const expectedCargolayout = [
                            [
                                [ '01', '01', '02' ],
                                [ '01', '01', '02' ],
                                [ '00', '03', '02' ],
                                [ '00', '03', '00' ],
                                [ '00', '03', '00' ]
                            ],
                        ]

                        expect(res.body.cargoLayout).to.eql(expectedCargolayout)
                        expect(Math.round(res.body.loadDistribution.leftKg)).to.equal(400)
                        expect(Math.round(res.body.loadDistribution.rightKg)).to.equal(400)
                        expect(res.body.cargoLayoutSteps.length).to.equal(3)
                        expect(res.body.amountOfOriginalProducts).to.be.not.equal(res.body.loadList.length)

                        done();
                    });
            })

            describe('TC26: EmergencyStop', () => {
                it('TC26.1: should take into account the amount of units and weight when checking the emergencyStop option', (done) => {
                    // Arrange
                    const testCargospace = new Cargospace(1, 'Cargospace 1', 250, 150, 150, 40000);
                    const p1 = new Product(1, 'p1', 100, 100, 50, 400, 400);
                    const p2 = new Product(2, 'p2', 100, 50, 50, 200, 200);
                    const p3 = new Product(3, 'p3', 150, 100, 150, 600, 600, 20);

                    const orders = [
                        new Order(1,  [p1, p2, p3]),
                    ];

                    // Act
                    chai.request(app)
                        .post(planEndpoint)
                        .send({
                            "orders": orders,
                            "cargospace": testCargospace,
                            "settings":{
                                algorithm:"greedy",
                                timer:600,
                                emergencyStop: true,
                            },
                            "loadDirection": 'x-y-z'
                        })
                        .end((err, res) => {
                            // Assert
                            expect(res.status).to.equal(200);
                            expect(res.body).to.haveOwnProperty('cargoLayout')
                            expect(res.body).to.haveOwnProperty('loadList')
                            expect(res.body).to.haveOwnProperty('loadDistribution')

                            const expectedCargolayout = [
                                [
                                    ["01", "01", "02"],
                                    ["01", "01", "02"],
                                    ["01", "01", "00"],
                                    ["00", "03", "03"],
                                    ["00", "03", "03"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ],
                                [
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["01", "01", "00"],
                                    ["00", "00", "00"],
                                    ["00", "00", "00"],
                                ]
                            ];
                            expect(res.body.cargoLayout).to.eql(expectedCargolayout);

                            done()
                        })
                })
            })
        })

        describe('Compare load directions', () => {
            it('D-TC12: should show the difference between ZXY and XYZ load direction', (done) => {
                // Arrange
                const orders = [
                    {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
                            {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":600},
                            {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":600},
                            {"id":3,"name":"Product 3","length":150,"width":50,"height":50,"weight":600},
                            {"id":4,"name":"Product 4","length":100,"width":50,"height":50,"weight":200}],
                    }
                ];
                const cargospace = {
                    "id":1,"name":"Test laadruimte","length":350,"width":250,"height":50,"maxWeight":10000
                };

                let zxy_output,
                    xyz_output;

                // Act
                chai.request(app)
                    .post(planEndpoint)
                    .send({
                        "orders": orders,
                        "cargospace": cargospace,
                        "settings":{
                            algorithm:"greedy",
                            timer:600
                        },
                        "loadDirection": 'z-x-y'
                    })
                    .end((err, res) => {
                        zxy_output = res.body;

                        // Act
                        chai.request(app)
                            .post(planEndpoint)
                            .send({
                                "orders": orders,
                                "cargospace": cargospace,
                                "settings":{
                                    algorithm:"greedy",
                                    timer:600
                                },
                                "loadDirection": 'x-y-z'
                            })
                            .end((err, res) => {
                                // Assert
                                xyz_output = res.body;

                                const expectedZxyCargoLayout = [
                                    [
                                        ['00', '01', '01', '02', '00'],
                                        ['00', '01', '01', '02', '00'],
                                        ['00', '04', '00', '03', '00'],
                                        ['00', '04', '00', '03', '00'],
                                        ['00', '04', '00', '03', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                    ]
                                ];

                                const expectedXyzCargoLayout = [
                                    [
                                        ['04', '01', '01', '02', '03'],
                                        ['04', '01', '01', '02', '03'],
                                        ['04', '00', '00', '00', '03'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                        ['00', '00', '00', '00', '00'],
                                    ]
                                ];

                                expect(zxy_output.cargoLayout).to.eql(expectedZxyCargoLayout);
                                expect(xyz_output.cargoLayout).to.eql(expectedXyzCargoLayout);

                                done();
                            });
                    });
            })
        });
    })

    describe('Supportive functions', function() {
        it('D-TC02: should return an array with two empty cargoresponses when calling the /plan/cargoplannings endpoint', function(done) {
            // Act
            chai.request(app)
                .get('/api/plan/cargoplannings')
                .then((res) => {
                    // Assert
                    expect(res.body).to.be.an('array')
                    expect(res.body.length).to.equal(2)

                    done();
                })
                .catch((err) => {
                    console.log(err);
                })
        })

        it('D-TC03: should return the updated record when calling the /plan/cargoplanning/completed endpoint', function(done) {
            // Act
            chai.request(app)
                .put('/api/plan/cargoplanning/completed')
                .send({_id: 2})
                .then((res) => {
                    // Assert
                    expect(res.body).to.be.an('object')
                    expect(res.body.id).to.equal('2')

                    done();
                })
                .catch((err) => {
                    console.log(err);
                })
        })

        it('D-TC04: should return an object with id 2 when calling the /plan/cargoplanning/2 endpoint (adding note)', function(done) {
            // Act
            chai.request(app)
                .put('/api/plan/cargoplanning/2')
                .send({note: 'Nieuwe notitie'})
                .then((res) => {
                    // Assert
                    expect(res.body).to.be.an('object')
                    expect(res.body.id).to.equal('2')

                    done();
                })
                .catch((err) => {
                    console.log(err);
                })
        })
    });

    // describe('NF TC10: Test', () => {
    //     it('NF TC10.1: should return 200 when calling the new algorithm', (done) => {
    //         // Act
    //         chai.request(app)
    //             .post(planEndpoint)
    //             .send({
    //                 "orders":[
    //                     {"id":1,"customer":"Test de Test","address":"Teststraat 1, Breda","products":[
    //                             {"id":1,"name":"Product 1","length":100,"width":100,"height":50,"weight":800},
    //                             {"id":2,"name":"Product 2","length":150,"width":50,"height":50,"weight":400}],
    //                     },
    //                     {"id":2,"customer":"Test de Test 2","address":"Teststraat 2, Breda","products":[
    //                             {"id":3,"name":"Product 3","length":100,"width":50,"height":50,"weight":400},
    //                             {"id":4,"name":"Product 1","length":100,"width":100,"height":50,"weight":800}]
    //                     }
    //                 ],
    //                 "cargospace":{
    //                     "id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000
    //                 },
    //                 "settings":{
    //                     algorithm:"test",
    //                     timer:600
    //                 },
    //                 "loadDirection": 'z-x-y'
    //             })
    //             .end((err, res) => {
    //                 // Assert
    //                 expect(res.status).to.equal(200);
    //
    //                 done();
    //             });
    //     })
    // })
})
