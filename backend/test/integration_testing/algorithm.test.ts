import {expect} from 'chai';
import init = require('../init')
import {CanPlaceAndCoordinatesWeight} from '../../src/domain/types/types';
import {Order} from '../../src/domain/models/order.model';
import {Algorithm} from '../../src/business/services/plan/algorithms/algorithm';
import {Product} from '../../src/domain/models/product.model';
import {Cargospace} from '../../src/domain/models/cargospace.model';
import {Coordinates} from '../../src/domain/models/coordinates.model';
import {CoordinatesWeight} from '../../src/domain/models/coordinatesWeight.model';

init // Test DI

describe('Algorithm', () => {
    describe('Z-X-Y', () => {
        describe('TC05: Measurements/boundaries', () => {
            it('TC05.1: should return false when the product cannot fit on the specified start coordinates because product will be out of boundaries', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 100, 100, 50, 100, 100);
                const orders = [new Order(1, [p1])];

                class testAlgorithm extends Algorithm {
                    protected order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsLeft(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsLeftRequirements(coordinates, p)
                    }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'z-x-y',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsLeft(0, 0, 0, p1);
                expect(response).to.be.false;

                expect(rightCoordinatesWeight.voxelCoordinatesArray.length).to.equal(0);

                expect(rightCoordinatesWeight.leftWeight).to.equal(0);
                expect(rightCoordinatesWeight.rightWeight).to.equal(0);

                const coordinatesArray = [];
                expect(rightCoordinatesWeight.voxelCoordinatesArray).to.eql(coordinatesArray);
                done()
            })

            it('TC05.2: should return false when product cannot fit on specified start coordinates because some voxels are already filled', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);

                const p1 = new Product(1, 'p1', 50, 100, 50, 100, 100);
                const p2 = new Product(2, 'p2', 150, 100, 50, 100, 100);

                const orders = [new Order(1, [p1, p2])];

                class testAlgorithm extends Algorithm {
                    protected order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, toRight: boolean, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'z-x-y',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], false, rightCoordinatesWeight)

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "00"],
                        ["00", "00", "00"],
                        ["00", "00", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })
        });

        describe('Weights', () => {
            it('TC09: should return false when product on top of another is heavier than the weight of product below', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 150, 100, 50, 200);
                const p2 = new Product(2, 'p2', 150, 100, 50, 100);
                const orders = [
                    new Order(1, [p1]),
                    new Order(2, [p2])
                ];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, toRight: boolean, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'z-x-y',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], false, rightCoordinatesWeight)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })

            it('TC11: should return false when second order is not evenly distributed', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 100, 40000);
                const p1 = new Product(1, 'p1', 150, 100, 50, 1200, 1200);
                const p2 = new Product(2, 'p2', 150, 50, 50, 300, 300);
                const p3 = new Product(3, 'p3', 150, 50, 50, 600, 600);
                const p4 = new Product(4, 'p4', 150, 100, 50, 300, 300);
                const orders = [
                    new Order(1, [p1, p2]),
                    new Order(2, [p3, p4])
                ];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'z-x-y',
                    timer: 600,
                    emergencyStop: false
                })

                const [response1, rightCoordinatesWeight1] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], rightCoordinatesWeight1)
                algorithm.order = algorithm.getOrderlist()[0];
                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 2, 0, p2);
                algorithm.placeTheProduct(0, 2, 0, 2, p2, orders[0], rightCoordinatesWeight2)
                algorithm.order = algorithm.getOrderlist()[0];
                const [response3, rightCoordinatesWeight3] = algorithm.checkIfFitsRight(1, 0, 0, p3);
                algorithm.placeTheProduct(1, 0, 0, 3, p3, orders[0], rightCoordinatesWeight3)
                algorithm.order = algorithm.getOrderlist()[0];

                // assert
                const [response4, rightCoordinatesWeight4] = algorithm.checkIfFitsRight(1, 0, 0, p4);
                expect(response4).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "02"],
                        ["01", "01", "02"],
                        ["01", "01", "02"],
                    ],
                    [
                        ["03", "00", "00"],
                        ["03", "00", "00"],
                        ["03", "00", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })

            it('TC16: should return false when product on top of another is heavier than the max. load weight of product below', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 150, 100, 50, 100, 100);
                const p2 = new Product(2, 'p2', 150, 100, 50, 200, 100);
                const orders = [
                    new Order(1, [p1]),
                    new Order(2, [p2])
                ];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, toRight: boolean, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'z-x-y',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], false, rightCoordinatesWeight)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })
        });
    })

    describe('X-Y-Z', () => {
        describe('TC05: Measurements/boundaries', () => {
            it('TC05.1: should return false when the product cannot fit on the specified start coordinates because product will be out of boundaries', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 100, 100, 50, 100, 100);
                const orders = [new Order(1, [p1])];

                class testAlgorithm extends Algorithm {
                    protected order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsLeft(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsLeftRequirements(coordinates, p)
                    }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'x-y-z',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsLeft(0, 0, 0, p1);
                expect(response).to.be.false;

                expect(rightCoordinatesWeight.voxelCoordinatesArray.length).to.equal(0);

                expect(rightCoordinatesWeight.leftWeight).to.equal(0);
                expect(rightCoordinatesWeight.rightWeight).to.equal(0);

                const coordinatesArray = [];
                expect(rightCoordinatesWeight.voxelCoordinatesArray).to.eql(coordinatesArray);
                done()
            })

            it('TC05.2: should return false when product cannot fit on specified start coordinates because some voxels are already filled', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 50, 100, 50, 100, 100);
                const p2 = new Product(2, 'p2', 150, 100, 50, 100, 100);
                const orders = [new Order(1, [p1, p2])];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'x-y-z',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], rightCoordinatesWeight)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "00"],
                        ["00", "00", "00"],
                        ["00", "00", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })
        });

        describe('Weights', () => {
            it('TC09: should return false when product on top of another is heavier than the weight of product below', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 150, 100, 50, 200);
                const p2 = new Product(2, 'p2', 150, 100, 50, 100);
                const orders = [
                    new Order(1, [p1]),
                    new Order(2, [p2])
                ];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'x-y-z',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], rightCoordinatesWeight)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })

            it('TC11: should return false when second order is not evenly distributed', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 100, 40000);
                const p1 = new Product(1, 'p1', 150, 100, 50, 1200, 1200);
                const p2 = new Product(2, 'p2', 150, 50, 50, 300, 300);
                const p3 = new Product(3, 'p3', 150, 50, 50, 600, 600);
                const p4 = new Product(4, 'p4', 150, 100, 50, 300, 300);
                const orders = [
                    new Order(1,  [p1, p2]),
                    new Order(2,  [p3, p4])
                ];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'x-y-z',
                    timer: 600,
                    emergencyStop: false
                })

                const [response1, rightCoordinatesWeight1] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], rightCoordinatesWeight1)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 2, 0, p2);
                algorithm.placeTheProduct(0, 2, 0, 2, p2, orders[0], rightCoordinatesWeight2)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response3, rightCoordinatesWeight3] = algorithm.checkIfFitsRight(1, 0, 0, p3);
                algorithm.placeTheProduct(1, 0, 0, 3, p3, orders[0], rightCoordinatesWeight3)
                algorithm.order = algorithm.getOrderlist()[0];

                // assert
                const [response4, rightCoordinatesWeight4] = algorithm.checkIfFitsRight(1, 0, 0, p4);
                expect(response4).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "02"],
                        ["01", "01", "02"],
                        ["01", "01", "02"],
                    ],
                    [
                        ["03", "00", "00"],
                        ["03", "00", "00"],
                        ["03", "00", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })

            it('TC16: should return false when product on top of another is heavier than the max. load weight of product below', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);
                const p1 = new Product(1, 'p1', 150, 100, 50, 100, 100);
                const p2 = new Product(2, 'p2', 150, 100, 50, 200, 100);
                const orders = [
                    new Order(1, [p1]),
                    new Order(2, [p2])
                ];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'x-y-z',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 0, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 0, 1, p1, orders[0], rightCoordinatesWeight)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;

                const expectedCargolayout = [
                    [
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                        ["01", "01", "00"],
                    ]
                ];

                expect(algorithm.getCargolayout()).to.eql(expectedCargolayout)
                done()
            })

        });

        describe('Special requirements', () => {
            it('D-TC24: should return false when cargo is not directly packable when using vehicle with tailgate', (done) => {
                // arrange
                const testCargospace = new Cargospace(1, 'Cargospace 1', 100, 100, 50, 40000);
                const p1 = new Product(1, 'p1', 50, 100, 50, 100, 100);
                const p2 = new Product(2, 'p2', 50, 100, 50, 100, 100);
                const orders = [new Order(1, [p1, p2])];

                class testAlgorithm extends Algorithm {
                    order: Order = orders[0];

                    planCargo(): boolean {
                        return true;
                    }

                    checkIfFitsRight(y: number, x: number, z: number, p: Product): CanPlaceAndCoordinatesWeight {
                        const coordinates = new Coordinates(y, x, z);
                        return this.productMeetsRightRequirements(coordinates, p)
                    }

                    placeTheProduct(y: number, x: number, z: number, c: number, p: Product, o: Order, coordinatesWeight: CoordinatesWeight): void {
                        return this.placeProduct(c, p, o, coordinatesWeight);
                    }

                    getCargolayout() {
                        return this.cargolayout;
                    }

                    getOrderlist() { return this.orderList; }

                    loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean {
                        return false;
                    }
                }

                // act
                const algorithm = new testAlgorithm({
                    title: '',
                    algorithm: 'testAlgorithm',
                    orders: orders,
                    cargoSpace: testCargospace,
                    direction: 'x-y-z',
                    timer: 600,
                    emergencyStop: false
                })

                // assert
                const [response, rightCoordinatesWeight] = algorithm.checkIfFitsRight(0, 0, 1, p1);
                expect(response).to.be.true;
                algorithm.placeTheProduct(0, 0, 1, 1, p1, orders[0], rightCoordinatesWeight)
                algorithm.order = algorithm.getOrderlist()[0];

                const [response2, rightCoordinatesWeight2] = algorithm.checkIfFitsRight(0, 0, 0, p2);
                expect(response2).to.be.false;
                done()
            })
        });
    })
});
