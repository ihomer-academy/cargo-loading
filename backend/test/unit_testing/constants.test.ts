import {expect} from 'chai';
import {Cargospace} from '../../src/domain/models/cargospace.model';
import {Container} from 'typescript-ioc';
import init = require('../init')

init

describe('Constant class', () => {
    describe('Voxel size', () => {
        it('D-TC23: should use default voxelsize if no voxelsize is set', (done) => {
            // Act
            const testCargospace = new Cargospace(1, 'Cargospace 1', 150, 150, 50, 40000);

            // Assert
            expect(testCargospace.config.voxelSize).to.equal(50);
            expect(testCargospace.gridWidth).to.equal(3);
            expect(testCargospace.gridHeight).to.equal(1);
            expect(testCargospace.gridLength).to.equal(3);
            done()
        })

        describe('Custom voxelsize', () => {
            // Arrange
            before(function() {
                Container.bindName('Configuration').to({voxelSize: 100})
            })

            it('D-TC23: should use voxelsize of custom config', (done) => {
                // Act
                const testCargospace = new Cargospace(1, 'Cargospace 1', 200, 100, 100, 40000)

                // Assert
                expect(testCargospace.config.voxelSize).to.equal(100);
                expect(testCargospace.gridLength).to.equal(2);
                expect(testCargospace.gridWidth).to.equal(1);
                expect(testCargospace.gridHeight).to.equal(1);
                done()
            })

            after(function() {
                Container.bindName('Configuration').to({voxelSize: 50})
            })
        })
    })
})
