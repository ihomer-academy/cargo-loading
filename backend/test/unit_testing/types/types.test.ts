import {expect} from 'chai';
import init = require('../../init')
import {
    CanPlaceAndCoordinatesWeight,
    CanPlaceAndLeftAndRightWeightInKg, CanPlaceAndWasPlaced, CargoLayout, CargoSettings,
    LeftAndRightWeightInKg,
    YXAndZDimensionArrays,
    YAndZDimensionArrays
} from '../../../src/domain/types/types';

init

describe('Types', () => {
    it('D-TC22: CanPlaceAndCoordinatesWeight should contain a boolean and a CoordinatesWeight', (done) => {
        // Arrange
        let bAndCW: CanPlaceAndCoordinatesWeight;

        // Act
        bAndCW = [true, {voxelCoordinatesArray: [], leftWeight: 0, rightWeight: 0}];
        let [b, cw] = bAndCW;

        // Assert
        expect(b).to.be.an('boolean')
        expect(b).to.be.true;

        expect(cw).to.haveOwnProperty('voxelCoordinatesArray');
        expect(cw).to.haveOwnProperty('leftWeight');
        expect(cw).to.haveOwnProperty('rightWeight');

        done();
    })

    it('D-TC22: LeftAndRightWeightInKg should contain a number and a number', (done) => {
        // Arrange
        let lAndRW: LeftAndRightWeightInKg;

        // Act
        lAndRW = [10, 5];
        let [l, rw] = lAndRW;

        // Assert
        expect(l).to.be.an('number')
        expect(rw).to.be.an('number')

        expect(l).to.equal(10);
        expect(rw).to.equal(5);

        done();
    })

    it('D-TC22: YXAndZDimensionArrays should contain a number array, number array and a number array', (done) => {
        // Arrange
        let tArrToLoop: YXAndZDimensionArrays;

        // Act
        tArrToLoop = [[5, 2, 1], [1, 2, 3], [3, 2, 1]];
        let [firstArr, secArr, thirdArr] = tArrToLoop;

        // Assert
        expect(firstArr).to.be.an('array')
        expect(secArr).to.be.an('array')
        expect(thirdArr).to.be.an('array')

        expect(firstArr).to.eql([5, 2, 1]);
        expect(secArr).to.eql([1, 2, 3]);
        expect(thirdArr).to.eql([3, 2, 1]);

        done();
    })

    it('D-TC22: YAndZDimensionArrays should contain a number array and a number array', (done) => {
        // Arrange
        let tArrToLoop: YAndZDimensionArrays;

        // Act
        tArrToLoop = [[1, 2, 3], [3, 2, 1]];
        let [firstArr, secArr] = tArrToLoop;

        // Assert
        expect(firstArr).to.be.an('array')
        expect(secArr).to.be.an('array')

        expect(firstArr).to.eql([1, 2, 3]);
        expect(secArr).to.eql([3, 2, 1]);

        done();
    })

    it('D-TC22: CanPlaceAndLeftAndRightWeightInKg should contain a boolean and a LeftAndRightWeightInKg', (done) => {
        // Arrange
        let bAndLRW: CanPlaceAndLeftAndRightWeightInKg;

        // Act
        bAndLRW = [true, [18, 12]];
        let [b, rw] = bAndLRW;
        let [left, right] = rw;

        // Assert
        expect(b).to.be.an('boolean')
        expect(b).to.be.true;

        expect(rw).to.be.an('array')
        expect(left).to.be.an('number')
        expect(right).to.be.an('number')
        expect(left).to.be.equal(18)
        expect(right).to.be.equal(12)

        done();
    })

    it('D-TC22: CanPlaceAndWasPlaced should contain a boolean and a boolean', (done) => {
        // Arrange
        let bAndWp: CanPlaceAndWasPlaced;

        // Act
        bAndWp = [true, false];
        let [b, wp] = bAndWp;

        // Assert
        expect(b).to.be.an('boolean')
        expect(b).to.be.true;

        expect(wp).to.be.an('boolean')
        expect(wp).to.be.false;

        done();
    })

    it('D-TC22: CargoLayout should contain three dimensional string array', (done) => {
        // Arrange
        let cl: CargoLayout;

        // Act
        cl = [
            [
                ['00', '00', '01'],
                ['00', '00', '01']
            ]
        ];

        // Assert
        expect(cl).to.be.an('array')
        expect(cl.length).to.be.equal(1);
        expect(cl[0].length).to.be.equal(2);
        expect(cl[0][0].length).to.be.equal(3);

        done();
    })

    it('D-TC22: CargoSettings should contain an object with algorithm string and timer number', (done) => {
        // Arrange
        let cs: CargoSettings;

        // Act
        cs = {algorithm: 'testAlg', timer: 500, emergencyStop: false};

        // Assert
        expect(cs).to.be.an('object')
        expect(cs).to.haveOwnProperty('algorithm')
        expect(cs).to.haveOwnProperty('timer')
        expect(cs.algorithm).to.be.equal('testAlg')
        expect(cs.timer).to.be.equal(500)

        done();
    })
})
