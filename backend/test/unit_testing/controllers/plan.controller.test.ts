import chai = require('chai');
const expect = chai.expect;
import init = require('../../init')
import chaiHttp = require('chai-http')
import app from '../../../src/app';
import * as sinon from 'sinon';
import {DirectionFactory} from '../../../src/business/services/plan/algorithms/factories/direction.factory';
import {Direction} from '../../../src/business/services/plan/algorithms/load_directions/direction';
import {CargoPlanningService} from '../../../src/business/services/plan/cargo-planning.service';
import {CargoPlanning} from '../../../src/domain/models/cargoPlanning.model';
import {Cargospace} from '../../../src/domain/models/cargospace.model';
import {PlanService} from '../../../src/business/services/plan/plan.service';
chai.use(chaiHttp);

init

const planEndpoint = '/api/plan' // Arrange

describe('Plan controller', () => {
    let cp: CargoPlanning;

    before(() => {
        cp = {
            id: '4',
            cargoLayout: [],
            loadList: [],
            loadDistribution: {leftKg: 0, rightKg: 0},
            cargoLayoutSteps: [],
            cargoSpace: new Cargospace(1, '', 10, 10, 10, 100),
            completed: false,
            createdOn: new Date(),
            note: '',
            title: '',
            loadDirection: '',
            amountOfOriginalProducts: 1
        }
    })

    afterEach(function() {
        sinon.restore();
    });

    it("D-TC01: should return an CargoPlanning when calling the post plan endpoint with body", function(done) {
        // Arrange
        const stubPlan = sinon.stub(PlanService, <any>"plan").callsFake(function(): Promise<any> {
            return Promise.resolve(cp);
        });

        // Act
        chai.request(app)
            .post(planEndpoint)
            .send({
                "orders":[],
                "cargospace":{"id":1,"name":"Test laadruimte","length":250,"width":150,"height":50,"maxWeight":10000},
                "settings":{type:"backtracking", timer:600},
                "loadDirection": 'z-x-y'
            })
            .end((err, res) => {
                // Assert
                expect(stubPlan.calledOnce).to.be.true;

                expect(res.body).to.haveOwnProperty('id')
                expect(res.body).to.haveOwnProperty('cargoLayout')
                expect(res.body).to.haveOwnProperty('loadList')
                expect(res.body).to.haveOwnProperty('loadDistribution')
                expect(res.body).to.haveOwnProperty('cargoLayoutSteps')
                expect(res.body).to.haveOwnProperty('cargoSpace')
                expect(res.body).to.haveOwnProperty('completed')
                expect(res.body).to.haveOwnProperty('createdOn')
                expect(res.body).to.haveOwnProperty('note')
                expect(res.body).to.haveOwnProperty('title')
                expect(res.body).to.haveOwnProperty('loadDirection')
                expect(res.body).to.haveOwnProperty('amountOfOriginalProducts')
                done();
            });
    })

    it("D-TC02: should return an array with CargoPlannings when calling the get plan/cargoplannings endpoint", function(done) {
        // Arrange
        const stubGetAll = sinon.stub(CargoPlanningService.prototype, <any>"getAllUnusedCargoPlannings").callsFake(function(): Promise<CargoPlanning[]> {
            return Promise.resolve([cp]);
        });

        // Act
        chai.request(app)
            .get(planEndpoint + '/cargoplannings')
            .then((res) => {
                // Assert
                expect(stubGetAll.calledOnce).to.be.true;
                expect(res.body).to.be.an('array')
                expect(res.body.length).to.equal(1)
                expect(res.body[0]).to.haveOwnProperty('id')
                expect(res.body[0]).to.haveOwnProperty('cargoLayout')
                expect(res.body[0]).to.haveOwnProperty('loadList')
                expect(res.body[0]).to.haveOwnProperty('loadDistribution')
                expect(res.body[0]).to.haveOwnProperty('cargoLayoutSteps')
                expect(res.body[0]).to.haveOwnProperty('cargoSpace')
                expect(res.body[0]).to.haveOwnProperty('completed')
                expect(res.body[0]).to.haveOwnProperty('createdOn')
                expect(res.body[0]).to.haveOwnProperty('note')
                expect(res.body[0]).to.haveOwnProperty('title')
                expect(res.body[0]).to.haveOwnProperty('loadDirection')
                expect(res.body[0]).to.haveOwnProperty('amountOfOriginalProducts')

                done();
            })
            .catch((err) => {
                console.log(err);
            })
    })

    it("D-TC03: should return an CargoPlanning when calling the put plan/cargoplanning/completed endpoint", function(done) {
        // Arrange
        const stubMarkCompl = sinon.stub(CargoPlanningService.prototype, <any>"markCargoPlanningAsCompleted").callsFake(function(): Promise<CargoPlanning> {
            cp.completed = true;
            return Promise.resolve(cp);
        });

        // Act
        chai.request(app)
            .put(planEndpoint + '/cargoplanning/completed')
            .send({_id: 4})
            .then((res) => {
                // Assert
                expect(stubMarkCompl.calledOnce).to.be.true;
                expect(res.body).to.be.an('object')
                expect(res.body.id).to.equal('4');
                expect(res.body.completed).to.equal(true);

                expect(res.body).to.haveOwnProperty('id')
                expect(res.body).to.haveOwnProperty('cargoLayout')
                expect(res.body).to.haveOwnProperty('loadList')
                expect(res.body).to.haveOwnProperty('loadDistribution')
                expect(res.body).to.haveOwnProperty('cargoLayoutSteps')
                expect(res.body).to.haveOwnProperty('cargoSpace')
                expect(res.body).to.haveOwnProperty('completed')
                expect(res.body).to.haveOwnProperty('createdOn')
                expect(res.body).to.haveOwnProperty('note')
                expect(res.body).to.haveOwnProperty('title')
                expect(res.body).to.haveOwnProperty('loadDirection')
                expect(res.body).to.haveOwnProperty('amountOfOriginalProducts')

                done();
            })
            .catch((err) => {
                console.log(err);
            })
    })

    it("D-TC04: should return a CargoPlanning when calling the put plan/cargoplanning/:id endpoint", function(done) {
        // Arrange
        const stubAddNote = sinon.stub(CargoPlanningService.prototype, <any>"addNoteToCargoPlanning").callsFake(function(): Promise<CargoPlanning> {
            cp.note = 'The note';
            return Promise.resolve(cp);
        });

        // Act
        chai.request(app)
            .put(planEndpoint + '/cargoplanning/4')
            .send({note: 'The note'})
            .then((res) => {
                // Assert
                expect(stubAddNote.calledOnce).to.be.true;
                expect(res.body).to.be.an('object')
                expect(res.body.id).to.equal('4');
                expect(res.body.note).to.equal('The note');

                expect(res.body).to.haveOwnProperty('id')
                expect(res.body).to.haveOwnProperty('cargoLayout')
                expect(res.body).to.haveOwnProperty('loadList')
                expect(res.body).to.haveOwnProperty('loadDistribution')
                expect(res.body).to.haveOwnProperty('cargoLayoutSteps')
                expect(res.body).to.haveOwnProperty('cargoSpace')
                expect(res.body).to.haveOwnProperty('completed')
                expect(res.body).to.haveOwnProperty('createdOn')
                expect(res.body).to.haveOwnProperty('note')
                expect(res.body).to.haveOwnProperty('title')
                expect(res.body).to.haveOwnProperty('loadDirection')
                expect(res.body).to.haveOwnProperty('amountOfOriginalProducts')

                done();
            })
            .catch((err) => {
                console.log(err);
            })
    })
})
