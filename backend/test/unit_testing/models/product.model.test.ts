import {expect} from 'chai';
import {Product} from '../../../src/domain/models/product.model';
import init = require('../../init')

init

describe('Product model', () => {
    it('D-TC08: should set product values when initializing product', (done) => {
        // Act
        const p = new Product(1, 'Product 1', 400, 100, 100, 800, 800);

        // Assert
        expect(p.id).to.equal(1);
        expect(p.name).to.equal('Product 1');
        expect(p.length).to.equal(400);
        expect(p.width).to.equal(100);
        expect(p.height).to.equal(100);
        expect(p.weight).to.equal(800);
        expect(p.maxLoadWeight).to.equal(800);
        expect(p.gridWidth).to.not.be.undefined;
        expect(p.gridHeight).to.not.be.undefined;
        expect(p.gridLength).to.not.be.undefined;

        done();
    })

    it('D-TC09: should create virtual x, y and z when creating a product', (done) => {
        // Act
        const p = new Product(1, 'Product 1', 400, 100, 100, 800, 800);

        // Assert
        expect(p.gridWidth).to.equal(2);
        expect(p.gridLength).to.equal(8);
        expect(p.gridHeight).to.equal(2);

        done();
    })

    it('D-TC09: should ceil the virtual x, y and z when creating a product with measurement % voxelsize !== 0', (done) => {
        // Act
        const p = new Product(1, 'Product 1', 435, 145, 100, 800, 800);

        // Assert
        expect(p.gridWidth).to.equal(3);
        expect(p.gridLength).to.equal(9);
        expect(p.gridHeight).to.equal(2);

        done();
    })

    it('D-TC10: should not change the x and z axis if passed wrong orientation value', (done) => {
        // Arrange
        const p = new Product(1, 'Product 1', 400, 100, 100, 800, 800);

        // Act
        p.changeOrientation(2)

        // Assert
        expect(p.gridWidth).to.be.equal(2)
        expect(p.gridHeight).to.be.equal(2)
        expect(p.gridLength).to.be.equal(8)

        done();
    })

    it('D-TC10: gridLength should contain the largest number when passing param 1 to changeOrientation', (done) => {
        // Arrange
        const p = new Product(1, 'Product 1', 400, 100, 100, 800, 800);

        // Act
        p.changeOrientation(1)

        // Assert
        expect(p.gridWidth).to.be.equal(2)
        expect(p.gridHeight).to.be.equal(2)
        expect(p.gridLength).to.be.equal(8)

        done();
    })

    it('D-TC10: gridWidth should contain the largest number when passing param 0 to changeOrientation', (done) => {
        // Arrange
        const p = new Product(1, 'Product 1', 400, 100, 100, 800, 800);

        // Act
        p.changeOrientation(0)

        // Assert
        expect(p.gridWidth).to.be.equal(8)
        expect(p.gridHeight).to.be.equal(2)
        expect(p.gridLength).to.be.equal(2)

        done();
    })

    it('D-TC10: gridLength should contain the largest number when passing param 1 to changeOrientation with wide p', (done) => {
        // Arrange
        const p = new Product(1, 'Product 1', 100, 400, 100, 800, 800);

        // Act
        p.changeOrientation(1)

        // Assert
        expect(p.gridWidth).to.be.equal(2)
        expect(p.gridHeight).to.be.equal(2)
        expect(p.gridLength).to.be.equal(8)

        done();
    })

    it('D-TC10: gridWidth should contain the largest number when passing param 0 to changeOrientation with wide p', (done) => {
        // Arrange
        const p = new Product(1, 'Product 1', 100, 400, 100, 800, 800);

        // Act
        p.changeOrientation(0)

        // Assert
        expect(p.gridWidth).to.be.equal(8)
        expect(p.gridHeight).to.be.equal(2)
        expect(p.gridLength).to.be.equal(2)

        done();
    })
})
