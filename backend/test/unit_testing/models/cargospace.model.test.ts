import {expect} from 'chai';
import {Cargospace} from '../../../src/domain/models/cargospace.model';
import init = require('../../init')

init

describe('Cargospace model', () => {
    it('D-TC06: should set cargospace values when initializing cargospace', (done) => {
        // Act
        const m = new Cargospace(1, 'Cargospace 1', 1000, 200, 250, 40000);

        // Assert
        expect(m.id).to.equal(1);
        expect(m.name).to.equal('Cargospace 1');
        expect(m.length).to.equal(1000);
        expect(m.width).to.equal(200);
        expect(m.height).to.equal(250);
        expect(m.maxWeight).to.equal(40000);
        expect(m.gridWidth).to.not.be.undefined;
        expect(m.gridHeight).to.not.be.undefined;
        expect(m.gridLength).to.not.be.undefined;

        done();
    })

    it('D-TC07: should create virtual x, y and z when creating a cargospace', (done) => {
        // Act
        const m = new Cargospace(1, 'Cargospace 1', 1000, 200, 250, 40000);

        // Assert
        expect(m.gridWidth).to.equal(4);
        expect(m.gridHeight).to.equal(5);
        expect(m.gridLength).to.equal(20);

        done();
    })

    it('D-TC07: should floor the virtual x, y and z when creating a cargospace with measurement % voxelsize !== 0', (done) => {
        // Act
        const m = new Cargospace(1, 'Cargospace 1', 1000, 220, 275, 40000);

        // Assert
        expect(m.gridWidth).to.equal(4);
        expect(m.gridHeight).to.equal(5);
        expect(m.gridLength).to.equal(20);

        done();
    })
})
