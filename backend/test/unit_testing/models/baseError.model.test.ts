import {expect} from 'chai';
import {BaseError} from '../../../src/domain/models/baseError.model';
import init = require('../../init')

init

describe('BaseError model', () => {
    it('D-TC05: should set message and originalAction if passed as parameter', (done) => {
        // Act
        const e = new BaseError("Test error message", "Test base error model");

        // Assert
        expect(e.message).to.be.equal("Test error message")
        expect(e.originalAction).to.be.equal("Test base error model")
        expect(e.body).to.be.undefined;
        done();
    })

    it('D-TC05: should set message, originalAction and body if passed as parameter', (done) => {
        // Act
        const e = new BaseError("Test error message", "Test base error model", {test: "This is a test value"});

        // Assert
        expect(e.message).to.be.equal("Test error message")
        expect(e.originalAction).to.be.equal("Test base error model")
        expect(e.body).haveOwnProperty('test')
        expect(e.body).to.be.eql({test: "This is a test value"})
        done();
    })
})
