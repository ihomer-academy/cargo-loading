import {expect} from 'chai';
import init = require('../../../init')
const faker = require("faker");
import * as sinon from "sinon";
import {Cargospace} from '../../../../src/domain/models/cargospace.model';
import {CargoPlanning} from '../../../../src/domain/models/cargoPlanning.model';
import {AlgorithmFactory} from '../../../../src/business/services/plan/algorithms/factories/algorithm.factory';
import {Algorithm} from '../../../../src/business/services/plan/algorithms/algorithm';
import {PlanService} from '../../../../src/business/services/plan/plan.service';
import {Coordinates} from '../../../../src/domain/models/coordinates.model';

init


describe('Plan service', () => {
    let stubValue: CargoPlanning;

    before(() => {
        // Arrange
        stubValue = {
            id: '',
            loadList: [],
            loadDistribution: {leftKg: 10, rightKg: 15},
            cargoLayoutSteps: [],
            title: '',
            cargoLayout: [],
            cargoSpace: new Cargospace(1, '', 10, 10, 10, 100),
            completed: false,
            note: '',
            createdOn: faker.date.past(),
            loadDirection: '',
            amountOfOriginalProducts: 1
        };
    })

    afterEach(function() {
        sinon.restore();
    });

    it('D-TC21: plan returns failed when planCargo returns false', (done) => {
        // Arrange
        class mockedAlg extends Algorithm {
            planCargo(): boolean {return false;}
            loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean { return false; }
        }

        const stubCreateAlg = sinon.stub(AlgorithmFactory, "createAlgorithm").callsFake(function(): Algorithm {
            return new mockedAlg({orders: [], cargoSpace: stubValue.cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});
        });

        // Act
        PlanService.plan('', stubValue.cargoSpace, [], {algorithm: '', timer: 600, emergencyStop: false}, '')
            .then((response) => {
            })
            .catch((err) => {
                // Assert
                expect(stubCreateAlg.calledOnce).to.be.true;
                expect(err).to.equal('failed');
                done();
            })
    })

    it('D-TC21: plan returns part of CargoPlanning when planCargo returns true', (done) => {
        // Arrange
        class mockedAlg extends Algorithm {
            planCargo(): boolean {return true;}
            loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean { return false; }
        }

        const stubCreateAlg = sinon.stub(AlgorithmFactory, "createAlgorithm").callsFake(function(): Algorithm {
            return new mockedAlg({orders: [], cargoSpace: stubValue.cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});
        });

        const stubCreateSave = sinon.stub(Algorithm.prototype, "saveCargoplanning").callsFake(function(): Promise<string> {
            return Promise.resolve('5');
        });

        // Act
        PlanService.plan('', stubValue.cargoSpace, [], {algorithm: '', timer: 600, emergencyStop: false}, '')
            .then((response) => {
                // Assert
                expect(stubCreateAlg.calledOnce).to.be.true;
                expect(stubCreateSave.calledOnce).to.be.true;

                const expectedResponse = {
                    id: '5',
                    cargoLayout: [],
                    loadList: [],
                    loadDistribution: { leftKg: 0, rightKg: 0 },
                    cargoLayoutSteps: [],
                    amountOfOriginalProducts: 0
                }
                expect(response).to.eql(expectedResponse);
                done();
            })
            .catch((err) => {
                console.log(err)
            })
    })

    it('D-TC21: plan returns rejected promise when planCargo succeeded but saveCargoplanning failed', (done) => {
        // Arrange
        class mockedAlg extends Algorithm {
            planCargo(): boolean {return true;}
            loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean { return false; }
        }

        const stubCreateAlg = sinon.stub(AlgorithmFactory, "createAlgorithm").callsFake(function(): Algorithm {
            return new mockedAlg({orders: [], cargoSpace: stubValue.cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});
        });

        const stubCreateSave = sinon.stub(Algorithm.prototype, "saveCargoplanning").callsFake(function() {
            return Promise.reject();
        });

        // Act
        PlanService.plan('', stubValue.cargoSpace, [], {algorithm: '', timer: 600, emergencyStop: false}, '')
            .then((response) => {
            })
            .catch((err) => {
                // Assert
                expect(stubCreateAlg.calledOnce).to.be.true;
                expect(stubCreateSave.calledOnce).to.be.true;

                done();
            })
    })

    it('D-TC21: plan returns rejected promise when products cannot be placed', (done) => {
        // Arrange
        class mockedAlg extends Algorithm {
            planCargo(): boolean {return false;}
            loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean { return false; }
        }

        const stubCreateAlg = sinon.stub(AlgorithmFactory, "createAlgorithm").callsFake(function(): Algorithm {
            return new mockedAlg({orders: [], cargoSpace: stubValue.cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});
        });

        // Act
        PlanService.plan('', stubValue.cargoSpace, [], {algorithm: '', timer: 600, emergencyStop: false}, '')
            .then((response) => {
            })
            .catch((err) => {
                // Assert
                expect(stubCreateAlg.calledOnce).to.be.true;
                expect(err).to.equal('failed')

                done();
            })
    })
})
