import {expect} from 'chai';
import init = require('../../../init')
import {Cargospace} from '../../../../src/domain/models/cargospace.model';
import {CargolayoutService} from '../../../../src/business/services/plan/cargolayout.service';

init


describe('Cargolayout service', () => {
    it('D-TC20: initializeEmptyCargoLayout returns an empty CargoLayout with the correct lengths', (done) => {
        // Arrange
        const cs = new Cargospace(1, '', 500, 250, 50, 30000);

        // Act
        const cl = CargolayoutService.initializeEmptyCargoLayout(cs)

        // Assert
        expect(cl).to.be.an('array')
        expect(cl.length).to.equal(1);
        expect(cl[0].length).to.equal(10);
        expect(cl[0][0].length).to.equal(5);
        expect(cl[0][0][0]).to.equal('00');

        done();
    })
})
