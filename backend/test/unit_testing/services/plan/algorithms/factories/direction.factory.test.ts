import {expect} from 'chai';
import init = require('../../../../../init')
import {DirectionFactory} from '../../../../../../src/business/services/plan/algorithms/factories/direction.factory';
import {ZXYDirection} from '../../../../../../src/business/services/plan/algorithms/load_directions/z-x-y-direction';
import {XYZDirection} from '../../../../../../src/business/services/plan/algorithms/load_directions/x-y-z-direction';

init

describe('Direction Factory', () => {
    it('D-TC12: should create an instance of the ZXY class when passing z-x-y as parameter', (done) => {
        // Act
        const direction = DirectionFactory.chooseDirection('z-x-y')

        // Assert
        expect(direction instanceof ZXYDirection).to.be.true;
        expect(direction instanceof XYZDirection).to.be.false;

        done();
    })

    it('D-TC12: should create an instance of the ZXY class when passing a wrong parameter', (done) => {
        // Act
        const direction = DirectionFactory.chooseDirection('z-z-z')

        // Assert
        expect(direction instanceof ZXYDirection).to.be.true;
        expect(direction instanceof XYZDirection).to.be.false;

        done();
    })

    it('D-TC12: should create an instance of the XYZ class when passing x-y-z as parameter', (done) => {
        // Act
        const direction = DirectionFactory.chooseDirection('x-y-z')

        // Assert
        expect(direction instanceof XYZDirection).to.be.true;
        expect(direction instanceof ZXYDirection).to.be.false;

        done();
    })

    // it('D-TC12-test: should create an instance of the test-direction class when passing test-direction as parameter', (done) => {
    //     // Act
    //     const direction = DirectionFactory.chooseDirection('test-direction')
    //
    //     // Assert
    //     expect(direction instanceof ZXYDirection).to.be.false;
    //     expect(direction instanceof XYZDirection).to.be.false;
    //     expect(direction instanceof TestDirection).to.be.true;
    //
    //     done();
    // })
})
