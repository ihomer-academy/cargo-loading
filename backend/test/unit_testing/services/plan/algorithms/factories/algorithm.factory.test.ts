import {expect} from 'chai';
import init = require('../../../../../init')
import {Cargospace} from '../../../../../../src/domain/models/cargospace.model';
import {AlgorithmFactory} from '../../../../../../src/business/services/plan/algorithms/factories/algorithm.factory';
import {Backtracking} from '../../../../../../src/business/services/plan/algorithms/types/backtracking';
import {Greedy} from '../../../../../../src/business/services/plan/algorithms/types/greedy';

init

describe('Algorithm Factory', () => {
    it('D-TC11: should create an instance of the backtracking class when passing the backtracking parameter', (done) => {
        // Arrange
        const testCargospace = new Cargospace(1, 'Test cargospace', 1000, 250, 250, 30000);

        // Act
        const algorithm = AlgorithmFactory.createAlgorithm({title: '', cargoSpace: testCargospace, orders: [], algorithm: 'backtracking', direction: 'z-x-y', timer: 600, emergencyStop: false})

        // Assert
        expect(algorithm instanceof Backtracking).to.be.true;
        expect(algorithm instanceof Greedy).to.be.false;

        done();
    })

    it('D-TC11: should create an instance of the backtracking class when passing a wrong algorithm parameter', (done) => {
        // Arrange
        const testCargospace = new Cargospace(1, 'Test cargospace', 1000, 250, 250, 30000);

        // Act
        const algorithm = AlgorithmFactory.createAlgorithm({title: '', cargoSpace: testCargospace, orders: [], algorithm: 'somealgorithm', direction: 'z-x-y', timer: 600, emergencyStop: false})

        // Assert
        expect(algorithm instanceof Backtracking).to.be.true;
        expect(algorithm instanceof Greedy).to.be.false;

        done();
    })

    it('D-TC11: should create an instance of the Greedy class when passing the greedy parameter', (done) => {
        // Arrange
        const testCargospace = new Cargospace(1, 'Test cargospace', 1000, 250, 250, 30000);

        // Act
        const algorithm = AlgorithmFactory.createAlgorithm({title: '', cargoSpace: testCargospace, orders: [], algorithm: 'greedy', direction: 'z-x-y', timer: 600, emergencyStop: false})

        // Assert
        expect(algorithm instanceof Greedy).to.be.true;
        expect(algorithm instanceof Backtracking).to.be.false;

        done();
    })
})
