import {expect} from 'chai';
import init = require('../../../../../init')
import * as sinon from "sinon";
import {Cargospace} from '../../../../../../src/domain/models/cargospace.model';
import {Order} from '../../../../../../src/domain/models/order.model';
import {Product} from '../../../../../../src/domain/models/product.model';
import {Algorithm} from '../../../../../../src/business/services/plan/algorithms/algorithm';
import {Backtracking} from '../../../../../../src/business/services/plan/algorithms/types/backtracking';

init

describe('Backtracking implementation', () => {
    let cargoSpace,
        orders;

    before(() => {
        // Arrange
        cargoSpace = new Cargospace(1, 'Test cargospace', 500, 250, 50, 30000);
        orders = [
            new Order(1, [
                new Product(1, 'Product 1', 100, 50, 50, 500, 500),
                new Product(2, 'Product 2', 50, 50, 50, 400, 400)
            ])
        ]
    })

    afterEach(function() {
        sinon.restore();
    });

    it('D-TC16: planCargo should return true when cargospace is not full and product can be placed', (done) => {
        // Arrange
        const stubIsFull = sinon.stub(Algorithm.prototype, <any>"checkIfCargoSpaceIsFull").callsFake(function(): boolean {
            return false;
        });

        const bt = new Backtracking({orders, cargoSpace: cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});

        const stubLoop = sinon.stub(bt, <any>"loopVoxelsAndCheckIfProductCanBePlaced").callsFake(function(): boolean {
            return true;
        });

        // Act
        const response = bt.planCargo()

        // Assert
        expect(stubIsFull.calledOnce).to.be.true;
        expect(stubLoop.calledOnce).to.be.true;
        expect(response).to.be.true;

        done();
    })

    it('D-TC16: planCargo should return false when cargospace is full', (done) => {
        // Arrange
        const stubIsFull = sinon.stub(Algorithm.prototype, <any>"checkIfCargoSpaceIsFull").callsFake(function(): boolean {
            return true;
        });

        const bt = new Backtracking({orders, cargoSpace: cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});

        // Act
        const response = bt.planCargo()

        // Assert
        expect(stubIsFull.calledOnce).to.be.true;
        expect(response).to.be.false;

        done();
    })

    it('D-TC16: planCargo should return false when cargospace is not full but product cannot be placed', (done) => {
        // Arrange
        const stubIsFull = sinon.stub(Algorithm.prototype, <any>"checkIfCargoSpaceIsFull").callsFake(function(): boolean {
            return false;
        });

        const bt = new Backtracking({orders, cargoSpace: cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});

        const stubLoop = sinon.stub(bt, <any>"loopVoxelsAndCheckIfProductCanBePlaced").callsFake(function(): boolean {
            return false;
        });

        // Act
        const response = bt.planCargo()

        // Assert
        expect(stubIsFull.calledOnce).to.be.true;
        expect(stubLoop.calledOnce).to.be.true;
        expect(response).to.be.false;

        done();
    })
})
