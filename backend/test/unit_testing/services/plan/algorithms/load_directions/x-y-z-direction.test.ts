import {expect} from 'chai';
import init = require('../../../../../init')
import * as sinon from "sinon";
import {XYZDirection} from '../../../../../../src/business/services/plan/algorithms/load_directions/x-y-z-direction';
import {Direction} from '../../../../../../src/business/services/plan/algorithms/load_directions/direction';

init

describe('X-Y-Z', () => {
    afterEach(function() {
        sinon.restore();
    });

    it('D-TC14: cartesianVoxels should return an array with all number combinations', (done) => {
        // Arrange
        const stubCart = sinon.stub(Direction.prototype, <any>"cartesianProduct").callsFake(function(): any[] {
            return [
                [0, 1, 0],
                [0, 2, 0],
                [1, 1, 0],
                [1, 2, 0]
            ];
        });

        const direction = new XYZDirection();
        const y = [0, 1];
        const x = [1, 2];
        const z = [0];

        // Act
        const response = direction.cartesianVoxels(y, x, z);

        // Assert
        expect(stubCart.calledOnce).to.be.true;
        expect(response).to.be.an('array')
        expect(response.length).to.be.equal(4);
        expect(response).to.eql([
                                [0, 1, 0],
                                [0, 2, 0],
                                [1, 1, 0],
                                [1, 2, 0]
                            ]);

        done();
    })

    it('D-TC15: getXYZOfCartesianLoop should return a coordinatesArray in the right x, y, z sequence', (done) => {
        // Arrange
        const direction = new XYZDirection();
        const coordinatesArray = [4, 1, 0]

        // Act
        const [x, y, z] = direction.getXYZOfCartesianLoop(coordinatesArray);

        // Assert
        expect(x).to.be.eql(0);
        expect(y).to.be.eql(1);
        expect(z).to.be.eql(4);

        done();
    })
})
