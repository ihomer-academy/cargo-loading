import {expect} from 'chai';
import init = require('../../../../../init')
import {Direction} from '../../../../../../src/business/services/plan/algorithms/load_directions/direction';
import {Cargospace} from '../../../../../../src/domain/models/cargospace.model';
import {Product} from '../../../../../../src/domain/models/product.model';
import {Order} from '../../../../../../src/domain/models/order.model';
import {YXAndZDimensionArrays} from '../../../../../../src/domain/types/types';

init


describe('Direction', () => {
    // Arrange
    let cargoSpace,
        p1,
        orders;

    before(() => {
        cargoSpace = new Cargospace(1, '', 500, 250, 50, 30000);
        p1 = new Product(1, 'p1', 100, 100, 50, 100, 100);
        orders = [new Order(1, [p1])];
    })

    class MockedDir extends Direction {
        cartesianVoxels(yCoordinates: number[], xCoordinates: number[], zCoordinates: number[]): any[] {return [];}
        getXYZOfCartesianLoop(coordinatesArray: number[]): [number, number, number] {return [0, 0, 0];}

        getAllTheCargospaceYAndXAndZVoxelsToLoop(cs: Cargospace): YXAndZDimensionArrays {
            return this.getAllCargospaceYAndXAndZVoxelsToLoop(cs);
        }
    }

    it('D-TC13: getAllCargospaceYAndXAndZVoxelsToLoop should return three arrays', (done) => {
        // Arrange
        const direction = new MockedDir();

        // Act
        const [height, width, length] = direction.getAllTheCargospaceYAndXAndZVoxelsToLoop(cargoSpace);

        // Assert
        expect(height).to.be.an('array')
        expect(height.length).to.eql(1);
        expect(height).to.eql([0]);

        expect(width).to.be.an('array')
        expect(width.length).to.eql(5);
        expect(width).to.eql([0, 1, 2, 3, 4]);

        expect(length).to.be.an('array')
        expect(length.length).to.eql(10);
        expect(length).to.eql([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

        done();
    })
})
