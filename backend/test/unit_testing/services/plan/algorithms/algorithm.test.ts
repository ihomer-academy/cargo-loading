import {expect} from 'chai';
import init = require('../../../../init')
import * as sinon from "sinon";
import {Algorithm} from '../../../../../src/business/services/plan/algorithms/algorithm';
import {Cargospace} from '../../../../../src/domain/models/cargospace.model';
import {Order} from '../../../../../src/domain/models/order.model';
import {DirectionFactory} from '../../../../../src/business/services/plan/algorithms/factories/direction.factory';
import {Direction} from '../../../../../src/business/services/plan/algorithms/load_directions/direction';
import {Product} from '../../../../../src/domain/models/product.model';
import {Coordinates} from '../../../../../src/domain/models/coordinates.model';

init

describe('Algorithm', () => {
    let cargoSpace,
        p1,
        orders;

    before(() => {
        // Arrange
        cargoSpace = new Cargospace(1, '', 500, 250, 50, 30000);
        p1 = new Product(1, 'p1', 100, 100, 50, 100, 100);
        orders = [new Order(1, [p1])];
    })

    afterEach(function() {
        sinon.restore();
    });

    class MockedAlgorithm extends Algorithm {
        protected order: Order = orders[0];

        planCargo(): boolean { return true; }
        getCargoSpace(): Cargospace { return this.cargoSpace; }
        getOrderList(): Order[] { return this.orderList; }
        getTimer(): number { return this.timer; }
        getRightBeginX(): number { return this.rightBeginX; }
        loopProductAndCheckIfItCanFitOnStartPosition(coordinates: Coordinates): boolean { return false; }
    }

    it('D-TC17: constructor should save some values in the class', (done) => {
        // Arrange
        class MockedDir extends Direction {
            cartesianVoxels(yCoordinates: number[], xCoordinates: number[], zCoordinates: number[]): any[] {return [];}
            getXYZOfCartesianLoop(coordinatesArray: number[]): [number, number, number] {return [0, 0, 0];}
        }
        const stubCreateDirection = sinon.stub(DirectionFactory, "chooseDirection").callsFake(function(): Direction {
            return new MockedDir();
        });

        // Act
        const algorithm = new MockedAlgorithm({orders: [], cargoSpace: cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});

        // Assert
        expect(stubCreateDirection.calledOnce).to.be.true;
        expect(algorithm.getCargoSpace()).to.eql(cargoSpace)
        expect(algorithm.getOrderList()).to.eql([])
        expect(algorithm.getTimer()).to.eql(600);
        expect(algorithm.getRightBeginX()).to.equal(3);

        done();
    })

    it('D-TC18: should be able to save the cargoplanning', (done) => {
        // Arrange
        class MockedDir extends Direction {
            cartesianVoxels(yCoordinates: number[], xCoordinates: number[], zCoordinates: number[]): any[] {return [];}
            getXYZOfCartesianLoop(coordinatesArray: number[]): [number, number, number] {return [0, 0, 0];}
        }
        const stubCreateDirection = sinon.stub(DirectionFactory, "chooseDirection").callsFake(function(): Direction {
            return new MockedDir();
        });

        const stubSavePlan = sinon.stub(Algorithm.prototype, "saveCargoplanning").callsFake(function(): Promise<string> {
            return Promise.resolve('4');
        });

        // Act
        const algorithm = new MockedAlgorithm({orders: [], cargoSpace: cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});
        algorithm.saveCargoplanning()
            .then((id) => {
                // Assert
                expect(stubSavePlan.calledOnce).to.be.true;
                expect(id).to.be.equal('4');

                done();
            })
    })

    it('D-TC19: should return the response values when calling getReponseValues()', (done) => {
        // Arrange
        class MockedDir extends Direction {
            cartesianVoxels(yCoordinates: number[], xCoordinates: number[], zCoordinates: number[]): any[] {return [];}
            getXYZOfCartesianLoop(coordinatesArray: number[]): [number, number, number] {return [0, 0, 0];}
        }
        const stubCreateDirection = sinon.stub(DirectionFactory, "chooseDirection").callsFake(function(): Direction {
            return new MockedDir();
        });

        // Act
        const algorithm = new MockedAlgorithm({orders: [], cargoSpace: cargoSpace, timer: 600, title: '', algorithm: '', direction: '', emergencyStop: false});
        const response = algorithm.getResponseValues();

        // Assert
        expect(response.cargoSpace).to.eql(cargoSpace);
        expect(response.cargoLayout.length).to.eql(1);
        expect(response.cargoLayout[0].length).to.eql(10);
        expect(response.cargoLayout[0][0].length).to.eql(5);
        expect(response.loadDistribution).to.eql({leftKg: 0, rightKg: 0});
        expect(response.loadList).to.eql([]);
        expect(response.cargolayoutSteps).to.eql([])

        done();
    })
})
