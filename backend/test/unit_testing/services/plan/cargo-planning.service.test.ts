import {expect} from 'chai';
import init = require('../../../init')
import {CargoPlanning} from '../../../../src/domain/models/cargoPlanning.model';
import {Cargospace} from '../../../../src/domain/models/cargospace.model';
import {CargoPlanningService} from '../../../../src/business/services/plan/cargo-planning.service';
import {MockPlanDao} from '../../../../src/datastorage/plan/mockedDB/mock.plan.dao';
const faker = require("faker");
import * as sinon from "sinon";

init

describe('Cargo planning service', () => {
    let stubValue: CargoPlanning;

    before(() => {
        // Arrange
        stubValue = {
            id: '',
            loadList: [],
            loadDistribution: {leftKg: 10, rightKg: 15},
            cargoLayoutSteps: [],
            title: '',
            cargoLayout: [],
            cargoSpace: new Cargospace(1, '', 10, 10, 10, 100),
            completed: false,
            note: '',
            createdOn: faker.date.past(),
            loadDirection: '',
            amountOfOriginalProducts: 1
        };
    })

    afterEach(function() {
        sinon.restore();
    });

    it('D-TC02: getAllUnusedCargoplannings returns a promise of type CargoPlanning[]', (done) => {
        // Arrange
        const stub = sinon.stub(MockPlanDao.prototype, "getAllUnusedCargoPlannings").callsFake(function() {
            return Promise.resolve([stubValue, stubValue]);
        });

        // Act
        const cpService = new CargoPlanningService();
        cpService.getAllUnusedCargoPlannings()
            .then((cargoPlannings) => {
                // Assert
                expect(stub.calledOnce).to.be.true;
                expect(cargoPlannings.length).to.equal(2);
                expect(cargoPlannings[0]).to.eql(stubValue)

                done();
            })
    })

    it('D-TC03: markCargoPlanningAsCompleted returns a promise of type CargoPlanning', (done) => {
        // Arrange
        const stub = sinon.stub(MockPlanDao.prototype, "markCargoPlanningAsCompleted").callsFake(function() {
            return Promise.resolve(stubValue);
        });

        // Act
        const cpService = new CargoPlanningService();
        cpService.markCargoPlanningAsCompleted({})
            .then((cargoPlanning) => {
                // Assert
                expect(stub.calledOnce).to.be.true;
                expect(cargoPlanning).to.eql(stubValue)

                done();
            })
    })

    it('D-TC04: addNoteToCargoPlanning returns a promise of type CargoPlanning', (done) => {
        // Arrange
        const stub = sinon.stub(MockPlanDao.prototype, "addNoteToCargoPlanning").callsFake(function() {
            return Promise.resolve(stubValue);
        });

        // Act
        const cpService = new CargoPlanningService();
        cpService.addNoteToCargoPlanning('1', {})
            .then((cargoPlanning) => {
                // Assert
                expect(stub.calledOnce).to.be.true;
                expect(cargoPlanning).to.eql(stubValue)

                done();
            })
    })
})
