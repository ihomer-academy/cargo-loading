import {Container} from 'typescript-ioc';
import Config from '../src/config/config';
import {PlanDao} from '../src/datastorage/plan/plan.dao';
import {MockPlanDao} from '../src/datastorage/plan/mockedDB/mock.plan.dao';

before(function() {
    Container.bindName('Configuration').to(Config(true))
    Container.bind(PlanDao).to(MockPlanDao);
})
