"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const js_yaml_1 = __importDefault(require("js-yaml"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
function default_1() {
    // Read the constants of the constants.yml file and convert the response to Config type
    const constants = js_yaml_1.default.load(fs_1.default.readFileSync(path_1.default.resolve('./src/config/constants.yml'), 'utf8'));
    return constants;
}
exports.default = default_1;
//# sourceMappingURL=config.js.map