"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlanService = void 0;
const algorithm_factory_1 = require("./algorithms/factories/algorithm.factory");
/**
 * Class between the controller and the implementation
 */
class PlanService {
    // Plan function
    static plan(cargospace, orders, alg) {
        // Factory to get the right algorithm implementation
        const ALGORITHM = algorithm_factory_1.AlgorithmFactory.createAlgorithm({ cargoSpace: cargospace, orders: orders, type: alg });
        // Make cargo layout
        return ALGORITHM.makeCargoPlanning();
    }
}
exports.PlanService = PlanService;
//# sourceMappingURL=plan.service.js.map