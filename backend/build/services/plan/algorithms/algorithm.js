"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Algorithm = void 0;
const loaditem_model_1 = require("../../../models/loaditem.model");
const cargolayout_service_1 = require("../cargolayout.service");
const debug_1 = __importDefault(require("debug"));
const coordinates_model_1 = require("../../../models/coordinates.model");
/**
 * Abstract class that represents an algorithm.
 * This class also contains functions that will be used for all algorithms (such as checking if there is place and placing the products)
 */
class Algorithm {
    constructor(algorithmConfig) {
        this.log = debug_1.default('cargoServer');
        this.loadList = []; // Loadlist that will be used by the loaders to see the sequence of the products
        this.orderList = []; // List with the orders that needs to be included in the planning
        this.cargolayout = []; // 3d array that represents the cargo layout
        this.rightBeginX = 0; // x value where the right part of the cargospace starts
        this.leftWeightTotal = 0; // total weight on left side in kilograms
        this.rightWeightTotal = 0; // total weight on right side in kilograms
        this.leftPerctWeight = 0; // left side weight in comparison to the total weight (in percents)
        this.rightPerctWeight = 0; // right side weight in comparison to the total weight (in percents)
        this.highestYValue = -1; // highest y value that is loaded (if first p is 2 voxels high, value will be 1)
        this.amountOfFilledVoxels = 0; // to determine of the cargospace is fully loaded
        this.getRightXVoxelsToLoop = (x, productToPlace) => {
            const X_COORDINATES = [];
            for (let xx = x; xx < x + productToPlace.gridWidth; xx++) {
                X_COORDINATES.push(xx);
            }
            return X_COORDINATES;
        };
        this.getLeftXVoxelsToLoop = (x, productToPlace) => {
            const X_COORDINATES = [];
            for (let xx = x; xx >= x - (productToPlace.gridWidth - 1); xx--) {
                X_COORDINATES.push(xx);
            }
            return X_COORDINATES;
        };
        // Get configuration options
        this.cargoSpace = algorithmConfig.cargoSpace;
        this.orderList = algorithmConfig.orders;
        // Create multidimensional array with the right amount of voxels
        this.cargolayout = cargolayout_service_1.CargolayoutService.initializeEmptyCargoLayout(this.cargoSpace);
        this.rightBeginX = Math.ceil(this.cargoSpace.gridWidth / 2); // This is the x value where the right side of the cargospace starts
    }
    /*
     * Check if the cargospace boundaries are not exceeded when placing the product on the specified location
     */
    productFitsCargospaceBoundaries(coordinates) {
        return (coordinates.y < this.cargoSpace.gridHeight && coordinates.y >= 0) &&
            (coordinates.x < this.cargoSpace.gridWidth && coordinates.x >= 0) &&
            (coordinates.z < this.cargoSpace.gridLength && coordinates.z >= 0);
    }
    // Check if the current product fits in the cargospace with as start voxel(y, x and z) and moving to the right
    productMeetsRightRequirements(coordinates, productToPlace) {
        return this.productMeetsRequirements(coordinates, productToPlace, this.getRightXVoxelsToLoop(coordinates.x, productToPlace));
    }
    // Check if the current product fits in the cargospace with as start voxel(y, x and z) and moving to the left
    productMeetsLeftRequirements(coordinates, productToPlace) {
        return this.productMeetsRequirements(coordinates, productToPlace, this.getLeftXVoxelsToLoop(coordinates.x, productToPlace));
    }
    /*
     * General functions
    */
    productMeetsRequirements(coordinates, productToPlace, x_coordinates) {
        const { Y_COORDINATES, Z_COORDINATES } = this.getYAndZVoxelsToLoop(coordinates.y, coordinates.z, productToPlace);
        const COORDINATES_OF_ALL_VOXELS_TO_LOOP = this.cartesianProduct([Y_COORDINATES, x_coordinates, Z_COORDINATES]);
        let tempTotalLeft = this.leftWeightTotal; // Bind current left weight total value
        let tempTotalRight = this.rightWeightTotal; // Bind current right weight total value
        /*
         * Array to save the coordinates. If the product can be placed on the current start location, then we use this value
         *  when placing the product so we don't need to do this 3x for loop twice
         */
        const VOXEL_COORDINATES_ARRAY = [];
        /*
            Loop cartesian product coordinates
         */
        for (const COORDINATES_ARRAY of COORDINATES_OF_ALL_VOXELS_TO_LOOP) {
            const yy = COORDINATES_ARRAY[0], xx = COORDINATES_ARRAY[1], zz = COORDINATES_ARRAY[2];
            const COORDINATES = new coordinates_model_1.Coordinates(yy, xx, zz);
            if (!this.productFitsCargospaceBoundaries(COORDINATES)) {
                return {
                    response: false,
                    coordinatesWeight: {
                        voxelCoordinatesArray: [],
                        leftWeight: 0,
                        rightWeight: 0
                    }
                };
            }
            // Check if placing (a part of) the product in this voxel meets the weight and measurement requirements
            const { meetsRequirements, tempLeftWeight, tempRightWeight } = this.meetsAllRequirements(coordinates.y, COORDINATES, productToPlace, tempTotalLeft, tempTotalRight);
            // Return false if filling the voxel does not meet requirements
            if (!meetsRequirements) {
                return {
                    response: false,
                    coordinatesWeight: {
                        voxelCoordinatesArray: [],
                        leftWeight: 0,
                        rightWeight: 0
                    }
                };
            }
            // Update temporary weights
            tempTotalLeft = tempLeftWeight;
            tempTotalRight = tempRightWeight;
            VOXEL_COORDINATES_ARRAY.push(new coordinates_model_1.Coordinates(yy, xx, zz)); // Add current coordinates to the voxel array
        }
        // Check if cargo is evenly distributed when placing the product on the specified location and return response and coordinatesWeight
        return this.returnCoordinatesWeight(tempTotalLeft, tempTotalRight, VOXEL_COORDINATES_ARRAY);
    }
    /*
     * Place product on specified location
     */
    placeProduct(productCounter, productToPlace, order, toRight, coordinatesWeight) {
        // Call the right voxel loop function based on the direction
        this.loopCoordinatesAndPlaceProductInCargolayout(productCounter, coordinatesWeight);
        // Calculate new distribution of the weight in percents
        const TOTAL_LOADED_WEIGHT = this.leftWeightTotal + this.rightWeightTotal;
        if (this.leftWeightTotal > 0) {
            this.leftPerctWeight = this.leftWeightTotal / TOTAL_LOADED_WEIGHT * 100;
        }
        if (this.rightWeightTotal > 0) {
            this.rightPerctWeight = this.rightWeightTotal / TOTAL_LOADED_WEIGHT * 100;
        }
        // Show cargolayout visualisation after placing a product
        this.log(`After placing: p.id = ${productToPlace.id} with counter: ${productCounter}`);
        cargolayout_service_1.CargolayoutService.visualizeCargoLayout(this.cargoSpace, this.cargolayout);
        this.loadList.push(new loaditem_model_1.LoadItem(productCounter, order, productToPlace)); // Add item to load list
        /**
         * Remove product of productsList (because it is already placed)
         * If product of the current order is 0 after removing the current product, then remove the whole order (shift),
         *  because we can take the next order
         */
        const ALREADY_LOOPED_PRODUCTS = [];
        // Filter current product out of products array
        this.orderList[0].products = this.orderList[0].products.filter(loopedProduct => {
            if (ALREADY_LOOPED_PRODUCTS.indexOf(loopedProduct) > -1) {
                return true;
            }
            ALREADY_LOOPED_PRODUCTS.push(loopedProduct);
            return loopedProduct !== productToPlace;
        });
        // Remove whole order if no other products left
        if (this.orderList[0].products.length === 0) {
            this.orderList.shift();
        }
    }
    /**
     * Remove an already placed product of the cargolayout by loadlist number
     */
    removeProduct(productCounter) {
        this.log("Before removing product with nr: " + productCounter);
        const LOAD_ITEM = this.getLoadItemById(productCounter); // Get loaditem based on nr
        if (LOAD_ITEM) {
            this.log(`Remove p.id = ${LOAD_ITEM.product.id} of cargolayout`);
            this.removeProductOfCargolayout(productCounter, LOAD_ITEM.product); // Remove of cargolayout
            this.addPlacedOrderBackToOrderlist(LOAD_ITEM); // Add product back to orderlist
            this.loadList = this.loadList.filter(li => li.id !== productCounter); // Remove product from loadlist (because it isn't placed anymore)
        }
    }
    /**
     * Check if the cargo space is compconstely full (chance is very low that this is true)
     */
    checkIfCargoSpaceIsFull() {
        const AMOUNT_OF_VOXELS = this.cargoSpace.gridHeight * this.cargoSpace.gridWidth * this.cargoSpace.gridLength;
        return AMOUNT_OF_VOXELS === this.amountOfFilledVoxels;
    }
    /**
     * Get loaditem by id
     */
    getLoadItemById(id) {
        return this.loadList.find(loadItem => loadItem.id === id);
    }
    /**
     * Check on which side of the cargospace the voxel needs to be filled
     * Add voxel box weight to the corresponding side
     */
    addWeightPerVoxelToSide(xx, weightPerVoxel, leftTotal, rightTotal) {
        /*
         * If the x axis of the current voxel is less than the start x value of the right side AND the width is odd, then add the weight of the voxel to the left side
         * Or if the x axis of the current voxel is less than the start x value of the right side minus one, then add the weight of the voxel to the left side
        */
        if (this.cargoSpace.gridWidth % 2 === 0 && xx < this.rightBeginX ||
            xx < this.rightBeginX - 1) {
            leftTotal += weightPerVoxel;
        }
        // If the x axis of the current voxel is equal or more than the start x value of the right side, then add the weight of the voxel to the right side
        if (xx >= this.rightBeginX) {
            rightTotal += weightPerVoxel;
        }
        // Return the new side weight values as an object
        return {
            tempLeftWeight: leftTotal,
            tempRightWeight: rightTotal
        };
    }
    /*
     * Function that checks of placing the product will result in an evenly distributed cargo and will not break other products
     * Requirement 1. Placing the product on the specified location should result in an evenly distributed cargo
     * Requirement 2. Placing a product on top of another is only possible if the product(s) below can manage the weight of the product to place
    */
    productMeetsWeightRequirements(coordinates, productToPlace, start_y, tempTotalLeft, tempTotalRight) {
        // Calculate weight per voxel
        const WEIGHT_PER_VOXEL = productToPlace.weight / (productToPlace.gridWidth * productToPlace.gridHeight * productToPlace.gridLength);
        const Y_FLOOR = start_y === 0; // True if voxel is located on the first (0th) layer, false if y > 0
        // Get weight of left and right side when placing the product
        const { tempLeftWeight, tempRightWeight } = this.addWeightPerVoxelToSide(coordinates.x, WEIGHT_PER_VOXEL, tempTotalLeft, tempTotalRight);
        if (!Y_FLOOR) {
            /**
             * Check if the product to place is not heavier than the product one layer below
             *  because a heavier product cannot be placed on top of a product that weighs less
             */
            if (!this.checkIfProductBelowCanHandleWeightOfProductToPlace(coordinates, productToPlace)) {
                return {
                    fits: false,
                    tempLeftWeight: 0,
                    tempRightWeight: 0
                };
            }
            /*
              * If the product to place is placed on top of other products, we need to check if all products
              *  below can manage the sum of the weight on top of it
            */
            if (!this.checkIfProductsBelowCanHandleWeightOnTopOfIt(coordinates, productToPlace)) {
                return {
                    fits: false,
                    tempLeftWeight: 0,
                    tempRightWeight: 0
                };
            }
        }
        return {
            fits: true,
            tempLeftWeight,
            tempRightWeight
        };
    }
    /*
     * Place product in the cargo layout when it meets all requirements
     */
    loopCoordinatesAndPlaceProductInCargolayout(productCounter, coordinatesWeight) {
        // Loop saved coordinates
        coordinatesWeight.voxelCoordinatesArray.forEach((coordinates) => {
            // Place an unique number (unique for this layout) in the cargospace 'voxel' to recognize which product is planned on what location
            this.cargolayout[coordinates.y][coordinates.z][coordinates.x] = productCounter.toString().padStart(2, "0");
            this.updateHighestYValue(coordinates.y); // Update highest y value
            this.amountOfFilledVoxels++; // Add one to the amountOfFilledVoxels
        });
        // Add voxel weight to the right side
        this.leftWeightTotal = coordinatesWeight.leftWeight;
        this.rightWeightTotal = coordinatesWeight.rightWeight;
    }
    /*
     * Remove an already placed product of the cargolayout
     */
    removeProductOfCargolayout(productCounter, productToPlace) {
        // Calculate weight of the product per voxel
        const WEIGHT_PER_VOXEL = productToPlace.weight / (productToPlace.gridWidth * productToPlace.gridHeight * productToPlace.gridLength);
        const COORDINATES_OF_ALL_VOXELS_TO_LOOP = this.cartesianProduct(this.getAllCargospaceYAndXAndZVoxelsToLoop());
        // Loop all voxels
        for (const COORDINATES_ARRAY of COORDINATES_OF_ALL_VOXELS_TO_LOOP) {
            const Y = COORDINATES_ARRAY[0], X = COORDINATES_ARRAY[1], Z = COORDINATES_ARRAY[2];
            const PRODUCT_COUNTER_VALUE = productCounter.toString().padStart(2, "0");
            // If loadlist number is equal to the voxel value, change voxel value back to '00'
            if (this.cargolayout[Y][Z][X] === PRODUCT_COUNTER_VALUE) {
                this.cargolayout[Y][Z][X] = '00';
                // Remove weight per box of the left weight total
                if (this.cargoSpace.gridWidth % 2 === 0 && X < this.rightBeginX ||
                    X < this.rightBeginX - 1) {
                    this.leftWeightTotal -= WEIGHT_PER_VOXEL;
                }
                // Remove weight per box of the right weight total
                if (X >= this.rightBeginX) {
                    this.rightWeightTotal -= WEIGHT_PER_VOXEL;
                }
                // Remove one of the amount of filled voxels
                this.amountOfFilledVoxels--;
            }
        }
    }
    getAllCargospaceYAndXAndZVoxelsToLoop() {
        const Y_COORDINATES = [];
        const X_COORDINATES = [];
        const Z_COORDINATES = [];
        for (let y = 0; y < this.cargoSpace.gridHeight; y++) {
            Y_COORDINATES.push(y);
        }
        for (let x = 0; x < this.cargoSpace.gridWidth; x++) {
            X_COORDINATES.push(x);
        }
        for (let z = 0; z < this.cargoSpace.gridLength; z++) {
            Z_COORDINATES.push(z);
        }
        return [
            Y_COORDINATES,
            X_COORDINATES,
            Z_COORDINATES
        ];
    }
    /*
     * Add placed product (coming from an order) back to the orderlist so it can be placed again on another location
     */
    addPlacedOrderBackToOrderlist(loadItem) {
        // Check if there is still an order in the orderlist with the same order id
        const LOAD_ITEM_IS_IN_LIST = this.orderList.find(o => o.id === loadItem.order.id);
        if (LOAD_ITEM_IS_IN_LIST) {
            // If the order is available in the list, add product in front (unshift) of order.products array
            this.orderList[0].products.unshift(loadItem.product);
        }
        else {
            // If the order is not existing anymore (because the whole order was already processed), "recreate" the order and add the product
            const ORDER = loadItem.order;
            ORDER.products.push(loadItem.product);
            this.orderList.unshift(ORDER);
        }
    }
    /*
     * Check if the product below the voxel to fill can handle the weight of the product to place
     * With 'handle' we mean that the weight on top is less than the product's weight
     */
    checkIfProductBelowCanHandleWeightOfProductToPlace(coordinates, productToPlace) {
        // Get corresponding loaditem below current one (yy-1)
        const LOAD_ITEM = this.getLoadItemById(+this.cargolayout[coordinates.y - 1][coordinates.z][coordinates.x]);
        // Return false if product on top weighs more
        return LOAD_ITEM && LOAD_ITEM.product.weight >= productToPlace.weight || !LOAD_ITEM;
    }
    /*
     * Check if product can handle the weight of the products on top of it (including the product to place)
     */
    checkIfProductsBelowCanHandleWeightOnTopOfIt(coordinates, productToPlace) {
        var _a;
        // Loop all layers from bottom to top
        for (let layerLoop = 0; layerLoop < coordinates.y; layerLoop++) {
            // Get weight of product on bottom layer
            const WEIGHT_OF_BOTTOM_PRODUCT = (_a = this.getLoadItemById(+this.cargolayout[layerLoop][coordinates.z][coordinates.x])) === null || _a === void 0 ? void 0 : _a.product.weight;
            /**
             * Add the weight of the product to place (p.weight) to the total weight on top of the bottom product
             * because if we place this product on the current location, it will also be on top of it
             */
            let totalWeightOnTopOfBottomProduct = productToPlace.weight;
            // Loop all layers on top of current layer (layerLoop + 1)
            for (let layerTopLoop = layerLoop + 1; layerTopLoop < coordinates.y; layerTopLoop++) {
                // Get loaditem of looped layer
                const LOAD_ITEM = this.getLoadItemById(+this.cargolayout[layerTopLoop][coordinates.z][coordinates.x]);
                // Add weight of product to total weight
                if (LOAD_ITEM) {
                    totalWeightOnTopOfBottomProduct += LOAD_ITEM.product.weight;
                }
            }
            // Product cannot be placed on this location if the total weight on top is more than the weight of the bottom product
            if (WEIGHT_OF_BOTTOM_PRODUCT &&
                WEIGHT_OF_BOTTOM_PRODUCT < totalWeightOnTopOfBottomProduct) {
                return false;
            }
        }
        return true; // Return true if all weight requirements are passed
    }
    /*
     * Update the highestYValue variable if the current y value is bigger than the current highestYValue
     */
    updateHighestYValue(current_y) {
        if (current_y > this.highestYValue) {
            this.highestYValue = current_y;
        }
    }
    /*
     * Check if filling the voxel meets the measurement requirements
     * Return true if voxel is empty and not floating
     */
    checkIfFillingVoxelMeetsMeasurementRequirements(start_y, coordinates) {
        /*
         * Check if voxel is not empty, because if the voxel is already filled we cannot place another product on that location
         */
        if (this.cargolayout[coordinates.y][coordinates.z][coordinates.x] !== '00') {
            this.log(`Voxel with coordinates: (y=${coordinates.y}, z=${coordinates.z}, x=${coordinates.x}) is already filled with another product`);
            return false;
        }
        /*
          * Validate if product is not floating in the air
          * With 'floating' we mean that the product is not placed on the floor (so yy > 0) and that the voxel with coordinates(yy - 1, zz, xx) is not empty ('00')
          * We only need to check this for the first layer of the product to place (that's why yy === y is added)
          *  because if we don't do this and the product's height is more than 1 voxel, then the condition will be true while checking the second layer
          * We use yy - 1 to look at the voxel below the one where we would like to place the product
        */
        return coordinates.y <= 0 ||
            coordinates.y !== start_y ||
            this.cargolayout[coordinates.y - 1][coordinates.z][coordinates.x] !== '00';
    }
    /*
     * Return true if y is 0 OR y is bigger than 0 and voxel below current one is not empty
     */
    yIsNotFloating(coordinates) {
        return coordinates.y === 0 ||
            coordinates.y > 0 && this.cargolayout[coordinates.y - 1][coordinates.z][coordinates.x] !== '00';
    }
    /*
     * Check if the cargo is evenly distributed when placing all voxels
     */
    cargoIsStillEvenlyDistributed(tempTotalLeft, tempTotalRight) {
        // Calculate the new weight percents
        const TOTAL_TEMP_WEIGHT = tempTotalLeft + tempTotalRight;
        const LEFT_PERCENTAGE = tempTotalLeft / TOTAL_TEMP_WEIGHT * 100;
        const RIGHT_PERCENTAGE = tempTotalRight / TOTAL_TEMP_WEIGHT * 100;
        this.log(`tmpleft=${tempTotalLeft}, tmpright=${tempTotalRight}, leftperct=${LEFT_PERCENTAGE}, rightperct=${RIGHT_PERCENTAGE}`);
        this.log(`rightWeightTotal=${this.rightWeightTotal}, leftWeightTotal=${this.leftWeightTotal}`);
        /*
         * If the difference between one side relative to the other side is more than 20%, then return false
         *   because the cargo is not evenly distributed anymore
         */
        return this.rightWeightTotal === 0 ||
            this.leftWeightTotal === 0 ||
            LEFT_PERCENTAGE >= RIGHT_PERCENTAGE && LEFT_PERCENTAGE - RIGHT_PERCENTAGE <= 20 ||
            RIGHT_PERCENTAGE >= LEFT_PERCENTAGE && RIGHT_PERCENTAGE - LEFT_PERCENTAGE <= 20;
    }
    /*
     * Check if placing (a part of) the product in the specified voxel meets the weight and measurement requirements
     */
    meetsAllRequirements(start_y, coordinates, productToPlace, tempTotalLeft, tempTotalRight) {
        // Check measurement requirements
        if (!this.checkIfFillingVoxelMeetsMeasurementRequirements(start_y, coordinates)) {
            return {
                meetsRequirements: false,
                tempLeftWeight: 0,
                tempRightWeight: 0
            };
        }
        // Check weight requirements
        const { fits, tempLeftWeight, tempRightWeight } = this.productMeetsWeightRequirements(coordinates, productToPlace, start_y, tempTotalLeft, tempTotalRight);
        // Return false if the product to place does not meet the weight requirements, If false is returned, we do nothing with the temporary weights
        if (!fits) {
            return {
                meetsRequirements: false,
                tempLeftWeight: 0,
                tempRightWeight: 0
            };
        }
        return {
            meetsRequirements: true,
            tempLeftWeight: tempLeftWeight,
            tempRightWeight: tempRightWeight
        };
    }
    returnCoordinatesWeight(tempTotalLeft, tempTotalRight, voxelCoordinatesArray) {
        return {
            response: this.cargoIsStillEvenlyDistributed(tempTotalLeft, tempTotalRight),
            coordinatesWeight: {
                voxelCoordinatesArray: voxelCoordinatesArray,
                leftWeight: tempTotalLeft,
                rightWeight: tempTotalRight
            }
        };
    }
    cartesianProduct(arr) {
        return arr.reduce(function (a, b) {
            return a.map(function (x) {
                return b.map(function (y) {
                    // @ts-ignore
                    return x.concat([y]);
                });
            }).reduce(function (c, d) { return c.concat(d); }, []);
        }, [[]]);
    }
    voxelIsAlreadyFilled(coordinates) {
        return this.cargolayout[coordinates.y][coordinates.z][coordinates.x] !== '00';
    }
    getYAndZVoxelsToLoop(y, z, productToPlace) {
        const Y_COORDINATES = [];
        const Z_COORDINATES = [];
        for (let yy = y; y <= this.highestYValue + 2 && yy < y + productToPlace.gridHeight; yy++) {
            Y_COORDINATES.push(yy);
        }
        for (let zz = z; zz < z + productToPlace.gridLength; zz++) {
            Z_COORDINATES.push(zz);
        }
        return {
            Y_COORDINATES,
            Z_COORDINATES
        };
    }
}
exports.Algorithm = Algorithm;
//# sourceMappingURL=algorithm.js.map