"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlgorithmFactory = void 0;
const backtracking_1 = require("../types/backtracking");
const greedy_1 = require("../types/greedy");
/**
 * Factory to select the corresponding algorithm class
 */
class AlgorithmFactory {
    static createAlgorithm(algorithmConfig) {
        switch (algorithmConfig.type) {
            case 'backtracking': {
                return new backtracking_1.Backtracking(algorithmConfig);
            }
            case 'greedy': {
                return new greedy_1.Greedy(algorithmConfig);
            }
            default: {
                return new backtracking_1.Backtracking(algorithmConfig);
            }
        }
    }
}
exports.AlgorithmFactory = AlgorithmFactory;
//# sourceMappingURL=algorithm.factory.js.map