"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Backtracking = void 0;
const algorithm_1 = require("../algorithm");
const cargolayout_service_1 = require("../../cargolayout.service");
const product_orientation_enum_1 = require("../../../../models/enum/product.orientation.enum");
const order_model_1 = require("../../../../models/order.model");
const debug_1 = __importDefault(require("debug"));
const coordinates_model_1 = require("../../../../models/coordinates.model");
class Backtracking extends algorithm_1.Algorithm {
    constructor() {
        super(...arguments);
        this.log = debug_1.default('cargoServer');
        this.toRight = false;
        this.order = new order_model_1.Order(0, '', '', []);
        this.products = [];
    }
    // Implementation of the abstract function makeCargoPlanning of the Algorithm abstract class
    makeCargoPlanning() {
        // planCargo returns true if the planning is successfully made
        if (this.planCargo()) {
            this.log("Successfully made a cargo plan!");
            cargolayout_service_1.CargolayoutService.visualizeCargoLayout(this.cargoSpace, this.cargolayout); // Function to visualize the multidimensional array
            this.log(`Weight distribution: left: ${this.leftWeightTotal}, right: ${this.rightWeightTotal}`);
            this.log(`Weight distribution: left: ${this.leftPerctWeight}, right: ${this.rightPerctWeight}`);
            // Return the multidimensional array and the loadlist (to see the sequence of the products) as AlgorithmResponseModel
            return Promise.resolve({
                cargoLayout: this.cargolayout,
                loadList: this.loadList,
                loadDistribution: { leftKg: this.leftWeightTotal, rightKg: this.rightWeightTotal }
            });
        }
        else {
            // Product(s) couldn't be placed in the cargospace
            this.log("Making a cargo plan failed");
            // ToDo: Meegeven welke objecten niet pasten of juist wel zodat dit aan de planner kan worden getoond
            return Promise.reject('failed');
        }
    }
    /**
     * Backtracking implementation to find a location for the products
     */
    planCargo() {
        // Check if cargospace is compconstely full or that all orders are already processed in the cargospace
        if (this.orderList.length === 0) {
            this.log("All orders are already processed");
            return true;
        }
        if (this.checkIfCargoSpaceIsFull()) {
            this.log("Cargo space is full");
            return false;
        }
        this.order = this.orderList[0]; // Get first order of the orderlist (because of the loading order)
        this.products = this.order.products; // Get products of the first order
        this.log(`l=${this.leftPerctWeight} - r=${this.rightPerctWeight}`);
        this.log(`l=${this.leftWeightTotal} - r=${this.rightWeightTotal}`);
        // If the left side is heavier than the right side, than we place the next product on the right side
        this.toRight = this.leftPerctWeight > this.rightPerctWeight;
        return this.loopVoxelsAndCheckIfProductCanBePlaced();
    }
    /*
     * Loop all voxels of the right side from left to right and check of a product from the current order can be placed on these start coordinates
     */
    loopVoxelsAndCheckIfProductCanBePlaced() {
        const { Y_COORDINATES, Z_COORDINATES } = this.getCargospaceYAndZVoxelsToLoop();
        let x_coordinates;
        if (this.toRight) {
            x_coordinates = this.getRightCargospaceXVoxelsToLoop();
        }
        else {
            x_coordinates = this.getLeftCargospaceXVoxelsToLoop();
        }
        const COORDINATES_OF_ALL_VOXELS_TO_LOOP = this.cartesianProduct([Y_COORDINATES, x_coordinates, Z_COORDINATES]);
        for (const COORDINATES_ARRAY of COORDINATES_OF_ALL_VOXELS_TO_LOOP) {
            const y = COORDINATES_ARRAY[0], x = COORDINATES_ARRAY[1], z = COORDINATES_ARRAY[2];
            const COORDINATES = new coordinates_model_1.Coordinates(y, x, z);
            if (this.yIsNotFloating(COORDINATES) &&
                this.loopProductAndCheckIfItCanFitOnStartPosition(COORDINATES)) {
                return true;
            }
        }
        return false; // return false if no product of the current order fits
    }
    /*
     * Loop all products of the current order and check if one of them can fit in the cargolayout with the specified start coordinates and direction
     */
    loopProductAndCheckIfItCanFitOnStartPosition(coordinates) {
        this.log(`New voxel coordinates: y=${coordinates.y} x=${coordinates.x} z=${coordinates.z}`);
        // Return false if voxel is already filled
        if (this.voxelIsAlreadyFilled(coordinates)) {
            return false;
        }
        const ALREADY_TRIED_PRODUCTS = [];
        // Loop all products of the current order
        for (const PRODUCT of this.products) {
            this.log(`Product to try: p.id=${PRODUCT.id}, index=${ALREADY_TRIED_PRODUCTS.indexOf(PRODUCT)}`);
            /*
             * If the order contains two identical products, and the first one did not fit,
             *  then we do not need to check if the second (identical) product can fit in the cargo layout
             *  because if the first one didn't fit, then the second one will also not fit
             */
            if (ALREADY_TRIED_PRODUCTS.indexOf(PRODUCT) === -1) {
                ALREADY_TRIED_PRODUCTS.push(PRODUCT); // Add product to the ALREADY_TRIED_PRODUCTS array
                // Check if product is a square or not because changing the orientation of a square is unnecessary
                if (PRODUCT.gridWidth === PRODUCT.gridLength) {
                    if (this.canPlaceProductWithBacktracking(coordinates, PRODUCT)) {
                        return true;
                    }
                }
                else {
                    // Loop all kind of orientations and check if the product can fit with the changed orientation
                    for (let o = 0; o <= Object.keys(product_orientation_enum_1.ProductOrientation).length / 2; o++) {
                        PRODUCT.changeOrientation(o.toString()); // Change orientation of product to the current looped orientation
                        // Return true if the product can be placed
                        if (this.canPlaceProductWithBacktracking(coordinates, PRODUCT)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false; // Return false if none of the products of the current order fits in the cargo layout
    }
    /*
     * Check if product meets requirements. If so, place product and call recursive planCargo function
     */
    checkIfProductCanBePlacedOnCurrentCoordinates(coordinates, productToPlace) {
        const CURRENT_PRODUCT_COUNTER = this.loadList.length + 1;
        // If direction is right, then check right requirements, otherwise check left requirements
        if (this.toRight) {
            // First check if placing the product to the right fits. If not, then try it to the left
            const MEETS_RIGHT_REQUIREMENTS = this.productMeetsRightRequirements(coordinates, productToPlace);
            if (MEETS_RIGHT_REQUIREMENTS.response) {
                // If product meets right measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct(CURRENT_PRODUCT_COUNTER, productToPlace, this.order, true, MEETS_RIGHT_REQUIREMENTS.coordinatesWeight);
                // Contains recursive call
                return this.returnPlanCargoResponse();
            }
            const MEETS_LEFT_REQUIREMENTS = this.productMeetsLeftRequirements(coordinates, productToPlace);
            if (MEETS_LEFT_REQUIREMENTS.response) {
                // If product meets left measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct(CURRENT_PRODUCT_COUNTER, productToPlace, this.order, false, MEETS_LEFT_REQUIREMENTS.coordinatesWeight);
                // Recursive call
                return this.returnPlanCargoResponse();
            }
        }
        else {
            // First check if placing the product to the left fits. If not, then try it to the right
            const MEETS_LEFT_REQUIREMENTS = this.productMeetsLeftRequirements(coordinates, productToPlace);
            if (MEETS_LEFT_REQUIREMENTS.response) {
                // If product meets left measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct(CURRENT_PRODUCT_COUNTER, productToPlace, this.order, false, MEETS_LEFT_REQUIREMENTS.coordinatesWeight);
                // Recursive call
                return this.returnPlanCargoResponse();
            }
            const MEETS_RIGHT_REQUIREMENTS = this.productMeetsRightRequirements(coordinates, productToPlace);
            if (MEETS_RIGHT_REQUIREMENTS.response) {
                // If product meets right measurement- and weight requirements, the product can be placed in the cargospace
                this.placeProduct(CURRENT_PRODUCT_COUNTER, productToPlace, this.order, true, MEETS_RIGHT_REQUIREMENTS.coordinatesWeight);
                // Recursive call
                return this.returnPlanCargoResponse();
            }
        }
        // Return response: false and wasPlaced: false if product could not be placed
        return {
            response: false,
            wasPlaced: false
        };
    }
    /*
     * Check if product can be placed on current coordinates
     * If product can be placed (wasPlaced) but reponse is false, then remove the placed product and try another location
     */
    canPlaceProductWithBacktracking(coordinates, productToPlace) {
        // Product counter is used in the loadlist. In the load list you can see which number belongs to which product
        const CURRENT_PRODUCT_COUNTER = this.loadList.length + 1;
        const CAN_BE_PLACED = this.checkIfProductCanBePlacedOnCurrentCoordinates(coordinates, productToPlace);
        if (CAN_BE_PLACED.response) {
            return true;
        }
        else if (CAN_BE_PLACED.wasPlaced) {
            /**
             * Not all products can be placed, so we need to backtrack
             * Backtracking results in undoing the last correctly done step
             * Undoing in this case results in removing the last placed product
             */
            this.removeProduct(CURRENT_PRODUCT_COUNTER);
        }
        return false; // Return false in all other cases
    }
    returnPlanCargoResponse() {
        if (this.planCargo()) {
            return {
                response: true,
                wasPlaced: true
            };
        }
        else {
            return {
                response: false,
                wasPlaced: true
            };
        }
    }
    getCargospaceYAndZVoxelsToLoop() {
        const Y_COORDINATES = [];
        const Z_COORDINATES = [];
        for (let y = 0; y <= this.highestYValue + 2 && y < this.cargoSpace.gridHeight; y++) {
            Y_COORDINATES.push(y);
        }
        for (let z = 0; z < this.cargoSpace.gridLength; z++) {
            Z_COORDINATES.push(z);
        }
        return {
            Y_COORDINATES,
            Z_COORDINATES
        };
    }
    getRightCargospaceXVoxelsToLoop() {
        const X_COORDINATES = [];
        for (let x = this.rightBeginX; x < this.cargoSpace.gridWidth; x++) {
            X_COORDINATES.push(x);
        }
        return X_COORDINATES;
    }
    getLeftCargospaceXVoxelsToLoop() {
        const X_COORDINATES = [];
        for (let x = this.rightBeginX - 1; x >= 0; x--) {
            X_COORDINATES.push(x);
        }
        return X_COORDINATES;
    }
}
exports.Backtracking = Backtracking;
//# sourceMappingURL=backtracking.js.map