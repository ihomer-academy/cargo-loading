"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Greedy = void 0;
const algorithm_1 = require("../algorithm");
class Greedy extends algorithm_1.Algorithm {
    makeCargoPlanning() {
        return Promise.resolve({ loadDistribution: { leftKg: 0, rightKg: 0 }, loadList: [], cargoLayout: [] });
    }
}
exports.Greedy = Greedy;
//# sourceMappingURL=greedy.js.map