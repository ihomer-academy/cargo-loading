"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CargolayoutService = void 0;
const debug_1 = __importDefault(require("debug"));
/**
 * Class that contains functions related to the cargo layout
 */
class CargolayoutService {
    /**
     * Function to initialize an empty cargo layout based on the chosen cargospace
     */
    static initializeEmptyCargoLayout(cargospace) {
        const cargolayout = []; // Empty cargolayout (without size)
        for (let iy = 0; iy < cargospace.gridHeight; iy++) {
            cargolayout[iy] = new Array(cargospace.gridWidth).fill('00'); // Fill gives the array elements a value
            for (let iz = 0; iz < cargospace.gridLength; iz++) {
                cargolayout[iy][iz] = new Array(cargospace.gridWidth).fill('00'); // Fill gives the array elements a value
            }
        }
        return cargolayout; // Return 3d array that represents the cargo layout
    }
    /**
     * Function to visualize the cargo layout
     */
    static visualizeCargoLayout(cargospace, cargospace3d) {
        const log = debug_1.default('cargoServer');
        for (let ih = 0; ih < cargospace.gridHeight; ih++) {
            log("---- Laag  " + ih + " ----");
            for (let il = 0; il < cargospace.gridLength; il++) {
                log(cargospace3d[ih][il]);
            }
        }
    }
}
exports.CargolayoutService = CargolayoutService;
//# sourceMappingURL=cargolayout.service.js.map