"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var backtracking_1 = __importDefault(require("../plan/algorithms/backtracking"));
var AlgorithmFactory = /** @class */ (function () {
    function AlgorithmFactory() {
    }
    AlgorithmFactory.createAlgorithm = function (type) {
        var algorithm;
        switch (type) {
            case 'backtracking': {
                algorithm = new backtracking_1.default();
                break;
            }
            default: {
                algorithm = new backtracking_1.default();
                break;
            }
        }
        return algorithm;
    };
    return AlgorithmFactory;
}());
exports.default = AlgorithmFactory;
