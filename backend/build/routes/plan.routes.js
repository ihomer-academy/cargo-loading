"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const router = require('express').Router();
const plan_controller_1 = __importDefault(require("../controllers/plan.controller"));
// Plan routes
router.post('/', plan_controller_1.default.planCargoLayout);
exports.default = router;
//# sourceMappingURL=plan.routes.js.map