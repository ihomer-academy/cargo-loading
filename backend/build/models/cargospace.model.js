"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cargospace = void 0;
const typescript_ioc_1 = require("typescript-ioc");
/**
 * Model that represents a cargospace (= the space to be loaded)
 */
class Cargospace {
    constructor(id, name, length, width, height, maxWeight) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.width = width;
        this.height = height;
        this.maxWeight = maxWeight;
        /**
         * The real length, width and height of the cargospace will be converted to virtual version.
         * Math.floor is used to round down the virtual value
         * For example: real length = 625 cm; virtual length = 12. We don't want more voxels in the cargospace than there in reality is
         */
        const VOXELSIZE = this.config.voxelSize;
        this.gridLength = Cargospace.calculateAmountOfVoxels(length, VOXELSIZE);
        this.gridWidth = Cargospace.calculateAmountOfVoxels(width, VOXELSIZE);
        this.gridHeight = Cargospace.calculateAmountOfVoxels(height, VOXELSIZE);
    }
    static calculateAmountOfVoxels(value, voxelSize) {
        return Math.floor(value / voxelSize);
    }
}
__decorate([
    typescript_ioc_1.InjectValue("Configuration"),
    __metadata("design:type", Object)
], Cargospace.prototype, "config", void 0);
exports.Cargospace = Cargospace;
//# sourceMappingURL=cargospace.model.js.map