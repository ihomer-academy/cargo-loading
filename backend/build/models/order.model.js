"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
/**
 * Model that represents an order (an order can contain multiple products)
 */
class Order {
    constructor(id, customer, address, products) {
        this.id = id;
        this.customer = customer;
        this.address = address;
        this.products = products;
    }
}
exports.Order = Order;
//# sourceMappingURL=order.model.js.map