"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadItem = void 0;
/**
 * Model that represents a loaded item (this will be used for the loadlist that the loaders will use)
 */
class LoadItem {
    constructor(id, order, product) {
        this.id = id;
        this.order = order;
        this.product = product;
    }
}
exports.LoadItem = LoadItem;
//# sourceMappingURL=loaditem.model.js.map