"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductOrientation = void 0;
/**
 * Enum to change the orientation of a product
 * If there is a space available with size 1x4 (WxL), but the product is 3x1, then the product will not fit
 * But if we turn the product (so the size will be: 1x3), then it will fit
 */
var ProductOrientation;
(function (ProductOrientation) {
    ProductOrientation[ProductOrientation["Z_IS_LENGTH"] = 0] = "Z_IS_LENGTH";
    ProductOrientation[ProductOrientation["X_IS_LENGTH"] = 1] = "X_IS_LENGTH";
})(ProductOrientation = exports.ProductOrientation || (exports.ProductOrientation = {}));
//# sourceMappingURL=product.orientation.enum.js.map