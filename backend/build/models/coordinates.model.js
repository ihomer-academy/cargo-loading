"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Coordinates = void 0;
/**
 * Model that represents the coordinates of a voxel
 */
class Coordinates {
    constructor(y, x, z) {
        this.y = y;
        this.x = x;
        this.z = z;
    }
}
exports.Coordinates = Coordinates;
//# sourceMappingURL=coordinates.model.js.map