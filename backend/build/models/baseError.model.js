"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseError = void 0;
/**
 * Error model that will be used when there need to be an error returned with additional information
 */
class BaseError {
    constructor(message, originalAction, body) {
        this.message = message;
        this.originalAction = originalAction;
        this.body = body;
    }
}
exports.BaseError = BaseError;
//# sourceMappingURL=baseError.model.js.map