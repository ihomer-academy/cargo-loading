"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
const product_orientation_enum_1 = require("./enum/product.orientation.enum");
const typescript_ioc_1 = require("typescript-ioc");
/**
 * Model that represents a product
 */
class Product {
    constructor(id, name, length, width, height, weight) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        // Convert the real dimensions to virtual dimensions (= amount of voxels)
        this.gridLength = this.calculateAmountOfVoxels(length);
        this.gridWidth = this.calculateAmountOfVoxels(width);
        this.gridHeight = this.calculateAmountOfVoxels(height);
    }
    /**
     * Function to convert the real dimensions to virtual dimensions
     */
    calculateAmountOfVoxels(value) {
        return Math.ceil(value / this.config.voxelSize);
    }
    /**
     * Function to change the orientation of a product
     */
    changeOrientation(o) {
        switch (+o) {
            case product_orientation_enum_1.ProductOrientation.Z_IS_LENGTH:
                this.gridLength = this.calculateAmountOfVoxels(this.length);
                this.gridWidth = this.calculateAmountOfVoxels(this.width);
                break;
            case product_orientation_enum_1.ProductOrientation.X_IS_LENGTH:
                this.gridLength = this.calculateAmountOfVoxels(this.width);
                this.gridWidth = this.calculateAmountOfVoxels(this.length);
                break;
            default:
                break;
        }
    }
}
__decorate([
    typescript_ioc_1.InjectValue("Configuration"),
    __metadata("design:type", Object)
], Product.prototype, "config", void 0);
exports.Product = Product;
//# sourceMappingURL=product.model.js.map