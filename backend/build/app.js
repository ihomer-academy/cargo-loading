"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const plan_routes_1 = __importDefault(require("./routes/plan.routes"));
const env_config = require('./config/environment-config.json');
const cors = require('cors');
const path = require('path');
const typescript_ioc_1 = require("typescript-ioc");
const config_1 = __importDefault(require("./config/config"));
var app = express();
app.use(cors());
app.use(express.static(path.join(__dirname, '../')));
app.use(morgan('dev'));
app.use(bodyParser.json());
// Dependency injection
typescript_ioc_1.Container.bindName('Configuration').to(config_1.default()); // Bind Config object (based on yml file) to name 'Configuration'
// Routes
app.use('/api/plan', plan_routes_1.default);
// app.use('/api/orders', orderRoutes)
// app.use('/api/cargospaces', cargoSpaceRoutes)
app.use('*', (err, req, res, next) => { });
// Accept requests of the client-side
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
    next();
});
app.listen(env_config.port, () => console.log(`Cargo load backend listening on port ${env_config.port}!`));
// module.exports = app
exports.default = app;
//# sourceMappingURL=app.js.map