"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const plan_service_1 = require("../services/plan/plan.service");
const cargospace_model_1 = require("../models/cargospace.model");
const product_model_1 = require("../models/product.model");
const baseError_model_1 = require("../models/baseError.model");
class PlanController {
    planCargoLayout(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // Convert cargo space object to Cargospace class
            const CARGOSPACE = new cargospace_model_1.Cargospace(req.body.cargospace.id, req.body.cargospace.name, req.body.cargospace.length, req.body.cargospace.width, req.body.cargospace.height, req.body.cargospace.maxWeight);
            const ORDERS = req.body.orders;
            // Convert product-elements in Order object to Product class
            ORDERS.forEach(order => {
                order.products.forEach((product, index, product_array) => {
                    product_array[index] = new product_model_1.Product(product.id, product.name, product.length, product.width, product.height, product.weight);
                });
            });
            // Call planService to make a planning with the selected cargospace, orders and algorithm (response is a promise)
            try {
                const RESPONSE = yield plan_service_1.PlanService.plan(CARGOSPACE, ORDERS, req.body.algorithm);
                // Made a planning successfully
                res.status(200).send(RESPONSE);
            }
            catch (e) {
                // Failed making a planning
                res.status(400).send(new baseError_model_1.BaseError('Planning cargo layout failed', 'Plan cargo layout'));
            }
        });
    }
}
exports.default = new PlanController();
//# sourceMappingURL=plan.controller.js.map