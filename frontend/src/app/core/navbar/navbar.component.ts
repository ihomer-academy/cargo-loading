import {Component, OnInit} from '@angular/core';
import {PlanService} from '../../plan/create/_services/plan.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  step = 0;             // used to only underline the menu items that are already finished
  isPlanning = false;   // used to know when to show the planning steps

  constructor(
    private planService: PlanService
  ) {}

  ngOnInit(): void {
    this.planService.step.subscribe(nr => {
      this.step = nr;
    })

    this.planService.isPlanning.subscribe(value => {
      this.isPlanning = value;
    })
  }

}
