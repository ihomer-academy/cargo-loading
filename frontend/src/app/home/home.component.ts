import {Component, OnInit} from '@angular/core';
import {PlanService} from '../plan/create/_services/plan.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private planService: PlanService
  ) {}

  ngOnInit(): void {
    this.planService.isPlanning.next(false)   // set isplanning false because user is not planning right now
    this.planService.resetPlanService();           // clear plan service
  }
}
