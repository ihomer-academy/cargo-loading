import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PlanInformationComponent} from './plan/create/plan-information.component';
import {CargospaceComponent} from './plan/create/cargospace/cargospace.component';
import {OrdersComponent} from './plan/create/orders/orders.component';
import {OverviewComponent} from './plan/create/overview/overview.component';
import {FloorPlanComponent} from './plan/visualisations/floorplan/floorplan.component';
import {CargoPlanningComponent} from './plan/get/cargo-planning/cargo-planning.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'plan/information', component: PlanInformationComponent},
  {path: 'plan/kies-laadruimte', component: CargospaceComponent},
  {path: 'plan/kies-bestellingen', component: OrdersComponent},
  {path: 'plan/overzicht', component: OverviewComponent},
  {path: 'plan/plattegrond-weergave', component: FloorPlanComponent},
  {path: 'plan/selecteer-planning', component: CargoPlanningComponent},

  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
