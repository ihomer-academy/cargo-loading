import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Laadsysteem';

  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event): void {
    $event.returnValue = 'Wil je de site verlaten?';
  }
}
