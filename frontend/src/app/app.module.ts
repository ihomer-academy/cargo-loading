import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {HttpClientModule} from '@angular/common/http';
import localeNl from '@angular/common/locales/nl';
import {registerLocaleData} from '@angular/common';
import {ChartsModule} from 'ng2-charts';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {NavbarComponent} from './core/navbar/navbar.component';
import {PlanInformationComponent} from './plan/create/plan-information.component';
import {CargospaceComponent} from './plan/create/cargospace/cargospace.component';
import {OrdersComponent} from './plan/create/orders/orders.component';
import {OverviewComponent} from './plan/create/overview/overview.component';
import {FloorPlanComponent} from './plan/visualisations/floorplan/floorplan.component';
import {LoadlistComponent} from './plan/visualisations/loadlist/loadlist.component';
import {CargoPlanningComponent} from './plan/get/cargo-planning/cargo-planning.component';
import {DoneButtonComponent} from './plan/visualisations/done-button/done-button.component';
import {NoteFieldComponent} from './plan/visualisations/note-field/note-field.component';
import {CargospaceDialogComponent} from './plan/create/cargospace/cargospace-dialog/cargospace-dialog.component';
import {CargoLoadingSettingsDialogComponent} from './plan/create/overview/cargo-loading-settings-dialog/cargo-loading-settings-dialog.component';
import {DonutComponent} from './plan/_components/donut/donut.component';
import {LoadingInformationComponent} from './plan/_components/loading-information/loading-information.component';
import {ProgressDialogComponent} from './plan/create/overview/progress-dialog/progress-dialog.component';
import {DefaultDialogComponent} from './plan/_components/default-dialog/default-dialog.component'

import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatSliderModule} from '@angular/material/slider';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

registerLocaleData(localeNl);

const components = [
  AppComponent,
  HomeComponent,
  NavbarComponent,
  PlanInformationComponent,
  CargospaceComponent,
  OrdersComponent,
  OverviewComponent,
  FloorPlanComponent,
  LoadlistComponent,
  CargoPlanningComponent,
  DoneButtonComponent,
  NoteFieldComponent,
  CargospaceDialogComponent,
  CargoLoadingSettingsDialogComponent,
  DonutComponent,
  LoadingInformationComponent,
  ProgressDialogComponent,
  DefaultDialogComponent
];
const matImports = [
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatExpansionModule,
  MatIconModule,
  MatMenuModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTableModule,
  MatSliderModule,
  MatProgressSpinnerModule
]
const otherImports = [
  MDBBootstrapModule.forRoot(),
  BrowserModule,
  AppRoutingModule,
  BrowserAnimationsModule,
  ReactiveFormsModule,
  DragDropModule,
  HttpClientModule,
  FormsModule,
  ChartsModule,
]

@NgModule({
  declarations: components,
  imports: [
    otherImports,
    matImports
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'nl-NL'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
