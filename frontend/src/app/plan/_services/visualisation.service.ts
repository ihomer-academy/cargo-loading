import { Injectable } from '@angular/core';
import {Cargospace} from '../_models/cargospace.model';
import {LoadDirection} from '../_models/loaddirection.model';
import {Order} from '../_models/order.model';
import {CargoLayout, LoadDistribution} from '../_models/types/types';

@Injectable({
  providedIn: 'root'
})
export class VisualisationService {
  id:                       string;
  cargoSpace:               Cargospace;
  cargoLayout:              CargoLayout;
  loadList;
  loadDistribution:         LoadDistribution;
  cargoLayoutSteps:         CargoLayout[];
  title:                    string;
  note =                    '';
  amountOfOriginalProducts: number;
  loadDirection:            LoadDirection;
  selectedOrders:           Order[];
  createdOn:                Date;

  view =                    false;

  constructor() { }
}
