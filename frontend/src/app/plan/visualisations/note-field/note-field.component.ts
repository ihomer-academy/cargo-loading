import { Component, OnInit } from '@angular/core';
import {CargoplanService} from '../../get/_services/cargoplan.service';
import {VisualisationService} from '../../_services/visualisation.service';

/*
 * field to fill in the note as a planner or watch the note as a loader
 */
@Component({
  selector: 'app-note-field',
  templateUrl: './note-field.component.html',
  styleUrls: ['./note-field.component.scss']
})
export class NoteFieldComponent implements OnInit {
  note:       string;
  view:       boolean;
  savedNote   = false;
  aligned     = 'right';

  constructor(
    private cargoPlanService: CargoplanService,
    private visualisationService: VisualisationService,
  ) {}

  ngOnInit(): void {
    this.view = this.visualisationService.view;
    this.note = this.visualisationService.note;
    if (this.view && this.note === '') {
      this.note = 'Geen notitie'
    }
  }

  saveValue(): void {
    this.visualisationService.note = this.note;
  }

  saveNote(): void {
    const note = this.visualisationService.note;
    if (note && note !== '') {
      this.cargoPlanService.addNoteToCargoPlanning(this.visualisationService.id, note).subscribe(() => {
        this.savedNote = true;
      })
    } else {
      this.savedNote = true;
    }
    this.aligned = 'center';
  }
}
