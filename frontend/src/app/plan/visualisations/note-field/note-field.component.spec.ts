import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteFieldComponent } from './note-field.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('NoteFieldComponent', () => {
  let component: NoteFieldComponent;
  let fixture: ComponentFixture<NoteFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ NoteFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
