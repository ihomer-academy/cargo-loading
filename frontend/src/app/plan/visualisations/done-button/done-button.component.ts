import { Component, OnInit } from '@angular/core';
import {CargoplanService} from '../../get/_services/cargoplan.service';
import {VisualisationService} from '../../_services/visualisation.service';
import {Router} from '@angular/router';

/*
 * button to mark the plan as completed or to close the visualisation
 */
@Component({
  selector: 'app-done-button',
  templateUrl: './done-button.component.html',
  styleUrls: ['./done-button.component.scss']
})
export class DoneButtonComponent implements OnInit {
  showButton =  true;
  view:         boolean;

  constructor(
    private cargoPlanService: CargoplanService,
    private visualisationService: VisualisationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.view = this.visualisationService.view;
  }

  close(): void {
    this.router.navigate(['/']);
  }

  markAsCompleted(): void {
    this.cargoPlanService.markCargoplanningAsCompleted(this.visualisationService.id).subscribe(() => {
      this.showButton = false;
    })
  }

}
