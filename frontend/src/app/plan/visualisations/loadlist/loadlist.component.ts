import {Component, OnInit} from '@angular/core';
import {Cargospace} from '../../_models/cargospace.model';
import {VisualisationService} from '../../_services/visualisation.service';
import {Order} from '../../_models/order.model';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {HeaderItem} from '../../_models/types/types';
import {PlanService} from '../../create/_services/plan.service';

/*
 * loadlist
 */
@Component({
  selector: 'app-loadlist',
  templateUrl: './loadlist.component.html',
  styleUrls: ['./loadlist.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class LoadlistComponent implements OnInit {
  loadList          = [];
  cargoSpace:       Cargospace;
  leftPerct:        number;
  rightPerct:       number;
  leftKg:           number;
  rightKg:          number;
  title:            string;
  note:             string;
  fullyDone         = false;
  processed         = 0;
  amountOfOriginal: number;
  expandedElement:  Order[] = [];

  headers: HeaderItem[];

  constructor(
    private visualisationService: VisualisationService,
    private planService: PlanService
  ) {}

  ngOnInit(): void {
    this.headers = this.planService.getHeaders();

    if (this.visualisationService.title === '') {
      this.title = 'Geen titel'
    } else {
      this.title = this.visualisationService.title;
    }
    this.note               = this.visualisationService.note;
    this.amountOfOriginal   = this.visualisationService.amountOfOriginalProducts;

    this.loadList           = this.visualisationService.loadList;
    this.cargoSpace         = this.visualisationService.cargoSpace;
    const {leftKg, rightKg} = this.visualisationService.loadDistribution;

    const totalWeight = leftKg + rightKg;
    this.leftPerct  = Math.round(leftKg / totalWeight * 100);
    this.leftKg     = Math.round(leftKg);
    this.rightPerct = Math.round(rightKg / totalWeight * 100);
    this.rightKg    = Math.round(rightKg);

    this.loadList.forEach((li) => {
      this.processed += li.loadItems.length;
    })

    this.fullyDone = this.amountOfOriginal === this.processed;
  }

  checkExpanded(element): boolean {
    return this.planService.checkExpanded(element, this.expandedElement)
  }

  pushPopElement(element): void {
    this.expandedElement = this.planService.pushPopElement(element, this.expandedElement);
  }

}
