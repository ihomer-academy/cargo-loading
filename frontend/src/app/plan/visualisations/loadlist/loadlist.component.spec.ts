import {ComponentFixture, TestBed} from '@angular/core/testing';

import {LoadlistComponent} from './loadlist.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Cargospace} from '../../_models/cargospace.model';
import {LoadDirection} from '../../_models/loaddirection.model';
import {PlanService} from '../../create/_services/plan.service';
import {VisualisationService} from '../../_services/visualisation.service';
import {NoteFieldComponent} from '../note-field/note-field.component';

describe('LoadlistComponent', () => {
  let component: LoadlistComponent;
  let fixture: ComponentFixture<LoadlistComponent>;

  const mockVisualisationService = {
    loadDistribution: {leftKg: 0, rightKg: 0},
    loadList: []
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: VisualisationService, useValue: mockVisualisationService }],
      declarations: [LoadlistComponent, NoteFieldComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
