import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as p5 from 'p5';
import {VisualisationService} from '../../_services/visualisation.service';
import {CargospacesService} from '../../create/_services/cargospaces.service';
import {OrdersService} from '../../create/_services/orders.service';
import {PlanService} from '../../create/_services/plan.service';
import {CargoLayout} from '../../_models/types/types';

/*
 * floorplans to show the cargo layout
 */
@Component({
  selector: 'app-floorplan',
  templateUrl: './floorplan.component.html',
  styleUrls: ['./floorplan.component.scss']
})
export class FloorPlanComponent implements AfterViewInit, OnInit {
  loadList          = [];
  cargoLayoutSteps: CargoLayout[];
  cargoLayout:      CargoLayout = [];
  colors            = [];
  step              = 0;
  backtracking      = false;
  view:             boolean;
  pageHeight:       number;
  createdOn:        Date;

  squareSize        = 30;

  constructor(
    private visualisationService: VisualisationService,
    private cargoService: CargospacesService,
    private ordersService: OrdersService,
    private planService: PlanService
  ) {}

  ngOnInit(): void {
    this.planService.step.next(4);

    this.view =             this.visualisationService.view;
    this.cargoLayoutSteps = this.visualisationService.cargoLayoutSteps;
    this.cargoLayout =      this.visualisationService.cargoLayout;
    this.loadList =         this.visualisationService.loadList;
    this.step =             this.cargoLayoutSteps.length - 1;
    this.createdOn =        this.visualisationService.createdOn;

    const cargoSpaceId = localStorage.getItem('cargoSpace');
    this.visualisationService.cargoSpace = this.cargoService.getCargoSpaces().find(cs => cs.id === +cargoSpaceId)

    const orderIds = localStorage.getItem('selectedOrders');
    if (orderIds) {
      const orderList = this.ordersService.getOrdersDeliveryTomorrow();
      const array = JSON.parse('[' + orderIds + ']');
      const orders = [];
      array.map(id => orders.push(orderList.find(o => o.id === id)))
      this.planService.orders = orders;
    }

    if (this.cargoLayoutSteps.length > this.getAmountOfProducts()) {
      this.backtracking = true;
    }
  }

  ngAfterViewInit(): void {
    this.showCargoLayout();
  }

  private showCargoLayout(): void {
    // Make a floorplan for all layers
    for (let layer = 0; layer < this.cargoLayout.length; layer++) {
      const sketch = (s) => {
        const cols = this.cargoLayout[layer][0].length; // Amount of columns
        const rows = this.cargoLayout[layer].length;    // Amount of rows

        s.setup = () => {
          const canvas2 = s.createCanvas(this.squareSize * cols, this.squareSize * rows); // One square is this.squareSizexthis.squareSize
          canvas2.parent(`floorplan-holder-${layer}`);          // Place floorplan in corresponding element

          const amtOfProducts = this.getAmountOfProducts();

          // Make an array with x amount of color where x is equal to the amount of products
          if (this.colors.length === 0) {
            this.makeColorArray(cols, rows, amtOfProducts, s);
          }
          s.noLoop();
        };

        s.draw = () => {
          s.background('#d8d8d8'); // Background color of an empty square

          // Loop all 'squares'
          for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
              const x = i * this.squareSize; // Width of square is this.squareSize
              const y = j * this.squareSize; // Height of square is this.squareSize
              const cargoLayoutValue = this.cargoLayout[layer][j][i]; // for example: '01' or '05'

              // Fill background with a color of the color array
              s.colorMode(s.HSL);
              s.fill(this.colors[+cargoLayoutValue]);
              s.stroke('#dbdbdb');              // Stroke color is black
              s.rect(x, y, this.squareSize, this.squareSize);             // Draw the square
              s.fill(255);                      // Color of text is white
              s.textAlign(s.CENTER, s.CENTER);  // Horizontal and vertical align the text

              if (cargoLayoutValue !== '00') {
                s.text(cargoLayoutValue, x, y, this.squareSize, this.squareSize); // Set loading number in het middle of the square
              }
            }
          }
        };
      };

      const canvas = new p5(sketch);
    }

    this.pageHeight = window.innerHeight; // set page height so it will not scroll when replaying the steps
  }

  private showCargoLayoutSteps(
    first = false,
    clear = false
  ): void {
    if (first) {
      this.step = 0;  // reset step
    }

    if (clear) {
      // clear content of the current elements
      for (let layer = 0; layer < this.cargoLayoutSteps[this.step].length; layer++) {
        const el = document.getElementById(`floorplan-holder-${layer}`);
        el.innerHTML = '';
      }
    }

    // Loop all steps
    for (let layer = 0; layer < this.cargoLayoutSteps[this.step].length; layer++) {
      const sketch = (s) => {
        const cols = this.cargoLayoutSteps[this.step][layer][0].length; // Amount of columns
        const rows = this.cargoLayoutSteps[this.step][layer].length;    // Amount of rows

        s.setup = () => {
          const canvas2 = s.createCanvas(this.squareSize * cols, this.squareSize * rows); // One square is this.squareSizexthis.squareSize
          canvas2.parent(`floorplan-holder-${layer}`);          // Place floorplan in corresponding element

          const amtOfProducts = this.getAmountOfProducts();

          // Make an array with x amount of color where x is equal to the amount of products
          if (this.colors.length === 0) {
            this.makeColorArray(cols, rows, amtOfProducts, s);
          }
          s.noLoop();
        };

        s.draw = () => {
          s.background('#d8d8d8');

          // Loop all 'squares'
          for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
              const x = i * this.squareSize; // Width of square is this.squareSize
              const y = j * this.squareSize; // Height of square is this.squareSize
              const cargoLayoutValue = this.cargoLayoutSteps[this.step][layer][j][i];

              // Fill background with a color of the color array
              s.colorMode(s.HSL);
              s.fill(this.colors[+cargoLayoutValue]);
              s.stroke('#dbdbdb');              // Stroke color is black
              s.rect(x, y, this.squareSize, this.squareSize);             // Draw the square
              s.fill(255);                      // Color of text is white
              s.textAlign(s.CENTER, s.CENTER);  // Horizontal and vertical align the text

              if (cargoLayoutValue !== '00') {
                s.text(cargoLayoutValue, x, y, this.squareSize, this.squareSize); // Set loading number in het middle of the square
              }
            }
          }
        };
      };

      const canvas = new p5(sketch);
    }

    if (this.step < this.cargoLayoutSteps.length - 1) {
      setTimeout(() => {
        this.step++;
        this.showCargoLayoutSteps(false, true);
      }, 250);
    }
  }

  public replay(): void {
    this.step = 0;
    this.pageHeight = window.innerHeight;
    this.showCargoLayoutSteps(true, true);
  }

  // Make a 2d array based on the columns and rows
  private make2DArray(c, r): [][] {
    const arr = new Array(c);
    for (let i = 0; i < arr.length; i++) {
      arr[i] = new Array(r);
    }
    return arr;
  }

  private makeColorArray(
    cols,
    rows,
    amtOfProducts,
    s
  ): void {
    this.colors = this.make2DArray(cols, rows);
    this.colors[0] = [0, 0, 95]; // first color value (empty)

    let colorH = 250; // start hue
    let colorL = 90;  // start lightness
    const step = 15;

    for (let i = 1; i <= amtOfProducts; i++) {
      const colorS = s.random(55, 100); // random saturation

      this.colors[i] = [colorH, colorS, colorL]; // Random combination that represents the color
      if (colorL > step) {
        colorL -= step;
      } else {
        colorH -= step;
        colorL = 90;
      }
    }
  }

  private getAmountOfProducts(): number {
    // Get amount of product of the loadlist item
    let c = 0;
    for (const li of this.loadList) {
      c += li.loadItems.length;
    }
    return c;
  }
}
