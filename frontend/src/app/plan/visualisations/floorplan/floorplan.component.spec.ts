import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FloorPlanComponent} from './floorplan.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LoadlistComponent} from '../loadlist/loadlist.component';
import {DoneButtonComponent} from '../done-button/done-button.component';
import {LoadingInformationComponent} from '../../_components/loading-information/loading-information.component';
import {VisualisationService} from '../../_services/visualisation.service';
import {RouterTestingModule} from '@angular/router/testing';
import {NoteFieldComponent} from '../note-field/note-field.component';
import {DonutComponent} from '../../_components/donut/donut.component';
import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('FloorPlanComponent', () => {
  let component: FloorPlanComponent;
  let fixture: ComponentFixture<FloorPlanComponent>;

  const mockVisualisationService = {
    cargoLayoutSteps: [],
    loadList: [],
    loadDistribution: {leftKg: 0, rightKg: 0},
    view: true,
    cargoLayout: [],
    createdOn: new Date()
  }

  @Component({
    selector: 'app-loadlist',
    template: '<p>Mocked load list</p>'
  })
  class MockAppLoadList {}

  @Component({
    selector: 'app-done-button',
    template: '<p>Mocked done button</p>'
  })
  class MockDoneButton {}

  @Component({
    selector: 'app-loading-information',
    template: '<p>Mocked loading information</p>'
  })
  class MockLoadingInformation {}


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [FloorPlanComponent, MockAppLoadList, MockDoneButton, MockLoadingInformation],
      providers: [
        { provide: VisualisationService, useValue: mockVisualisationService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorPlanComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
