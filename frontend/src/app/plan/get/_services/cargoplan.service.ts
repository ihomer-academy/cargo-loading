import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CargoPlanning} from '../../_models/cargo-planning';
import {environment} from '../../../../environments/environment';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {PlanService} from '../../create/_services/plan.service';
import {VisualisationService} from '../../_services/visualisation.service';
import {CargospacesService} from '../../create/_services/cargospaces.service';

@Injectable({
  providedIn: 'root'
})
export class CargoplanService {
  apiUrl        =   environment.API_URL;
  cargoPlanning:    CargoPlanning;

  constructor(
    private http: HttpClient,
    private planService: PlanService,
    private cargoService: CargospacesService,
    private visualisationService: VisualisationService
  ) {}

  getAllNotUsedCargoPlannings(): Observable<CargoPlanning[]> {
      return this.http.get(this.apiUrl + '/api/plan/cargoplannings').pipe(
        map(response => {
          return response as CargoPlanning[];
        }),
        catchError(this.handleError<any>('getCargoPlannings'))
    );
  }

  markCargoplanningAsCompleted(
    id: string
  ): Observable<CargoPlanning> {
    return this.http.put(this.apiUrl + '/api/plan/cargoplanning/completed', {_id: id}).pipe(
      map(response => {
        return response as CargoPlanning;
      }),
      catchError(this.handleError<any>('markCargoPlanningAsCompleted'))
    );
  }

  addNoteToCargoPlanning(
    id: string,
    note: string
  ): Observable<CargoPlanning> {
    return this.http.put(`${this.apiUrl}/api/plan/cargoplanning/${id}`, {note}).pipe(
      map(response => {
        return response as CargoPlanning;
      }),
      catchError(this.handleError<any>('addNoteToCargoPlanning'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      console.error(error) // log to console instead
      return of(result as T)
    }
  }

  setCargoPlanning(
    selectedCargoPlanning: CargoPlanning
  ): void {
    this.visualisationService.id                        = selectedCargoPlanning._id;
    this.visualisationService.loadDistribution          = selectedCargoPlanning.loadDistribution;
    this.visualisationService.cargoLayout               = selectedCargoPlanning.cargoLayout;
    this.visualisationService.cargoSpace                = selectedCargoPlanning.cargoSpace;
    this.visualisationService.loadList                  = this.planService.convertLoadlist(selectedCargoPlanning.loadList);
    this.visualisationService.cargoLayoutSteps          = selectedCargoPlanning.cargoLayoutSteps;
    this.visualisationService.amountOfOriginalProducts  = selectedCargoPlanning.amountOfOriginalProducts;
    this.planService.cargoSpace                         = this.visualisationService.cargoSpace;
    this.visualisationService.createdOn                 = selectedCargoPlanning.createdOn;
    this.visualisationService.title                     = this.setValueOrSetDefault(selectedCargoPlanning.title);
    this.visualisationService.note                      = this.setValueOrSetDefault(selectedCargoPlanning.note);

    this.visualisationService.loadDirection = this.cargoService.getLoadDirections().find(ld => {
      return ld.id === selectedCargoPlanning.loadDirection
    });

    this.calculateNewValues();
  }

  public setValueOrSetDefault(
    value: string
  ): string {
    if (value === '') {
      return '-'
    }
    return value;
  }

  private calculateNewValues(): void {
    // set default values
    let totalProductWeight    = 0;
    let totalProductVolume    = 0;
    let amountOfProducts      = 0;
    let amountOfProductsPerct = 0;
    let weightPerct           = 0;
    let volumePerct           = 0;

    if (this.visualisationService.loadList && this.visualisationService.loadList.length > 0) {
      // amountOfProducts = this.visualisationService.loadList.length;

      this.visualisationService.loadList.map(wholeLi => {
        wholeLi.loadItems.map(li => {
          totalProductWeight += li.product.weight; // Add product weight to total weight
          totalProductVolume += li.product.length * li.product.width * li.product.height;
          amountOfProducts++;
        })
      });

      const cargoSpace = this.visualisationService.cargoSpace;
      weightPerct = Math.round(totalProductWeight / cargoSpace.maxWeight * 100)
      const totalCargospaceVolume = cargoSpace.width * cargoSpace.height * cargoSpace.length;
      volumePerct = Math.round(totalProductVolume / totalCargospaceVolume * 100)

      amountOfProductsPerct = 100;
    }
    else {
      if (totalProductWeight > 0) {
        totalProductWeight    = 0;
        totalProductVolume    = 0;
        amountOfProducts      = 0;
        weightPerct           = 0;
        volumePerct           = 0;
        amountOfProductsPerct = 0;
      }
    }

    this.planService.currentWeight.next(weightPerct);
    this.planService.currentVolume.next(volumePerct);
    this.planService.amtOfProducts.next(amountOfProducts);
    this.planService.amountOfProductsPerct.next(amountOfProductsPerct);
  }
}
