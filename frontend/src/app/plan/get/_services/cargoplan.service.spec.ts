import { TestBed } from '@angular/core/testing';

import { CargoplanService } from './cargoplan.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CargoplanService', () => {
  let service: CargoplanService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(CargoplanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
