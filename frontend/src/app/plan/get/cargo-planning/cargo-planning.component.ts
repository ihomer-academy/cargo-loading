import { Component, OnInit } from '@angular/core';
import {CargoplanService} from '../_services/cargoplan.service';
import {CargoPlanning} from '../../_models/cargo-planning';
import {Router} from '@angular/router';
import {VisualisationService} from '../../_services/visualisation.service';
import {PlanService} from '../../create/_services/plan.service';

@Component({
  selector: 'app-cargo-planning',
  templateUrl: './cargo-planning.component.html',
  styleUrls: ['./cargo-planning.component.scss']
})
export class CargoPlanningComponent implements OnInit {
  cargoPlanningList:      CargoPlanning[] = [];
  selectedCargoPlanning:  CargoPlanning;

  constructor(
    private cargoplanService: CargoplanService,
    private visualisationService: VisualisationService,
    private planService: PlanService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCargoPlannings();
    this.planService.isPlanning.next(false)
  }

  private getCargoPlannings(): void {
    this.cargoplanService.getAllNotUsedCargoPlannings()
      .subscribe({
        next: plannings => {
          this.cargoPlanningList = plannings
        },
        error: err => {
          err = err.json()
        }
      })
  }

  confirmCargoPlanning(): void {
    this.cargoplanService.setCargoPlanning(this.selectedCargoPlanning);
    this.visualisationService.view = true;
    this.router.navigate(['/plan/plattegrond-weergave']);
  }

  changeValues(): void {
    this.selectedCargoPlanning.title  = this.cargoplanService.setValueOrSetDefault(this.selectedCargoPlanning.title);
    this.selectedCargoPlanning.note   = this.cargoplanService.setValueOrSetDefault(this.selectedCargoPlanning.note);
  }
}
