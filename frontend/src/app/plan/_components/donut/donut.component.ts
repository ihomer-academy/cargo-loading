import {AfterViewInit, Component, Input} from '@angular/core';
import {Chart} from 'chart.js';
import {PlanService} from '../../create/_services/plan.service';
import {OrdersService} from '../../create/_services/orders.service';

@Component({
  selector: 'app-donut',
  templateUrl: './donut.component.html',
  styleUrls: ['./donut.component.scss']
})
export class DonutComponent implements AfterViewInit {
  @Input() typeOfDonut: string;
  @Input() showPlanning = false;

  public doughnutChartOptions: any;
  public doughnutChartData: [number, number] = [0, 100];  // Default data is: 0% selected, 100% not selected

  value;
  amountOfProducts = 0;

  constructor(
    private planService: PlanService,
    private ordersService: OrdersService
  ) {}

  canvas: any;
  ctx: any;

  blue = '#147dff';         // default color
  red = '#ff0000';          // color to use when value is more than 100%
  primaryColor = this.blue; // default primary color is the blue one

  ngAfterViewInit(): void {
    const orderList = this.ordersService.getOrdersDeliveryTomorrow();
    orderList.map(ol => this.amountOfProducts += ol.products.length); // Get total amount of products

    if (this.typeOfDonut === 'amt') {
      this.planService.amtOfProducts.subscribe((value) => {
        this.value = value
        this.draw();
      });

      this.planService.amountOfProductsPerct.subscribe((value) => {
        this.doughnutChartData = [value, 100 - value];
        this.draw();
      });

      this.draw();
    } else if (this.typeOfDonut === 'weight') {
      this.planService.currentWeight.subscribe((value) => {
        this.setValue(value);
      });
    } else if (this.typeOfDonut === 'volume') {
      this.planService.currentVolume.subscribe((value) => {
        this.setValue(value);
      });
    }

    // Sometimes the donuts were not shown correctly, so added an interval of 100ms that triggers the subscribes
    const intervalId = setInterval(() => {
      clearInterval(intervalId);
      this.planService.currentWeight.next(this.planService.currentWeight.value)
      this.planService.currentVolume.next(this.planService.currentVolume.value)
      this.planService.amtOfProducts.next(this.planService.amtOfProducts.value);

      // If donuts are shown on the visualisation page, then use standard 100% for the amount of products
      if (this.showPlanning) {
        this.planService.amountOfProductsPerct.next(100);
      } else {
        this.planService.amountOfProductsPerct.next(this.planService.amountOfProductsPerct.value);
      }
    }, 100)
  }

  // Function to create the donut
  private draw(): void {
    this.doughnutChartOptions = {
      cutoutPercentage: 80,       // thickness of the donut (big number is small thickness)
      legend: { display: false},
      labels: { display: false },
      tooltips: false,            // disable tooltip when hovering the donut
      type: this.typeOfDonut,     // type of doughnut (amt, weight or volume)
      hover: {mode: null},
      aspectRatio: 1
    };

    const bl = 'myChart-' + this.typeOfDonut;                         // get the doughnut id
    const canvas = document.getElementById(bl) as HTMLCanvasElement;  // select the element on the HTML page
    const ctx = canvas.getContext('2d');

    // tslint:disable-next-line:variable-name
    const _this = this;

    // Create donut/chart
    const myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: [''],
        datasets: [{
          label: '',
          data: this.doughnutChartData,     // data is array with two values. for example: [20, 80] (20% is blue)
          backgroundColor: [                // first value is primaryColor (selected), second value is grey (not selected)
            this.primaryColor,
            '#f6f8fa',
          ]
        }]
      },
      plugins: [{
        // Add text in center of doughnut
        beforeDraw(chart): void {
          const width = chart.width;
          const height = chart.height;

          // tslint:disable-next-line:no-shadowed-variable
          const ctx = chart.ctx;
          ctx.restore();
          ctx.textBaseline = 'middle';

          let text = '';
          if (_this.typeOfDonut === 'amt')
          {
            if (_this.showPlanning) {
              text = String(_this.value);
            } else {
              text = String(_this.value + ' / ' + _this.amountOfProducts);
            }
          } else if (_this.typeOfDonut === 'weight') {
            text = String(_this.value + ' %');
          } else if (_this.typeOfDonut === 'volume') {
            text = String(_this.value + ' %');
          }

          const textX = Math.round((width - ctx.measureText(text).width) / 2.1);   // x coordinate to start the text
          const textY = height / 2;                                                   // y coordinate to start the text
          ctx.font = 'bold 16px Inter';
          ctx.fillText(text, textX, textY);
          ctx.save();
        }
      }],
      options: this.doughnutChartOptions
    });
  }

  private setValue(value: number): void {
    this.value = value
    if (value > 100) {
      this.doughnutChartData = [100, 0];
      this.primaryColor = this.red;
    } else {
      this.primaryColor = this.blue;
      this.doughnutChartData = [value, 100 - value];
    }
    this.draw();
  }
}
