import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingInformationComponent } from './loading-information.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID} from '@angular/core';
import {PlanService} from '../../create/_services/plan.service';
import {Cargospace} from '../../_models/cargospace.model';
import localeNl from '@angular/common/locales/nl';
import {registerLocaleData} from '@angular/common';
import {LoadDirection} from '../../_models/loaddirection.model';
registerLocaleData(localeNl);

describe('LoadingInformationComponent', () => {
  let component: LoadingInformationComponent;
  let fixture: ComponentFixture<LoadingInformationComponent>;

  const mockPlanService = {
    cargoSpace: new Cargospace(1, 'name', 100, 100, 50, 500),
    loadDirection: new LoadDirection('dir', ''),
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ LoadingInformationComponent ],
      providers: [
        { provide: PlanService, useValue: mockPlanService },
        { provide: LOCALE_ID, useValue: 'nl-NL'},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
