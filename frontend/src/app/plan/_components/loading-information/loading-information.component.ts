import {Component, Input, OnInit} from '@angular/core';
import {LoadDirection} from '../../_models/loaddirection.model';
import {Cargospace} from '../../_models/cargospace.model';
import {Router} from '@angular/router';
import {PlanService} from '../../create/_services/plan.service';
import {CargospacesService} from '../../create/_services/cargospaces.service';
import {Order} from '../../_models/order.model';
import {VisualisationService} from '../../_services/visualisation.service';

@Component({
  selector: 'app-loading-information',
  templateUrl: './loading-information.component.html',
  styleUrls: ['./loading-information.component.scss']
})
export class LoadingInformationComponent implements OnInit {
  @Input() showPlanning: boolean;

  loadDirection:          LoadDirection;
  cargoSpace:             Cargospace;
  maxCargospaceWeight:    number;
  totalCargospaceVolume:  number;
  selectedOrders:         Order[] = [];

  leftPerct   = 25;
  rightPerct  = 25;

  constructor(
    private router: Router,
    private planService: PlanService,
    private cargoService: CargospacesService,
    private visualisationService: VisualisationService
  ) {}

  ngOnInit(): void {
    this.cargoSpace = this.planService.cargoSpace;

    if (this.showPlanning) {
      const leftKg = this.visualisationService.loadDistribution.leftKg;
      const rightKg = this.visualisationService.loadDistribution.rightKg;
      this.leftPerct = Math.round(leftKg / (leftKg + rightKg) * 100);
      this.rightPerct = Math.round(rightKg / (leftKg + rightKg) * 100);
    }

    if (this.cargoSpace) {
      this.maxCargospaceWeight = this.cargoSpace.maxWeight;
      this.totalCargospaceVolume = this.cargoSpace.height * this.cargoSpace.length
        * this.cargoSpace.width;

      if (this.planService.loadDirection) {
        this.loadDirection = this.planService.loadDirection;
      } else {
        this.loadDirection = this.visualisationService.loadDirection;
      }
    } else {
      const cargoSpaceId = localStorage.getItem('cargoSpace');
      this.cargoSpace = this.cargoService.getCargoSpaces().find(cs => cs.id === +cargoSpaceId)
      if (this.cargoSpace) {
        this.maxCargospaceWeight = this.cargoSpace.maxWeight;
        this.totalCargospaceVolume = this.cargoSpace.height * this.cargoSpace.length * this.cargoSpace.width;

        const loadDirId = localStorage.getItem('loadDirection');
        this.loadDirection = this.cargoService.getLoadDirections().find(ld => ld.id === loadDirId)
      }
    }

    this.selectedOrders = this.planService.orders;
  }

}
