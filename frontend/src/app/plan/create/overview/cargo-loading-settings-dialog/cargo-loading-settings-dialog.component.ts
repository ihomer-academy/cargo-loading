import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {Algorithm} from '../../../_models/algorithm.model';
import {PlanService} from '../../_services/plan.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

/*
 * Dialog to change the settings
 */
@Component({
  selector: 'app-cargo-loading-settings-dialog',
  templateUrl: './cargo-loading-settings-dialog.component.html',
  styleUrls: ['./cargo-loading-settings-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CargoLoadingSettingsDialogComponent implements OnInit {
  settings =      {algorithm: 'backtracking', timer: 3, emergencyStop: false};

  algorithms:     Algorithm[];
  algorithm =     'backtracking';
  timer =         3;
  emergencyStop = false;
  timeFormat    = 'min';

  constructor(
    private planService: PlanService,
    public dialogRef: MatDialogRef<CargoLoadingSettingsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {}

  ngOnInit(): void {
    this.algorithm =      this.data.algorithm;
    this.timer =          this.data.timer;
    this.timeFormat =     this.data.format;
    this.emergencyStop =  this.data.emergencyStop;

    this.algorithms =     this.planService.getAlgorithms();
  }

  back(): void {
    this.dialogRef.close();
  }

  saveSettings(): void {
    this.settings = {
      algorithm:      this.algorithm,
      timer:          this.timer,
      emergencyStop:  this.emergencyStop
    };

    this.dialogRef.close(this.settings);
  }
}
