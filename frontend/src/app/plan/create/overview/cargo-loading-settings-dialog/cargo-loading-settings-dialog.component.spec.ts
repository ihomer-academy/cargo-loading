import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargoLoadingSettingsDialogComponent } from './cargo-loading-settings-dialog.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatOptionModule} from '@angular/material/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('CargoLoadingSettingsDialogComponent', () => {
  let component: CargoLoadingSettingsDialogComponent;
  let fixture: ComponentFixture<CargoLoadingSettingsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatOptionModule],
      declarations: [ CargoLoadingSettingsDialogComponent ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {data: {}} },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CargoLoadingSettingsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
