import {Component, OnInit} from '@angular/core';
import {PlanService} from '../_services/plan.service';
import {Order} from '../../_models/order.model';
import {Cargospace} from '../../_models/cargospace.model';
import {Router} from '@angular/router';
import {VisualisationService} from '../../_services/visualisation.service';
import {LoadDirection} from '../../_models/loaddirection.model';
import {MatDialog} from '@angular/material/dialog';
import {CargoLoadingSettingsDialogComponent} from './cargo-loading-settings-dialog/cargo-loading-settings-dialog.component';
import {CargospacesService} from '../_services/cargospaces.service';
import {OrdersService} from '../_services/orders.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ProgressDialogComponent} from './progress-dialog/progress-dialog.component';
import {HeaderItem} from '../../_models/types/types';
import {DefaultDialogComponent} from '../../_components/default-dialog/default-dialog.component';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class OverviewComponent implements OnInit {
  orders:                 Order[] = [];
  ordersInOrderSequence:  Order[] = [];
  cargoSpace:             Cargospace;
  title =                 '';
  loadDirection:          LoadDirection;
  settings =              {algorithm: 'backtracking', timer: 3, emergencyStop: false};
  expandedElement:        Order[] = [];

  headers: HeaderItem[];

  constructor(
    private planService: PlanService,
    private visualisationService: VisualisationService,
    private cargoService: CargospacesService,
    private ordersService: OrdersService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.headers = this.planService.getHeaders();

    this.planService.step.next(3);

    // this.orders = this.planService.orders;
    if (this.planService.ordersInOrderSequence) {
      this.ordersInOrderSequence = this.planService.ordersInOrderSequence;
    } else {
      const orderIds = localStorage.getItem('selectedOrders');
      if (orderIds) {
        const orderList = this.ordersService.getOrdersDeliveryTomorrow();
        const array = JSON.parse('[' + orderIds + ']');
        array.map(id => this.ordersInOrderSequence.push(orderList.find(o => o.id === id)))

        // this.ordersInOrderSequence = this.orders.reverse();
      }
    }

    this.cargoSpace = this.planService.cargoSpace;

    if (this.cargoSpace) {
      this.loadDirection = this.planService.loadDirection;
    } else {
      const cargoSpaceId = localStorage.getItem('cargoSpace');
      this.cargoSpace = this.cargoService.getCargoSpaces().find(cs => cs.id === +cargoSpaceId)

      const loadDirId = localStorage.getItem('loadDirection');
      this.loadDirection = this.cargoService.getLoadDirections().find(ld => ld.id === loadDirId)
    }

    // this.cargoSpace = this.planService.cargoSpace;

    if (this.planService.loadDirection) {
      this.loadDirection = this.planService.loadDirection;
    } else {
      const loadDirectionId = localStorage.getItem('loadDirection');
      this.loadDirection = this.cargoService.getLoadDirections().find(ld => ld.id === loadDirectionId)
    }
  }

  async plan(): Promise<void> {
    let dialogRef;
    try {
      const formatted       =   this.settings.timer * 60;
      const algSettings     =   {timer: formatted, algorithm: this.settings.algorithm, emergencyStop: this.settings.emergencyStop};
      const reversedOrders  =   this.ordersInOrderSequence.slice().reverse();

      dialogRef = this.dialog.open(ProgressDialogComponent, {
        height: '200px',
        width: '400px',
        disableClose: true
      });

      await this.planService.plan(
        this.title,
        reversedOrders,
        this.cargoSpace,
        algSettings,
        this.loadDirection.id
      );

      dialogRef.close();
      this.visualisationService.title = this.title;
      this.visualisationService.view = false;
      this.orders = reversedOrders;
      this.router.navigate(['/plan/plattegrond-weergave']);
    } catch (e) {
      dialogRef.close();
      this.dialog.open(DefaultDialogComponent, {
        data: {
          title: 'Planning maken is mislukt',
          content: 'Er is iets misgegaan bij het maken van een planning.<br/><br/>\n' +
            '  Tips:\n' +
            '  <ul>\n' +
            '    <li>Verander de volgorde van de bestellingen</li>\n' +
            '    <li>Kies andere bestellingen</li>\n' +
            '    <li>Kies een andere laadruimte</li>\n' +
            '  </ul>'
        }
      });
    }
  }

  checkExpanded(element): boolean {
    return this.planService.checkExpanded(element, this.expandedElement)
  }

  pushPopElement(element): void {
    this.expandedElement = this.planService.pushPopElement(element, this.expandedElement);
  }

  openSettingsDialog(): void {
    const dialogRef = this.dialog.open(CargoLoadingSettingsDialogComponent, {
      height: '350px',
      width: '600px',
      data: {
        algorithm:      this.settings.algorithm,
        timer:          this.settings.timer,
        emergencyStop:  this.settings.emergencyStop
      }
    });

    dialogRef.afterClosed().subscribe(settings => {
      if (settings !== undefined) {
        this.settings = settings;
      }
    });
  }
}
