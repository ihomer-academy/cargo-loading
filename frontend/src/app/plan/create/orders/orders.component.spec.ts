import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrdersComponent} from './orders.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {LoadingInformationComponent} from '../../_components/loading-information/loading-information.component';
import {Cargospace} from '../../_models/cargospace.model';
import {LoadDirection} from '../../_models/loaddirection.model';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {PlanService} from '../_services/plan.service';
import {HeaderItem} from '../../_models/types/types';
import {BehaviorSubject} from 'rxjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('OrdersComponent', () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;

  const mockPlanService = {
    cargoSpace: new Cargospace(1, 'name', 100, 100, 50, 500),
    loadDirection: new LoadDirection('dir', ''),
    step: new BehaviorSubject<number>(0),
    getHeaders(): HeaderItem[] { return []; },
    checkExpanded(): boolean { return false; },
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatCheckboxModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [OrdersComponent, LoadingInformationComponent],
      providers: [
        { provide: PlanService, useValue: mockPlanService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
