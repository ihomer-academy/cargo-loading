import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Router} from '@angular/router';
import {PlanService} from '../_services/plan.service';
import {Order} from '../../_models/order.model';
import {OrdersService} from '../_services/orders.service';
import {CargospacesService} from '../_services/cargospaces.service';
import {Cargospace} from '../../_models/cargospace.model';
import {LoadDirection} from '../../_models/loaddirection.model';
import {MatSort} from '@angular/material/sort';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {HeaderItem} from '../../_models/types/types';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class OrdersComponent implements OnInit {
  orders =                new FormControl();
  orderList:              Order[] = [];   // list with orders
  selectedOrders:         Order[] = [];   // list with selected orders
  selectedIds:            number[] = [];  // list with selected order IDs
  totalProductWeight =    0;              // total weight of selected products
  weightPerct =           0;              // total weight of selected products (in %)
  totalProductVolume =    0;              // total volume of selected products
  volumePerct =           0;              // total volume of selected products (in %)
  maxCargospaceWeight =   0;              // max weight of the cargospace
  totalCargospaceVolume = 0;              // max volume of the cargospace
  amountOfProducts =      0;              // amount of selected products
  amountOfProductsPerct = 0;              // amount of selected products (in %)
  maxProducts =           0;              // amount of total products
  cargoSpace:             Cargospace;     // chosen cargospace
  loadDirection:          LoadDirection;  // chosen load direction

  sortedCol =             '';             // default sorted column
  arrow =                 'south';        // sort arrow is default in south direction
  checkedAll =            false;          // check all checkboxes
  expandedElement:        Order[] = [];   // list with all expanded orders
  validAmount =           true;           // true if all percentages are equal to or less than 100%

  @ViewChild(MatSort) sort: MatSort;

  sortableHeaders: HeaderItem[];

  constructor(
    private router: Router,
    private planService: PlanService,
    private cargoService: CargospacesService,
    private ordersService: OrdersService
  ) {}

  ngOnInit(): void {
    this.sortableHeaders = this.planService.getHeaders();

    this.planService.step.next(2); // Enable second step (for the navigation)

    this.orderList = this.ordersService.getOrdersDeliveryTomorrow();  // Get all orders that need to be delivered tomorrow

    this.cargoSpace = this.planService.cargoSpace;  // Get already chosen cargospace

    if (this.cargoSpace) {
      this.maxCargospaceWeight = this.cargoSpace.maxWeight;
      this.totalCargospaceVolume = this.cargoSpace.height * this.cargoSpace.length
        * this.cargoSpace.width;
      this.loadDirection = this.planService.loadDirection;
    } else {
      const cargoSpaceId = localStorage.getItem('cargoSpace');
      this.cargoSpace = this.cargoService.getCargoSpaces().find(cs => cs.id === +cargoSpaceId)
      this.maxCargospaceWeight = this.cargoSpace.maxWeight;
      this.totalCargospaceVolume = this.cargoSpace.height * this.cargoSpace.length * this.cargoSpace.width;

      const loadDirId = localStorage.getItem('loadDirection');
      this.loadDirection = this.cargoService.getLoadDirections().find(ld => ld.id === loadDirId)
    }

    this.orderList.map(ol => this.maxProducts += ol.products.length); // Count amount of products that need to be delivered
    this.planService.maxProducts = this.maxProducts;

    if (this.planService.orders) {
      this.selectedOrders = this.planService.orders;
      this.selectedIds = this.selectedOrders.map(a => a.id);
      this.calculateNewValues();

      this.setOrderListInRightSequence();

    } else {
      const orderIds = localStorage.getItem('selectedOrders');
      if (orderIds) {
        const array = JSON.parse('[' + orderIds + ']');
        array.map(id => this.selectedOrders.push(this.orderList.find(o => o.id === id)))
        this.selectedIds = this.selectedOrders.map(a => a.id);
        this.calculateNewValues();

        this.setOrderListInRightSequence();
      }
    }
  }

  confirmOrders(): void {
    this.updateSequence();
    this.planService.ordersInOrderSequence = this.selectedOrders;
    this.router.navigate(['/plan/overzicht']);
  }

  // Makes it possible to drag and drop the orders in the right sequence
  cdkDrop(
    event: CdkDragDrop<string[]>
  ): void {
    moveItemInArray(this.orderList, event.previousIndex, event.currentIndex);
    this.updateSequence();
  }

  selectOrder(
    order: Order,
    event: MatCheckboxChange
  ): void {
    if (order !== null) {
      // Add order to selectedOrders when checkbox is now checked
      if (event.checked) {
        this.selectedOrders.push(order);
        this.selectedIds.push(order.id);
      } else {
        // Remove order of selectedOrders when checkbox is now unchecked
        const index = this.selectedOrders.indexOf(order);
        if (index > -1) {
          this.selectedOrders.splice(index, 1);

          const idIndex = this.selectedIds.indexOf(order.id);
          this.selectedIds.splice(idIndex, 1);
        }
      }
    } else {
      // Check all orders
      if (event.checked) {
        this.checkedAll = true;
        this.selectedOrders = this.orderList;
        this.selectedIds = this.selectedOrders.map(a => a.id);
      } else {
        // Uncheck all orders
        this.checkedAll = false;
        this.selectedOrders = [];
        this.selectedIds = [];
      }
    }

    this.calculateNewValues();
  }

  setSort(
    sortedCol: string
  ): void {
    if (this.sortedCol === sortedCol) {
      if (this.arrow === 'north') {
        this.arrow = 'south';
      } else if (this.arrow === 'south') {
        this.arrow = 'north';
      }
    } else {
      this.arrow = 'south'
    }
    this.sortedCol = sortedCol;
  }

  private calculateNewValues(): void {
    if (this.selectedOrders && this.selectedOrders.length > 0)
    {
      // Reset all values
      this.totalProductWeight     = 0;
      this.totalProductVolume     = 0;
      this.amountOfProducts       = 0;
      this.amountOfProductsPerct  = 0;

      this.selectedOrders.map(a => {
        this.amountOfProducts += a.products.length; // calculate amount of selected products

        // Calculate total weight and volume of selected products
        a.products.map(p => {
          this.totalProductWeight += p.weight; // Add product weight to total weight
          this.totalProductVolume += p.length * p.width * p.height;
        });
      });

      // round the percentages to a whole number
      this.weightPerct            =   Math.round(this.totalProductWeight / this.maxCargospaceWeight * 100)
      this.volumePerct            =   Math.round(this.totalProductVolume / this.totalCargospaceVolume * 100)
      this.amountOfProductsPerct  =   Math.round(this.amountOfProducts / this.maxProducts * 100)

    }
    else {
      // Reset all values
      if (this.totalProductWeight > 0) {
        this.totalProductWeight = 0;
        this.totalProductVolume = 0;
        this.amountOfProducts = 0;
        this.weightPerct = 0;
        this.volumePerct = 0;
        this.amountOfProductsPerct = 0;
      }
    }

    // set new values
    this.planService.currentWeight.next(this.weightPerct);
    this.planService.currentVolume.next(this.volumePerct);
    this.planService.amtOfProducts.next(this.amountOfProducts);
    this.planService.amountOfProductsPerct.next(this.amountOfProductsPerct);

    this.planService.orders = this.selectedOrders;

    this.validAmount = this.weightPerct <= 100 && this.volumePerct <= 100 && this.amountOfProductsPerct <= 100;

    const orderIds = [];
    this.selectedOrders.map(o => orderIds.push(o.id));
    localStorage.setItem('selectedOrders', String(orderIds));
  }

  private updateSequence(): void {
    const tempSelectedOrders = [];
    const tempSelectedIds = [];

    // Loop all orders (in right sequence)
    this.orderList.map(o => {
      // Check if order is selected or not
      const orderIndex = this.selectedOrders.findIndex(so => so.id === o.id);

      // If order is selected, add it to the selectedOrders array
      if (orderIndex > -1) {
        tempSelectedOrders.push(o);
        tempSelectedIds.push(o.id);
      }
    })

    this.selectedOrders = tempSelectedOrders;
    this.selectedIds = tempSelectedIds;
  }

  checkExpanded(element): boolean {
    return this.planService.checkExpanded(element, this.expandedElement)
  }

  pushPopElement(element): void {
    this.expandedElement = this.planService.pushPopElement(element, this.expandedElement);
  }

  private setOrderListInRightSequence(): void {
    // Set orderList in the right sequence (start with the the selectedOrders)
    const notSelected = this.orderList.filter(obj => {
      return !this.selectedOrders.some(obj2 => {
        return obj.id === obj2.id;
      });
    });

    this.orderList = this.selectedOrders.concat(notSelected)
  }
}
