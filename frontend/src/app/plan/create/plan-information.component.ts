import {Component, OnInit} from '@angular/core';
import {PlanService} from './_services/plan.service';

/*
 * Page that shows the steps that the user need to fullfill
 */
@Component({
  selector: 'app-plan-information',
  templateUrl: './plan-information.component.html',
  styleUrls: ['./plan-information.component.scss']
})
export class PlanInformationComponent implements OnInit {

  constructor(
    private planService: PlanService
  ) {}

  ngOnInit(): void {
    this.planService.isPlanning.next(false);
  }

}
