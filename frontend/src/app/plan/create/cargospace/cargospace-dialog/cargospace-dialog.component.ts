import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {Cargospace} from '../../../_models/cargospace.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

/*
 * Dialog to add a custom cargospace
 */
@Component({
  selector: 'app-cargospace-dialog',
  templateUrl: './cargospace-dialog.component.html',
  styleUrls: ['./cargospace-dialog.component.scss']
})
export class CargospaceDialogComponent implements OnInit {
  name:       string;
  length:     number;
  width:      number;
  height:     number;
  maxWeight:  number;

  cargospaceGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CargospaceDialogComponent>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.cargospaceGroup = this.fb.group({
      name:       ['', Validators.required],
      length:     ['', Validators.required],
      width:      ['', Validators.required],
      height:     ['', Validators.required],
      maxWeight:  ['', Validators.required]
    })
  }

  back(): void {
    this.dialogRef.close();
  }

  addCargospace(): void {
    this.dialogRef.close(new Cargospace(999, this.name, this.length, this.width, this.height, this.maxWeight));
  }
}
