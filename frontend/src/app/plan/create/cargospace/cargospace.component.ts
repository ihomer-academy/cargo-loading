import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PlanService} from '../_services/plan.service';
import {Router} from '@angular/router';
import {Cargospace} from '../../_models/cargospace.model';
import {CargospacesService} from '../_services/cargospaces.service';
import {LoadDirection} from '../../_models/loaddirection.model';
import {MatDialog} from '@angular/material/dialog';
import {CargospaceDialogComponent} from './cargospace-dialog/cargospace-dialog.component';

@Component({
  selector: 'app-cargospace',
  templateUrl: './cargospace.component.html',
  styleUrls: ['./cargospace.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CargospaceComponent implements OnInit {
  cargoSpaceList: Cargospace[] = [];
  cargoSpace:     Cargospace;
  loadDirections: LoadDirection[] = [];
  loadDirection:  LoadDirection;

  constructor(
    private planService: PlanService,
    private cargoService: CargospacesService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.cargoSpaceList = this.cargoService.getCargoSpaces();
    this.cargoSpace =     this.cargoSpaceList[0];                 // Default select the first item
    this.loadDirections = this.cargoService.getLoadDirections();
    this.loadDirection =  this.loadDirections[0];                 // Default select the first load direction

    this.planService.step.next(1);          // Set step to 1 because user is choosing a cargospace
    this.planService.isPlanning.next(true); // user is making a plan
  }

  confirmCargospace(): void {
    this.planService.cargoSpace     =   this.cargoSpace;
    this.planService.loadDirection  =   this.loadDirection;

    localStorage.setItem('cargoSpace', String(this.cargoSpace.id));
    localStorage.setItem('loadDirection', String(this.loadDirection.id));

    this.router.navigate(['/plan/kies-bestellingen']);
  }

  openAddCargospaceDialog(): void {
    const dialogRef = this.dialog.open(CargospaceDialogComponent, {
      height: '425px',
      width: '650px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.cargoSpace = result;
      }
    });
  }
}
