import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Cargospace} from '../../_models/cargospace.model';
import {LoadDirection} from '../../_models/loaddirection.model';

/*
 * Service that contains the cargospace
 * At the moment the cargospaces are mocked data (because this is a MVP), but in the future they will get these of an API or database
 */
@Injectable({
  providedIn: 'root'
})
export class CargospacesService {
  private cargoSpacesSubject: BehaviorSubject<Cargospace>;

  constructor(
    private http: HttpClient
  ) {}

  _getCargoSpaces(): Observable<any> {
    return this.http.get('/api/plan/cargospaces');
  }

  getCargoSpaces(): Cargospace[] {
    return [
      new Cargospace(2, 'Oplegger klein', 1000, 250, 250, 30000),
      new Cargospace(1, 'BT Oplegger klein', 1000, 250, 250, 30000),
      new Cargospace(3, 'Oplegger groot', 1350, 250, 250, 40000),
    ];
  }

  getLoadDirections(): LoadDirection[] {
    return [
      new LoadDirection('z-x-y', 'Via zijkanten laden'),
      new LoadDirection('x-y-z', 'Via achterkant/laadklep laden'),
    ];
  }
}
