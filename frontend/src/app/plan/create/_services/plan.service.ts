import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Order} from '../../_models/order.model';
import {Cargospace} from '../../_models/cargospace.model';
import {environment} from '../../../../environments/environment';
import {AlgorithmResponse} from '../../_models/algorithm-response.model';
import {Loaditem} from '../../_models/loaditem.model';
import {VisualisationService} from '../../_services/visualisation.service';
import {LoadDirection} from '../../_models/loaddirection.model';
import {Algorithm} from '../../_models/algorithm.model';
import {CargoLayout, HeaderItem, LoadDistribution} from '../../_models/types/types';

/*
 * Service that contains the algorithms and contains function to make a plan
 */
@Injectable({
  providedIn: 'root'
})
export class PlanService {
  orders:                 Order[];
  ordersInOrderSequence:  Order[];
  cargoSpace:             Cargospace;
  apiUrl =                environment.API_URL;
  cargoLayout:            CargoLayout;
  loadList;
  loadDistribution:       LoadDistribution;
  loadDirection:          LoadDirection;
  maxProducts:            number;
  request;

  currentWeight:          BehaviorSubject<number> = new BehaviorSubject<number>(0);
  currentVolume:          BehaviorSubject<number> = new BehaviorSubject<number>(0);
  amtOfProducts:          BehaviorSubject<number> = new BehaviorSubject<number>(0);
  amountOfProductsPerct:  BehaviorSubject<number> = new BehaviorSubject<number>(0);

  step:                   BehaviorSubject<number> = new BehaviorSubject<number>(0);
  isPlanning:             BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(
    private http: HttpClient,
    private visualisationService: VisualisationService
  ) {}

  getAlgorithms(): Algorithm[] {
    return [
      new Algorithm(
        'backtracking',
        'Uitgebreid',
        'Vind vaker een oplossing, maar duurt bij ingewikkelde indelingen wat langer'
      ),
      new Algorithm(
        'greedy',
        'Snel',
        'Is snel, maar kan geen ingewikkelde indelingen maken'
      )
    ];
  }

  plan(
    title: string,
    orders: Order[],
    cargospace: Cargospace,
    settings,
    loadDirection: string
  ): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      const planBody = {
        title,
        orders,
        cargospace,
        settings,
        loadDirection
      };
      const req = this.http.post(`${this.apiUrl}/api/plan`, planBody);

      this.request = req.subscribe({
          next: (response: AlgorithmResponse) => {
            this.visualisationService.id = response.id;
            this.visualisationService.cargoLayout = response.cargoLayout;
            this.visualisationService.loadList = this.convertLoadlist(response.loadList);
            this.visualisationService.loadDistribution = response.loadDistribution;
            this.visualisationService.cargoSpace = cargospace;
            this.visualisationService.cargoLayoutSteps = response.cargoLayoutSteps;
            this.visualisationService.amountOfOriginalProducts = response.amountOfOriginalProducts;
            this.visualisationService.createdOn = new Date();

            window.localStorage.clear();
            resolve(true);
          },
          error: e => {
            console.log(e.error);
            reject(e);
          }
        });
    });
  }

  public convertLoadlist(
    loadList: Loaditem[]
  ): any {
    const tempLoadlist = [];

    for (const loaditem of loadList) {
      const orderIndex = tempLoadlist.findIndex(li => li.order.id === loaditem.order.id);

      if (orderIndex !== -1) {
        tempLoadlist[orderIndex].loadItems.push(new Loaditem(loaditem.id, loaditem.order, loaditem.product));
      } else {
        const i = [];
        i.push(new Loaditem(loaditem.id, loaditem.order, loaditem.product));
        const obj = {order: loaditem.order, loadItems: i};
        tempLoadlist.push(obj);
      }
    }
    return tempLoadlist;
  }

  pushPopElement(element, expandedElement): [] {
    const index = expandedElement.indexOf(element);
    if (index === -1) {
      expandedElement.push(element);
    } else {
      expandedElement.splice(index, 1);
    }
    return expandedElement;
  }

  checkExpanded(element, expandedElement): boolean {
    let flag = false;
    expandedElement.forEach(e => {
      if (e === element) {
        flag = true;
      }
    });
    return flag;
  }

  getHeaders(): HeaderItem[] {
    return [
      {name: 'customer',    displayName: 'Naam'},
      {name: 'address',     displayName: 'Adres'},
      {name: 'postalCode',  displayName: 'Postcode'},
      {name: 'city',        displayName: 'Stad'},
      {name: 'products',    displayName: 'Producten'},
    ]
  }

  resetPlanService(): void {
    this.orders = [];
    this.ordersInOrderSequence = [];
    this.cargoSpace = null;
    this.amountOfProductsPerct.next(0);
    this.step.next(1);
    this.isPlanning.next(false);
    this.currentVolume.next(0);
    this.currentWeight.next(0);
    this.amtOfProducts.next(0);
  }
}
