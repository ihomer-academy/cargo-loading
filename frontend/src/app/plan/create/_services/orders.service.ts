import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Order} from '../../_models/order.model';
import {Product} from '../../_models/product.model';

/*
 * Service that contains the mocked products and orders
 */
@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private ordersSubject: BehaviorSubject<Order[]>;

  constructor(
    private http: HttpClient
  ) {}

  _getOrdersDeliveryTomorrow(): Observable<any> {
    return this.http.get('/api/plan/orders/' + new Date(new Date().getDate() + 1));
  }

  getOrdersDeliveryTomorrow(): Order[] {
    const p1 = new Product(1, 'Rode gevelstenen', 100, 100, 50, 1100, 1100, 600);
    const p2 = new Product(2, 'Gele gevelstenen', 100, 100, 50, 1000, 1000, 500);
    const p3 = new Product(3, 'Big Bag met zand', 100, 100, 100, 1450, 0);
    const p4 = new Product(4, 'Big Bag met grind', 100, 100, 100, 1600, 0);
    const p5 = new Product(5, 'Rockwool steenwolplaat', 100, 50, 50, 500);
    const p6 = new Product(6, 'Douglas geimpregneerde balken', 500, 100, 50, 1500, 1500, 6);
    const p7 = new Product(7, 'Vuren geimpregneerde balken', 250, 100, 50, 1100, 1100, 8);

    return [
      new Order(1, 'Piet Jansen', 'Stationstraat 12', '4871NJ', 'Etten-Leur', [p5, p7]),
      new Order(2, 'Jo de Bruijn', 'Straat 12', '4702GN', 'Roosendaal', [p2, p3, p3, p4]),
      new Order(3, 'Lisa de Renner', 'Kerstraat 34', '3039TM', 'Breda', [p4, p4, p4]),
      new Order(4, 'Chantal de Water', 'Waterstraat 12', '3039TM', 'Breda', [p3, p3, p3]),
      new Order(5, 'Co de Rood', 'Hoofdstraat 8', '4871NJ', 'St. Willebrord', [p4, p4, p1, p1, p7]),

      new Order(6, 'Piet de Regt', 'Kermislaan 14', '5011BD', 'Tilburg', [p6, p3, p3, p3]),
      new Order(7, 'Kees Bartels', 'Kermisstraat 22', '5011BD', 'Tilburg', [p2, p2, p2, p1]),
      new Order(8, 'Roos', 'Schoolstraat', '4702GN', 'Roosendaal', [p1, p3, p1]),
      new Order(9, 'Janssen', 'Schoolstraat', '4702GN', 'Roosendaal', [p3, p4, p1]),
      new Order(10, 'Jan', 'Schoolstraat', '4702GN', 'Roosendaal', [p3, p1, p1]),
      new Order(11, 'Chantal Janssen', 'Schoolstraat', '4702GN', 'Roosendaal', [p4, p4, p6]),

      new Order(15, 'Bt order 1', 'Knipplein 11', '4702GN', 'Roosendaal', [p4, p4, p6]),
      new Order(14, 'Bt order 2', 'Kerkstraat 15', '4702GN', 'Roosendaal', [p3, p1, p1]),
      new Order(13, 'Bt order 3', 'Kerkstraat 1', '4702GN', 'Roosendaal', [p3, p4, p1]),
      new Order(12, 'Bt order 4', 'Schoolstraat', '4702GN', 'Roosendaal', [p1, p3, p1]),
    ];
  }
}
