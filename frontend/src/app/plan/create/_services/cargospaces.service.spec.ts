import {TestBed} from '@angular/core/testing';

import {CargospacesService} from './cargospaces.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CargospacesService', () => {
  let service: CargospacesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(CargospacesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
