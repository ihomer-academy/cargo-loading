import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PlanInformationComponent} from './plan-information.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('PlanInformationComponent', () => {
  let component: PlanInformationComponent;
  let fixture: ComponentFixture<PlanInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PlanInformationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
