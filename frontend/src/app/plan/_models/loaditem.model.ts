import {Order} from './order.model';
import {Product} from './product.model';

export class Loaditem {
  constructor(
    public id:      number,
    public order:   Order,
    public product: Product
  ) {}
}
