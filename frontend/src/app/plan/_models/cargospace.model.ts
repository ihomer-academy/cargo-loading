export class Cargospace {
  constructor(
    public id:        number,
    public name:      string,
    public length:    number,
    public width:     number,
    public height:    number,
    public maxWeight: number
  ) {}
}
