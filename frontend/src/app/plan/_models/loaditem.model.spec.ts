import { Loaditem } from './loaditem.model';
import {Order} from './order.model';
import {Product} from './product.model';

describe('Loaditem', () => {
  it('should create an instance', () => {
    expect(new Loaditem(1, new Order(1, '', '',  '', '', []), new Product(1, '', 100, 100, 100, 100))).toBeTruthy();
  });
});
