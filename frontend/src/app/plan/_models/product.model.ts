export class Product {
  constructor(
    public id:              number,
    public name:            string,
    public length:          number,
    public width:           number,
    public height:          number,
    public weight:          number,
    public maxLoadWeight?:  number,
    public units:           number = 1
  ) {
    if (maxLoadWeight) {
      this.maxLoadWeight = maxLoadWeight;
    } else {
      this.maxLoadWeight = weight;
    }
  }
}
