export class Algorithm {
  constructor(
    public id:          string,
    public title:       string,
    public description: string
  ) {}
}
