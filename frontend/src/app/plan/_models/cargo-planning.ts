import {Loaditem} from './loaditem.model';
import {Cargospace} from './cargospace.model';
import {CargoLayout, LoadDistribution} from './types/types';

export class CargoPlanning {
  // tslint:disable-next-line:variable-name
  _id:                      string;
  createdOn:                Date;
  note:                     string;
  cargoLayout:              CargoLayout;
  cargoSpace:               Cargospace;
  loadList:                 Loaditem[];
  loadDistribution:         LoadDistribution;
  cargoLayoutSteps:         CargoLayout[];
  title:                    string;
  used:                     boolean;
  loadDirection:            string;
  amountOfOriginalProducts: number
}
