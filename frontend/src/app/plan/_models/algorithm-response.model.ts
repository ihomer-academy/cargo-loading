import {Loaditem} from './loaditem.model';
import {CargoLayout, LoadDistribution} from './types/types';

/**
 * Model that will be used as response for the created cargo layout
 */
export class AlgorithmResponse {
  id:                       string;
  cargoLayout:              CargoLayout;
  loadList:                 Loaditem[];
  loadDistribution:         LoadDistribution;
  cargoLayoutSteps:         [CargoLayout];
  amountOfOriginalProducts: number;
}
