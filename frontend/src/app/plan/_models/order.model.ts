import {Product} from './product.model';

export class Order {
  constructor(
    public id:          number,
    public customer:    string,
    public address:     string,
    public postalCode:  string,
    public city:        string,
    public products:    Product[]
  ) {}
}
