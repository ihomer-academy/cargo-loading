export type HeaderItem = {name: string, displayName: string}
export type CargoLayout = string[][][]
export type LoadDistribution = {leftKg: number, rightKg: number}
